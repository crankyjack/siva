import "../../Workspace/utils/workspace-register-list";
import * as React from "react";
import {ReactNode} from "react";
import {connect} from "react-redux";
import {Dispatch} from "redux";
import autobind from "autobind-decorator";
import {Button, ButtonGroup, Grid} from "react-bootstrap";
import MainState from "../flux/main-state";
import {ContextMenuData, default as ContextMenu} from "../../Common/components/context-menu";
import {appendNormalizedToState, Class, filterNormalizedStateIdCategory, initNormalizedCategoryInState, mapNormalizedStateIdCategory, mergeObjectToState, NormalizedStateIdCategory, removeNormalizedFromState, textToClipboard} from "../../Common/utils/util";
import ACTIONS from "../flux/actions";
import store from "../flux/store";
import WorkspaceChoicePanel from "../../Workspace/components/workspace-choice-panel";
import WorkspaceRegister from "../../Workspace/utils/workspace-register";
import ConfirmationModal from "../../Common/components/confirmation-modal";
import ApplicationStateInputModal from "./application-state-input-modal";

interface MainComponentProps {
    applicationState: MainState,
    dispatch: Dispatch<MainState>
}

interface MainComponentState {
    hasError: boolean
    contextMenuData: ContextMenuData | null
    modalsData: NormalizedStateIdCategory<any>
}

export class MainComponent extends React.PureComponent<MainComponentProps, MainComponentState> {
    constructor (props: MainComponentProps & {children?: ReactNode;}, context?: any) {
        super(props);

        this.state = {
            hasError: false,
            contextMenuData: null
        } as MainComponentState;

        this.state = initNormalizedCategoryInState(this.state, "modalsData");
    }

    @autobind
    fillAndShowContextMenu (data: ContextMenuData) {
        this.setState({
            contextMenuData: data
        });
    }

    @autobind
    hideContextMenu () {
        this.setState({
            contextMenuData: null
        });
    }

    @autobind
    fillAndShowModal (modalClass: Class<React.Component>, data: any) {
        data = {
            ...data,
            modalClass: modalClass
        };

        this.setState(prevState => appendNormalizedToState(prevState, ["modalsData"], data), () => {
            if (typeof data.onShowModal === "function") {
                data.onShowModal(data.id);
            }
        });
    }

    @autobind
    updateModal (id: number, data: any) {
        this.setState(prevState => {
            if (prevState.modalsData.allIds.indexOf(id) !== -1) {
                return mergeObjectToState(prevState, ["modalsData", "byId", id], data);
            }
        });
    };

    @autobind
    removeModal (id: number) {
        let onRemoveModal: () => void;
        this.setState(prevState => {
            onRemoveModal = prevState.modalsData.byId[id].onRemoveModal;
            return removeNormalizedFromState(prevState, ["modalsData"], id);
        }, () => {
            if (typeof onRemoveModal === "function") {
                onRemoveModal();
            }
        });
    }

    componentDidCatch (error: Error, errorInfo: React.ErrorInfo): void {
        console.error(error);
        this.setState({
                hasError: true
            }
        );
    }

    @autobind
    resetHandler () {
        this.fillAndShowModal(ConfirmationModal, {
            title: "State reset confirmation",
            message: "Are you sure you want to reset the state of the application? This will permanently delete all current data.",
            onConfirm: () => {
                store.dispatch(ACTIONS.CREATE_RESET_STATE());
                this.setState({
                        hasError: false
                    }
                );
            }
        });
    }

    render () {
        const debugButtons = (
            <ButtonGroup className="debug-button-toolbar">
                <Button
                    bsSize="xs"
                    onClick={() => {textToClipboard(JSON.stringify(this.props.applicationState));}}
                >
                    State to clipboard
                </Button>
                <Button
                    bsSize="xs"
                    onClick={() => {this.fillAndShowModal(ApplicationStateInputModal, null);}}
                >
                    State from clipboard
                </Button>
                <Button
                    bsSize="xs"
                    onClick={this.resetHandler}
                >
                    Reset state
                </Button>
            </ButtonGroup>
        );

        const stateManipulationModals = filterNormalizedStateIdCategory(this.state.modalsData, data => data.modalClass === ApplicationStateInputModal || (data.modalClass === ConfirmationModal && data.title === "State reset confirmation"));
        if (!this.state.hasError) {
            return (
                <Grid
                    className="main"
                    fluid
                >
                    {debugButtons}
                    {
                        this.props.applicationState.GLOBAL.activeWorkspaceIds.length > 0 ||
                        <WorkspaceChoicePanel
                            dispatch={this.props.dispatch}
                        />
                    }
                    {this.props.applicationState.GLOBAL.activeWorkspaceIds.map(workspaceId => {
                        const workspace = this.props.applicationState.Workspaces.byId[workspaceId];

                        const WorkspaceComponent = WorkspaceRegister.get(workspace.type)!;
                        return (
                            <WorkspaceComponent
                                key={`workspace-${workspace.id}`}
                                id={workspace.id}
                                applicationState={this.props.applicationState}
                                updateModal={this.updateModal}
                                fillAndShowModal={this.fillAndShowModal}
                                fillAndShowContextMenu={this.fillAndShowContextMenu}
                                dispatch={this.props.dispatch}
                            />
                        );
                    })}

                    {
                        mapNormalizedStateIdCategory(
                            this.state.modalsData, data => {
                            const ModalClass: Class<React.Component> = data.modalClass;

                            return <ModalClass
                                key={`${data.id}-${data.toString()}`}
                                {...data}
                                fillAndShowModal={this.fillAndShowModal}
                                updateModal={this.updateModal}
                                removeModal={this.removeModal}
                                dispatch={this.props.dispatch}
                            />;
                        })
                    }
                    {
                        this.state.contextMenuData ?
                            <ContextMenu
                                data={this.state.contextMenuData}
                                hide={this.hideContextMenu}
                            /> :
                            null
                    }
                </Grid>
            );
        }
        else {
            return <div>
                {
                    debugButtons
                }
                {

                    mapNormalizedStateIdCategory(
                        stateManipulationModals, data => {
                            const ModalClass: Class<React.Component> = data.modalClass;

                            return <ModalClass
                                key={`${data.id}-${data.toString()}`}
                                {...data}
                                fillAndShowModal={this.fillAndShowModal}
                                updateModal={this.updateModal}
                                removeModal={this.removeModal}
                                dispatch={this.props.dispatch}
                            />;
                        })
                }
            </div>;
        }
    }
}

const mapStateToProps = (state: MainState) => ({
    applicationState: state
});

export default connect(
    mapStateToProps
)(MainComponent);
