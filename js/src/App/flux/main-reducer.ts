import {AnyAction, default as ACTIONS} from "./actions";
import {appendNormalizedToState, appendToStateArray} from "../../Common/utils/util";
import {createNewEmptyMainState, default as MainState} from "./main-state";
import {Reducer} from "redux";
import WorkspaceRegister from "../../Workspace/utils/workspace-register";

const MainReducer: Reducer<MainState> = (state: MainState = createNewEmptyMainState(), payload: AnyAction) => {
    payloadSwitch:
        switch (payload.type) {
            case ACTIONS.SET_STATE: {
                const action = payload as ACTIONS.PAYLOAD_SET_STATE;
                state = action.state;
                break payloadSwitch;
            }
            case ACTIONS.RESET_STATE: {
                const action = payload as ACTIONS.PAYLOAD_RESET_STATE;
                state = createNewEmptyMainState();
                break payloadSwitch;
            }
            case ACTIONS.CREATE_WORKSPACE: {
                const action = payload as ACTIONS.PAYLOAD_CREATE_WORKSPACE;
                let workspace;
                [state, workspace] = WorkspaceRegister.get(action.workspaceType)!.factory(state, action.data);
                state = appendToStateArray(state, ["GLOBAL", "activeWorkspaceIds"], workspace.id, false);
                break payloadSwitch;
            }
            case ACTIONS.CREATE_PROBLEM: {
                const action = payload as ACTIONS.PAYLOAD_CREATE_PROBLEM;
                state = appendNormalizedToState(state, ["Problems"], action.problem);
                break payloadSwitch;
            }
        }
    return state;
};

export default MainReducer;