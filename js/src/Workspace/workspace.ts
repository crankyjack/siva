import {MainComponent} from "../App/components/main-component";
import MainState from "../App/flux/main-state";
import * as React from "react";
import {Dispatch} from "redux";
import {AnyAction} from "../App/flux/actions";

export default interface Workspace {
    id: number;
    type: string;
}

export interface WorkspaceProps {
    id: number,
    applicationState: MainState,
    updateModal: typeof MainComponent.prototype.updateModal
    fillAndShowModal: typeof MainComponent.prototype.fillAndShowModal
    fillAndShowContextMenu: typeof MainComponent.prototype.fillAndShowContextMenu
    dispatch: Dispatch<MainState>
}


export interface WorkspaceAction extends AnyAction {
    workspaceId: number
}

export abstract class WorkspaceView<S = {}> extends React.PureComponent<WorkspaceProps, S> {

}