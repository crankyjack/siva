import * as React from "react";
import ACTIONS from "../../App/flux/actions";
import WorkspaceRegister from "../utils/workspace-register";
import {Button, Panel} from "react-bootstrap";
import MainState from "../../App/flux/main-state";
import {Dispatch} from "redux";

export interface WorkspaceChoicePanelProps {
    dispatch: Dispatch<MainState>
}

export default class WorkspaceChoicePanel extends React.PureComponent<WorkspaceChoicePanelProps> {
    render () {
        return (

            <Panel
                className="workspace-choice-panel"
                bsStyle="primary"
                header="Create a new workspace"
            >
                {WorkspaceRegister.getAll().map(workspaceClass => (
                    <Button
                        key={workspaceClass.className}
                        onClick={() => {this.props.dispatch(ACTIONS.CREATE_CREATE_WORKSPACE(workspaceClass.className));}}
                    >
                        {workspaceClass.workspaceLabel}
                    </Button>
                ))}
            </Panel>
        );
    }
}