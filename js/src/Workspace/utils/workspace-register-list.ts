import WorkspaceRegister from "./workspace-register";
import {DLCTreeTableauWorkspaceView} from "../../DL/CTreeTableauReasoning/CompletionTree/DLCTreeTableauWorkspace/components/dl-ctree-tableau-workspace-view";

WorkspaceRegister.register(DLCTreeTableauWorkspaceView);