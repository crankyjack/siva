import Problem from "../problem";
import {DLDenotationState} from "../../DL/Denotation/denotation";

export default class ProblemRouter {
    private static helpers: Map<string, ProblemHelper> = new Map();
    private static workspaceProblemAssignHandlerMap: {
        [index: string]: <S extends DLDenotationState> (problem: Problem, workspaceId: number, state: S) => S
    } = {};

    static registerHelper (helper: ProblemHelper) {
        this.helpers.set(helper.PROBLEM_TYPE, helper);
    }

    static registerWorkspaceProblemAssignHandler (workspaceType: string, handler: <S extends DLDenotationState> (problem: Problem, workspaceId: number, state: S) => S) {
        this.workspaceProblemAssignHandlerMap[workspaceType] = handler;
    }

    static getHelper (ProblemType: string) {
        return this.helpers.get(ProblemType);
    }

    static getAllHelpers () {
        return Array.from(this.helpers.values());
    }

    static handleWorkspaceProblemAssign<S extends DLDenotationState> (problem: Problem, workspaceId: number, state: S): S {
        const handler = this.workspaceProblemAssignHandlerMap[state.Workspaces.byId[workspaceId].type];

        return handler(problem, workspaceId, state);
    }
}

export interface ProblemHelper {
    PROBLEM_TYPE: string;
    PROBLEM_NAME: string;
    PROBLEM_DESCRIPTION: string;
}
