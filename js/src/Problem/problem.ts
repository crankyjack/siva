export default interface Problem {
    id: number,
    type: string
}