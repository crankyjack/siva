import MainState from "../App/flux/main-state";
import {Dispatch} from "redux";
import {MainComponent} from "../App/components/main-component";

export interface ModalComponentProps {
    id: number
    removeModal: typeof MainComponent.prototype.removeModal
    fillAndShowModal: typeof MainComponent.prototype.fillAndShowModal
    updateModal: typeof MainComponent.prototype.updateModal
    dispatch: Dispatch<MainState>
}

export interface ModalComponentState {
    open: boolean
}