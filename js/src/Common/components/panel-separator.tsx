import * as React from "react";
import {MouseEvent} from "react";
import autobind from "autobind-decorator";

export interface PanelSeparatorProps {
    height: number
    onDragStart: (e: MouseEvent<HTMLDivElement>) => void
}

export interface PanelSeparatorState {
    isHovered: boolean
}

export default class PanelSeparator extends React.PureComponent<PanelSeparatorProps, PanelSeparatorState> {
    constructor (props: PanelSeparatorProps) {
        super(props);

        this.state = {
            isHovered: false
        };
    }

    @autobind
    handleMouseEnter () {
        this.setState({
            isHovered: true
        });
    }

    @autobind
    handleMouseLeave () {
        this.setState({
            isHovered: false
        });
    }

    render () {
        const halfHeight = Math.round(this.props.height / 2);

        return (
            <div
                onMouseEnter={this.handleMouseEnter}
                onMouseLeave={this.handleMouseLeave}
                className="panel-separator"
                style={{
                    height: `${this.props.height}px`,
                    background: this.state.isHovered ? `linear-gradient(to bottom, transparent, transparent ${halfHeight - 1}px, #FFFFFF80 ${halfHeight - 1}px, #FFFFFF80 ${halfHeight}px, transparent ${halfHeight}px)` : "none"

                }}
                onMouseDown={this.props.onDragStart}
            />
        );
    }
}