import * as React from "react";
import {FormEvent, SyntheticEvent} from "react";
import {ModalComponentProps, ModalComponentState} from "../modal-component";
import autobind from "autobind-decorator";
import {Button, ControlLabel, Fade, FormControl, FormGroup, HelpBlock, Modal} from "react-bootstrap";

export interface FormValidationInformation {
    hint: string;
    validationState: "success" | "warning" | "error" | undefined;
    buttonBsStyle: string | null;
    buttonDisabled: boolean;
}

export default abstract class SingleInputModal<P extends ModalComponentProps, S extends ModalComponentState> extends React.PureComponent<P, S> {
    protected nameInput: HTMLInputElement;

    protected abstract getModalTitleText (): string;

    protected abstract getInputLabelText (): string;

    protected abstract getInputDefaultText (): string;

    protected abstract getInputPlaceholderText (): string;

    protected abstract getSubmitButtonText (): string;

    protected abstract getFormValidationInformation (): FormValidationInformation;

    constructor (props: P) {
        super(props);
    }

    protected abstract nameInputChangeHandler (e: FormEvent<FormControl>): void;

    protected abstract onSubmit (): void;

    @autobind
    handleOk (e?: SyntheticEvent<any>) {
        !e || e.preventDefault();
        this.onSubmit();
        this.hide();
    }

    @autobind
    hide () {
        this.setState({
            open: false
        });
    }

    @autobind
    show () {
        this.setState({
            open: true
        });
    }

    @autobind
    focusSelectInput () {
        this.nameInput.focus();
        this.nameInput.select();
    }

    @autobind
    removeModal () {
        this.props.removeModal(this.props.id);
    }


    render () {
        const formValidationInformation = this.getFormValidationInformation();

        return (
            <Modal
                show={this.state.open}
                onEntered={this.focusSelectInput}
                onHide={this.hide}
                onExited={this.removeModal}
                backdrop
            >
                <form onSubmit={this.handleOk}>
                    <Modal.Header closeButton>
                        <Modal.Title>{this.getModalTitleText()}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <FormGroup
                            validationState={formValidationInformation.validationState}
                        >
                            <ControlLabel>{this.getInputLabelText()}</ControlLabel>
                            <FormControl
                                inputRef={(input) => this.nameInput = input!}
                                type="text"
                                defaultValue={this.getInputDefaultText()}
                                placeholder={this.getInputPlaceholderText()}
                                onChange={this.nameInputChangeHandler}
                            />
                            <FormControl.Feedback/>
                            <HelpBlock>
                                &#8203;
                                <Fade
                                    in={formValidationInformation.hint.length > 0}
                                >
                                    <span>
                                        {formValidationInformation.hint}
                                        </span>
                                </Fade>
                            </HelpBlock>
                        </FormGroup>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            type="submit"
                            bsStyle={formValidationInformation.buttonBsStyle}
                            disabled={formValidationInformation.buttonDisabled}
                        >
                            {this.getSubmitButtonText()}
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>
        );
    }
}