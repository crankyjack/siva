import {VocabularySymbol} from "./Vocabulary/vocabulary-symbol";
import MainState from "../../App/flux/main-state";
import {default as Expression} from "./KnowledgeBase/Expression/expression";
import KnowledgeBase from "./KnowledgeBase/knowledge-base";
import {NormalizedStateIdCategory} from "../../Common/utils/util";
import {default as Vocabulary} from "./Vocabulary/vocabulary";

export interface DLDenotationState extends MainState {
    DLKnowledgeBases: NormalizedStateIdCategory<KnowledgeBase>,
    DLVocabularies: NormalizedStateIdCategory<Vocabulary>,
    DLExpressions: NormalizedStateIdCategory<Expression>,
    DLVocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>
}