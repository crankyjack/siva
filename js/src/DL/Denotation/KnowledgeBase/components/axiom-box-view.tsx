import * as React from "react";
import Latex from "react-latex";
import {Glyphicon, Table} from "react-bootstrap";
import Expression from "../Expression/expression";
import Vocabulary from "../../Vocabulary/vocabulary";
import AxiomEditControl from "./axiom-edit-control";
import autobind from "autobind-decorator";
import AxiomView from "./axiom-view";
import {NormalizedStateIdCategory} from "../../../../Common/utils/util";
import {AxiomBoxProperties, AxiomBoxPropertiesType} from "../knowledge-base";
import MainState from "../../../../App/flux/main-state";
import {Dispatch} from "redux";
import {VocabularySymbol} from "../../Vocabulary/vocabulary-symbol";
import KnowledgeBaseView from "./knowledge-base-view";
import {MainComponent} from "../../../../App/components/main-component";

export interface AxiomBoxViewProps {
    knowledgeBaseId: number,
    axiomCategory: keyof AxiomBoxPropertiesType,
    axiomIds: number[]
    expressions: NormalizedStateIdCategory<Expression>
    vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>
    vocabulary: Vocabulary,
    fillAndShowModal: typeof MainComponent.prototype.fillAndShowModal
    dispatch: Dispatch<MainState>
    toggleActiveEditControlById: typeof KnowledgeBaseView.prototype.toggleActiveEditControlById
    setActiveAxiomEditControl: typeof KnowledgeBaseView.prototype.setActiveAxiomEditControl
    activeAxiomEditControlId: string | undefined
    knowledgeBaseEditable: boolean
}

export interface AxiomBoxViewState {

}

export default class AxiomBoxView extends React.PureComponent<AxiomBoxViewProps, AxiomBoxViewState> {
    get axiomBoxProperties () {
        return AxiomBoxProperties[this.props.axiomCategory];
    }

    @autobind
    handlePlusSignClick () {
        this.props.toggleActiveEditControlById(this.axiomBoxProperties.LABEL);
    }

    render () {
        let tableRows;
        if (this.props.axiomIds.length > 0) {
            tableRows = this.props.axiomIds.map((id, index) => {
                return (
                    <AxiomView
                        key={id}
                        knowledgeBaseId={this.props.knowledgeBaseId}
                        axiomCategory={this.props.axiomCategory}
                        axiomIndex={index}
                        axiomId={id}
                        expressions={this.props.expressions}
                        vocabularySymbols={this.props.vocabularySymbols}
                        vocabulary={this.props.vocabulary}
                        fillAndShowModal={this.props.fillAndShowModal}
                        expectedExpressionCategory={this.axiomBoxProperties.AXIOM_CATEGORY}
                        dispatch={this.props.dispatch}
                        setActiveAxiomEditControl={this.props.setActiveAxiomEditControl}
                        activeAxiomEditControlId={this.props.activeAxiomEditControlId}
                        toggleActiveEditControlById={this.props.toggleActiveEditControlById}
                        editable={this.props.knowledgeBaseEditable}
                    />
                );
            });
        }
        else {
            tableRows = (
                <tr>
                    <td>
                        (empty)
                    </td>
                </tr>
            );
        }

        return (
            <div
                className="axiom-collection"
            >
                <span>
                    <Latex
                        children={`$\\text{\\textit{${this.axiomBoxProperties.LABEL}:}}$`}
                    />
                    {
                        !this.props.knowledgeBaseEditable ||
                        <Glyphicon
                            className="add-icon"
                            glyph="plus-sign"
                            onClick={this.handlePlusSignClick}
                        />
                    }
                </span>
                {
                    this.props.knowledgeBaseEditable && this.props.activeAxiomEditControlId === this.axiomBoxProperties.LABEL ?
                        <AxiomEditControl
                            label={"New axiom:"}
                            knowledgeBaseId={this.props.knowledgeBaseId}
                            axiomCategory={this.props.axiomCategory}
                            id={this.axiomBoxProperties.LABEL}
                            expectedExpressionCategory={this.axiomBoxProperties.AXIOM_CATEGORY}
                            dispatch={this.props.dispatch}
                            setActiveAxiomEditControl={this.props.setActiveAxiomEditControl}
                            toggleActiveEditControlById={this.props.toggleActiveEditControlById}
                        /> :
                        null
                }
                <Table
                    className="axiom-table"
                    hover={this.props.axiomIds.length > 0}
                >
                    <tbody>
                    {
                        tableRows
                    }
                    </tbody>
                </Table>
            </div>
        );
    }
}