import * as React from "react";
import AxiomCollectionView from "./axiom-box-view";
import {default as Expression} from "../Expression/expression";
import KnowledgeBase from "../knowledge-base";
import autobind from "autobind-decorator";
import SymbolKeyboard from "../Expression/components/symbol-keyboard";
import {NormalizedStateIdCategory} from "../../../../Common/utils/util";
import Vocabulary from "../../Vocabulary/vocabulary";
import MainState from "../../../../App/flux/main-state";
import {Dispatch} from "redux";
import {VocabularySymbol} from "../../Vocabulary/vocabulary-symbol";
import AxiomEditControl from "./axiom-edit-control";
import {MainComponent} from "../../../../App/components/main-component";

type KnowledgeBaseViewProps = {
    knowledgeBase: KnowledgeBase,
    vocabulary: Vocabulary,
    expressions: NormalizedStateIdCategory<Expression>,
    vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>
    dispatch: Dispatch<MainState>
    fillAndShowModal: typeof MainComponent.prototype.fillAndShowModal
    knowledgeBaseEditable: boolean
}

type KnowledgeBaseViewState = {
    activeAxiomEditControlId: string | undefined
    activeAxiomEditControl: AxiomEditControl | undefined
}

export default class KnowledgeBaseView extends React.PureComponent<KnowledgeBaseViewProps, KnowledgeBaseViewState> {
    constructor (props: KnowledgeBaseViewProps) {
        super(props);

        this.state = {
            activeAxiomEditControlId: undefined,
            activeAxiomEditControl: undefined
        };
    }

    @autobind
    setActiveAxiomEditControl (axiomEditControl: AxiomEditControl) {
        this.setState({
            activeAxiomEditControl: axiomEditControl
        });
    }

    @autobind
    toggleActiveEditControlById (id: string) {
        if (this.state.activeAxiomEditControlId === id) {
            this.setState({
                activeAxiomEditControlId: undefined
            });
        }
        else {
            this.setState({
                activeAxiomEditControlId: id
            });
        }
    }

    @autobind
    handleSymbolKeyboardSymbolClick (symbol: string) {
        this.state.activeAxiomEditControl!.refs.expressionEditControl.insert(symbol);
    }

    render () {
        return (
            <div
                className="tab-pane-content"
            >
                {
                    this.state.activeAxiomEditControlId && this.state.activeAxiomEditControl ?
                        <SymbolKeyboard
                            onSymbolClick={this.handleSymbolKeyboardSymbolClick}
                        /> :
                        null
                }
                <div
                    className="axiom-collection-wrapper"
                >
                    <AxiomCollectionView
                        knowledgeBaseId={this.props.knowledgeBase.id}
                        axiomCategory="TBOX"
                        axiomIds={this.props.knowledgeBase.tBoxAxiomIds}
                        expressions={this.props.expressions}
                        vocabularySymbols={this.props.vocabularySymbols}
                        vocabulary={this.props.vocabulary}
                        fillAndShowModal={this.props.fillAndShowModal}
                        dispatch={this.props.dispatch}
                        setActiveAxiomEditControl={this.setActiveAxiomEditControl}
                        toggleActiveEditControlById={this.toggleActiveEditControlById}
                        activeAxiomEditControlId={this.state.activeAxiomEditControlId}
                        knowledgeBaseEditable={this.props.knowledgeBaseEditable}
                    />
                    <AxiomCollectionView
                        knowledgeBaseId={this.props.knowledgeBase.id}
                        axiomCategory="ABOX"
                        axiomIds={this.props.knowledgeBase.aBoxAxiomIds}
                        expressions={this.props.expressions}
                        vocabularySymbols={this.props.vocabularySymbols}
                        vocabulary={this.props.vocabulary}
                        fillAndShowModal={this.props.fillAndShowModal}
                        dispatch={this.props.dispatch}
                        setActiveAxiomEditControl={this.setActiveAxiomEditControl}
                        toggleActiveEditControlById={this.toggleActiveEditControlById}
                        activeAxiomEditControlId={this.state.activeAxiomEditControlId}
                        knowledgeBaseEditable={this.props.knowledgeBaseEditable}
                    />
                </div>
            </div>
        );
    }
}