import Expression, {ExpressionCategory} from "./expression";
import {default as ExpressionRouter, ExpressionPrecedence, ExpressionUpdate} from "./utils/expression-router";
import {ErrorExpressionHelper, UnknownSymbolError, WrongAlphabetError} from "./error-expression";
import {NormalizedStateIdCategory, pushToArrayIfAbsent} from "../../../../Common/utils/util";
import {VocabularySymbol} from "../../Vocabulary/vocabulary-symbol";
import Vocabulary, {findVocabularySymbolBySymbol, VocabularyAlphabet} from "../../Vocabulary/vocabulary";

export default interface Individual extends Expression {
    symbolId: number
}

export class IndividualHelper {
    static readonly EXPRESSION_TYPE = "Individual";
    static readonly CATEGORY = ExpressionCategory.INDIVIDUAL;
    static readonly PRECEDENCE = ExpressionPrecedence.VOCABULARY_SYMBOL;

    static create (symbolId: number, id?: number) {
        return {
            id: id,
            symbolId,
            category: this.CATEGORY,
            type: this.EXPRESSION_TYPE
        } as Individual;
    }

    static formatName (name: string) {
        return name;
    }

    static pushRelatedExpressionIdsToArray (individual: Individual, expressions: NormalizedStateIdCategory<Individual>, accumulator: number[]): void {
        pushToArrayIfAbsent(individual.id, accumulator);
    }

    static getLatexString (individual: Individual, expressions: NormalizedStateIdCategory<Individual>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return `\\text{\\texttt{${vocabularySymbols.byId[individual.symbolId].symbol}}}`;
    }

    static getSimpleString (individual: Individual, expressions: NormalizedStateIdCategory<Individual>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return vocabularySymbols.byId[individual.symbolId].symbol;
    }

    static getUniqueString (individual: Individual): string {
        return `${this.EXPRESSION_TYPE}(${individual.symbolId})`;
    }

    static getNormalizedUniqueString (individual: Individual, expressions: NormalizedStateIdCategory<Expression>): string {
        return `${this.EXPRESSION_TYPE}(${individual.symbolId})`;
    }

    static parseString (expression: string, expressions: NormalizedStateIdCategory<Individual>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>, vocabulary: Vocabulary): ExpressionUpdate | null {
        if (ExpressionRouter.validateSymbol(expression)) {
            let parsedExpression: Expression;
            expression = this.formatName(expression);

            const symbolInformation = findVocabularySymbolBySymbol(expression, vocabularySymbols, vocabulary);

            if (symbolInformation) {
                if (symbolInformation.vocabularyAlphabet === VocabularyAlphabet.INDIVIDUAL) {
                    parsedExpression = this.create(symbolInformation.vocabularySymbol.id);
                }
                else {
                    parsedExpression = ErrorExpressionHelper.create(WrongAlphabetError, `The symbol "${expression}" does not belong to the alphabet of individual names`, expression);
                }
            }
            else {
                parsedExpression = ErrorExpressionHelper.create(UnknownSymbolError, `The vocabulary doesn't contain an individual named ${expression}`, expression);
            }

            return ExpressionRouter.getExpressionUpdate(parsedExpression, expressions);
        }

        return null;
    }
}

ExpressionRouter.registerHelper(IndividualHelper);