import Expression, {ExpressionCategory} from "./expression";
import {default as ExpressionRouter, ExpressionPrecedence, ExpressionUpdate} from "./utils/expression-router";
import TableauOrNodeChoiceModal, {TableauOrNodeChoiceModalProps} from "../../../../Tableau/components/tableau-or-node-choice-modal";
import {TableauState} from "../../../../Tableau/tableau";
import ConceptRouter, {ConceptTableauRuleNode, TableauNodeType} from "./utils/concept-router";
import Vocabulary from "../../Vocabulary/vocabulary";
import {VocabularySymbol} from "../../Vocabulary/vocabulary-symbol";
import {findFirstGlobalCharOccurence, NormalizedStateIdCategory, pushToArrayIfAbsent} from "../../../../Common/utils/util";
import {IntersectionHelper} from "./intersection";
import TableauRuleRouter from "../../../../Tableau/utils/tableau-rule-router";
import CTreeNode from "../../../CTreeTableauReasoning/CompletionTree/Node/c-tree-node";
import CTreeEdge from "../../../CTreeTableauReasoning/CompletionTree/Edge/c-tree-edge";
import CTREE_HELPER_FUNCTIONS from "../../../CTreeTableauReasoning/CompletionTree/utils/c-tree-helper-functions";
import CTreeView from "../../../CTreeTableauReasoning/CompletionTree/components/c-tree-view";
import {CTreeState, CTreeTableauNodeDescriptionState} from "../../../CTreeTableauReasoning/CompletionTree/c-tree";
import DL_CTREE_TABLEAU_WORKSPACE_ACTIONS from "../../../CTreeTableauReasoning/CompletionTree/DLCTreeTableauWorkspace/flux/dl-ctree-tableau-workspace-actions";

export default interface Union extends Expression {
    concept1Id: number,
    concept2Id: number
}

export class UnionHelper {
    static readonly TABLEAU_NODE_TYPE = TableauNodeType.OR;
    static readonly TABLEAU_ACTION_TYPE = "CTreeTableauUnionRule";
    static readonly EXPRESSION_TYPE = "Union";
    static readonly CATEGORY = ExpressionCategory.CONCEPT;
    static readonly PRECEDENCE = ExpressionPrecedence.BINARY_LOW;
    static readonly SPECIAL_SYMBOLS = {
        UNION: {
            symbol: "\u2294",
            latex: "\\sqcup"
        }
    };

    static create (concept1Id: number, concept2Id: number, id?: number) {
        return {
            id: id,
            concept1Id,
            concept2Id,
            category: this.CATEGORY,
            type: this.EXPRESSION_TYPE
        } as Union;
    }

    static pushRelatedExpressionIdsToArray (union: Union, expressions: NormalizedStateIdCategory<Expression>, accumulator: number[]): void {
        if (pushToArrayIfAbsent(union.id, accumulator)) {
            ExpressionRouter.pushRelatedExpressionIdsToArray(union.concept1Id, expressions, accumulator);
            ExpressionRouter.pushRelatedExpressionIdsToArray(union.concept2Id, expressions, accumulator);
        }
    }

    static getLatexString (union: Union, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return `${ExpressionRouter.getLatexString(union.concept1Id, expressions, vocabularySymbols, this.EXPRESSION_TYPE)} ${this.SPECIAL_SYMBOLS.UNION.latex} ${ExpressionRouter.getLatexString(union.concept2Id, expressions, vocabularySymbols, this.EXPRESSION_TYPE)}`;
    }

    static getSimpleString (union: Union, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return `${ExpressionRouter.getSimpleString(union.concept1Id, expressions, vocabularySymbols, this.EXPRESSION_TYPE)} ${this.SPECIAL_SYMBOLS.UNION.symbol} ${ExpressionRouter.getSimpleString(union.concept2Id, expressions, vocabularySymbols, this.EXPRESSION_TYPE)}`;
    }

    static getUniqueString (union: Union): string {
        return `${this.EXPRESSION_TYPE}(${union.concept1Id}, ${union.concept2Id})`;
    }

    static getNormalizedUniqueString (union: Union, expressions: NormalizedStateIdCategory<Expression>): string {
        const sortedIds = [union.concept1Id, union.concept2Id].sort();
        return `${this.EXPRESSION_TYPE}(${ExpressionRouter.getNormalizedUniqueString(sortedIds[0], expressions)},${ExpressionRouter.getNormalizedUniqueString(sortedIds[1], expressions)})`;
    }

    static parseString (expression: string, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>, vocabulary: Vocabulary): ExpressionUpdate | null {
        const index = findFirstGlobalCharOccurence(this.SPECIAL_SYMBOLS.UNION.symbol, expression);

        if (index !== -1) {
            const parsedConcept1 = ExpressionRouter.parseString(expression.substr(0, index), expressions, vocabularySymbols, vocabulary, ExpressionCategory.CONCEPT);
            const parsedConcept2 = ExpressionRouter.parseString(expression.substr(index + 1), parsedConcept1.expressions, vocabularySymbols, vocabulary, ExpressionCategory.CONCEPT);

            const parsedExpression = this.create(parsedConcept1.id, parsedConcept2.id);

            return ExpressionRouter.getExpressionUpdate(parsedExpression, parsedConcept2.expressions);
        }
        return null;
    }

    static getNNFExpressionUpdate (union: Union, expressions: NormalizedStateIdCategory<Expression>): ExpressionUpdate {
        const nnfConcept1ExpressionUpdate = ConceptRouter.getNNFExpressionUpdate(union.concept1Id, expressions);
        const nnfConcept2ExpressionUpdate = ConceptRouter.getNNFExpressionUpdate(union.concept2Id, nnfConcept1ExpressionUpdate.expressions);

        return ExpressionRouter.getExpressionUpdate(this.create(nnfConcept1ExpressionUpdate.id, nnfConcept2ExpressionUpdate.id), nnfConcept2ExpressionUpdate.expressions);
    }

    static getNegatedNNFExpressionUpdate (union: Union, expressions: NormalizedStateIdCategory<Expression>): ExpressionUpdate {
        const negNnfConcept1ExpressionUpdate = ConceptRouter.getNegatedNNFExpressionUpdate(union.concept1Id, expressions);
        const negNnfConcept2ExpressionUpdate = ConceptRouter.getNegatedNNFExpressionUpdate(union.concept2Id, negNnfConcept1ExpressionUpdate.expressions);

        return ExpressionRouter.getExpressionUpdate(IntersectionHelper.create(negNnfConcept1ExpressionUpdate.id, negNnfConcept2ExpressionUpdate.id), negNnfConcept2ExpressionUpdate.expressions);
    }

    static getNewApplicableCTreeTableauConceptRuleNodes (cTreeNodeId: number, conceptId: number, cTreeNodes: NormalizedStateIdCategory<CTreeNode>, cTreeEdges: NormalizedStateIdCategory<CTreeEdge>, expressions: NormalizedStateIdCategory<Expression>): UnionTableauRuleNode[] {
        const union = expressions.byId[conceptId] as Union;

        const result = [];

        if (typeof CTREE_HELPER_FUNCTIONS.getEqualConceptInNodeLabel(cTreeNodes.byId[cTreeNodeId], union.concept1Id, expressions) === "undefined") {
            result.push({
                id: NaN,
                actionType: this.TABLEAU_ACTION_TYPE,
                childrenIds: [],
                state: undefined,
                conceptId: union.id,
                cTreeNodeId: cTreeNodeId,
                newConceptId: union.concept1Id
            });
        }
        else {
            return [];
        }

        if (typeof CTREE_HELPER_FUNCTIONS.getEqualConceptInNodeLabel(cTreeNodes.byId[cTreeNodeId], union.concept2Id, expressions) === "undefined") {
            if (union.concept1Id !== union.concept2Id) {
                result.push({
                    id: NaN,
                    actionType: this.TABLEAU_ACTION_TYPE,
                    childrenIds: [],
                    state: undefined,
                    conceptId: union.id,
                    cTreeNodeId: cTreeNodeId,
                    newConceptId: union.concept2Id
                });
            }
        }
        else {
            return [];
        }

        return result;
    }

    static getConceptTableauRuleDescription (union: Union, cTreeNode: CTreeNode, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        const concept1String = ExpressionRouter.getSimpleString(union.concept1Id, expressions, vocabularySymbols);
        const concept2String = ExpressionRouter.getSimpleString(union.concept2Id, expressions, vocabularySymbols);
        return `Insert "${concept1String}" or "${concept2String}" into label of ${CTREE_HELPER_FUNCTIONS.getNodeName(cTreeNode.nameOrIndividualSymbolId, vocabularySymbols)} node.`;
    }

    static onConceptTableauExpand (cTreeNodeId: number, conceptId: number, cTreeView: CTreeView): void {
        const cTreeNodeName = CTREE_HELPER_FUNCTIONS.getNodeName(cTreeView.props.filteredCTreeNodes.byId[cTreeNodeId].nameOrIndividualSymbolId, cTreeView.vocabularySymbols);
        const union = cTreeView.props.expressions.byId[conceptId] as Union;
        const unionString = ExpressionRouter.getSimpleString(conceptId, cTreeView.props.expressions, cTreeView.props.filteredVocabularySymbols);
        const concept1String = ExpressionRouter.getSimpleString(union.concept1Id, cTreeView.props.expressions, cTreeView.props.filteredVocabularySymbols);
        const concept2String = ExpressionRouter.getSimpleString(union.concept2Id, cTreeView.props.expressions, cTreeView.props.filteredVocabularySymbols);

        const tableauNodes = this.getNewApplicableCTreeTableauConceptRuleNodes(cTreeNodeId, conceptId, cTreeView.props.filteredCTreeNodes, cTreeView.props.filteredCTreeEdges, cTreeView.props.expressions);

        const concept1Index = tableauNodes.findIndex(node => node.newConceptId === union.concept1Id);
        const concept2Index = tableauNodes.findIndex(node => node.newConceptId === union.concept2Id);

        cTreeView.props.fillAndShowModal(TableauOrNodeChoiceModal, {
            title: `Choice of ${this.SPECIAL_SYMBOLS.UNION.symbol}-rule application on "${unionString}" in ${cTreeNodeName}`,
            options: [
                {
                    index: concept1Index,
                    text: concept1String,
                    disabled: concept1Index === -1
                },
                {
                    index: concept2Index,
                    text: concept2String,
                    disabled: concept2Index === -1
                }
            ],
            chooseHandler: option => {
                cTreeView.props.dispatch(DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.CREATE_APPLY_CONCEPT_TABLEAU_OR_RULE(cTreeNodeId, conceptId, option.index));
            }
        } as Partial<TableauOrNodeChoiceModalProps>);
    }

    static applyTableauRule<S extends TableauState> (unionTableauRuleNode: UnionTableauRuleNode, state: S & CTreeState): S {
        state = CTREE_HELPER_FUNCTIONS.extendNodeLabel(state, unionTableauRuleNode.cTreeNodeId, unionTableauRuleNode.newConceptId);
        return state;
    }

    static revertTableauRule<S extends TableauState> (unionTableauRuleNode: UnionTableauRuleNode, state: S & CTreeState): S {
        state = CTREE_HELPER_FUNCTIONS.removeFromNodeLabel(state, unionTableauRuleNode.cTreeNodeId, unionTableauRuleNode.newConceptId);
        return state;
    }

    static getTableauNodeLabelText (unionTableauRuleNode: UnionTableauRuleNode, tableauNodeDescriptionState: CTreeTableauNodeDescriptionState): string {
        const cTreeNodeName = tableauNodeDescriptionState.DLCTreeNodes.byId[unionTableauRuleNode.cTreeNodeId] ?
            CTREE_HELPER_FUNCTIONS.getNodeName(tableauNodeDescriptionState.DLCTreeNodes.byId[unionTableauRuleNode.cTreeNodeId].nameOrIndividualSymbolId, tableauNodeDescriptionState.DLVocabularySymbols) :
            "";

        const concept = tableauNodeDescriptionState.DLExpressions.byId[unionTableauRuleNode.conceptId] as Union;

        let subConcept1String = ExpressionRouter.getSimpleString(concept.concept1Id, tableauNodeDescriptionState.DLExpressions, tableauNodeDescriptionState.DLVocabularySymbols);
        let subConcept2String = ExpressionRouter.getSimpleString(concept.concept2Id, tableauNodeDescriptionState.DLExpressions, tableauNodeDescriptionState.DLVocabularySymbols);

        let applicationWayOrder;

        if (unionTableauRuleNode.newConceptId === concept.concept1Id) {
            applicationWayOrder = 1;
            subConcept1String = `<b>${subConcept1String}</b>`;
        }
        else {
            applicationWayOrder = 2;
            subConcept2String = `<b>${subConcept2String}</b>`;
        }

        return `<b>\u2022 ${cTreeNodeName}</b>\n${this.SPECIAL_SYMBOLS.UNION.symbol}-rule\n(${applicationWayOrder})`;
    }
}

interface UnionTableauRuleNode extends ConceptTableauRuleNode {
    newConceptId: number
}

ExpressionRouter.registerHelper(UnionHelper);
ConceptRouter.registerHelper(UnionHelper);
TableauRuleRouter.registerHelper(UnionHelper);