import Expression, {ExpressionCategory} from "./expression";
import {default as ExpressionRouter, ExpressionPrecedence, ExpressionUpdate} from "./utils/expression-router";
import ConceptRouter, {ConceptTableauRuleNode, TableauNodeType} from "./utils/concept-router";
import {NormalizedStateIdCategory, pushToArrayIfAbsent} from "../../../../Common/utils/util";
import {VocabularySymbol} from "../../Vocabulary/vocabulary-symbol";
import Vocabulary from "../../Vocabulary/vocabulary";
import CTreeNode from "../../../CTreeTableauReasoning/CompletionTree/Node/c-tree-node";
import CTreeEdge from "../../../CTreeTableauReasoning/CompletionTree/Edge/c-tree-edge";
import CTreeView from "../../../CTreeTableauReasoning/CompletionTree/components/c-tree-view";

export default interface ConceptComplement extends Expression {
    conceptId: number
}

export class ConceptComplementHelper {
    static readonly TABLEAU_NODE_TYPE = TableauNodeType.NONE;
    static readonly EXPRESSION_TYPE = "ConceptComplement";
    static readonly CATEGORY = ExpressionCategory.CONCEPT;
    static readonly PRECEDENCE = ExpressionPrecedence.UNARY;
    static readonly SPECIAL_SYMBOLS = {
        NEGATION: {
            symbol: "\u00AC",
            latex: "\\neg"
        }
    };

    static create (conceptId: number, id?: number) {
        return {
            id: id,
            conceptId,
            category: this.CATEGORY,
            type: this.EXPRESSION_TYPE
        } as ConceptComplement;
    }

    static pushRelatedExpressionIdsToArray (conceptComplement: ConceptComplement, expressions: NormalizedStateIdCategory<Expression>, accumulator: number[]): void {
        if (pushToArrayIfAbsent(conceptComplement.id, accumulator)) {
            ExpressionRouter.pushRelatedExpressionIdsToArray(conceptComplement.conceptId, expressions, accumulator);
        }
    }

    static getLatexString (conceptComplement: ConceptComplement, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return `${this.SPECIAL_SYMBOLS.NEGATION.latex} ${ExpressionRouter.getLatexString(conceptComplement.conceptId, expressions, vocabularySymbols, this.EXPRESSION_TYPE)}`;
    }

    static getSimpleString (conceptComplement: ConceptComplement, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return `${this.SPECIAL_SYMBOLS.NEGATION.symbol}${ExpressionRouter.getSimpleString(conceptComplement.conceptId, expressions, vocabularySymbols, this.EXPRESSION_TYPE)}`;
    }

    static getUniqueString (conceptComplement: ConceptComplement): string {
        return `${this.EXPRESSION_TYPE}(${conceptComplement.conceptId})`;
    }

    static getNormalizedUniqueString (conceptComplement: ConceptComplement, expressions: NormalizedStateIdCategory<Expression>): string {
        return `${this.EXPRESSION_TYPE}(${ExpressionRouter.getNormalizedUniqueString(conceptComplement.conceptId, expressions)})`;
    }

    static parseString (expression: string, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>, vocabulary: Vocabulary): ExpressionUpdate | null {
        if (expression.charAt(0) === this.SPECIAL_SYMBOLS.NEGATION.symbol) {
            const parsedConcept = ExpressionRouter.parseString(expression.substr(1), expressions, vocabularySymbols, vocabulary, ExpressionCategory.CONCEPT);

            const parsedExpression = this.create(parsedConcept.id);

            return ExpressionRouter.getExpressionUpdate(parsedExpression, parsedConcept.expressions);
        }
        return null;
    }

    static getNNFExpressionUpdate (conceptComplement: ConceptComplement, expressions: NormalizedStateIdCategory<Expression>): ExpressionUpdate {
        return ConceptRouter.getNegatedNNFExpressionUpdate(conceptComplement.conceptId, expressions);
    }

    static getNegatedNNFExpressionUpdate (conceptComplement: ConceptComplement, expressions: NormalizedStateIdCategory<Expression>): ExpressionUpdate {
        return ConceptRouter.getNNFExpressionUpdate(conceptComplement.conceptId, expressions);
    }

    static getNewApplicableCTreeTableauConceptRuleNodes (cTreeNodeId: number, conceptId: number, cTreeNodes: NormalizedStateIdCategory<CTreeNode>, cTreeEdges: NormalizedStateIdCategory<CTreeEdge>, expressions: NormalizedStateIdCategory<Expression>): ConceptTableauRuleNode[] {
        return [];
    }

    static getConceptTableauRuleDescription (conceptComplement: ConceptComplement, cTreeNode: CTreeNode, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return `No tableau rule is applicable to ${this.getSimpleString(conceptComplement, expressions, vocabularySymbols)}.`;
    }

    static onConceptTableauExpand (cTreeNodeId: number, conceptId: number, cTreeView: CTreeView): void {
    }
}

ExpressionRouter.registerHelper(ConceptComplementHelper);
ConceptRouter.registerHelper(ConceptComplementHelper);
