import {default as Expression, ExpressionCategory} from "./expression";
import {default as ExpressionRouter, ExpressionUpdate} from "./utils/expression-router";
import {NormalizedStateIdCategory, pushToArrayIfAbsent} from "../../../../Common/utils/util";
import {VocabularySymbol} from "../../Vocabulary/vocabulary-symbol";
import Vocabulary from "../../Vocabulary/vocabulary";

export default interface ErrorExpression extends Expression {
    errorType: string,
    errorMessage: string,
    errorData?: any
}

export const InvalidExpressionError = "InvalidExpressionNameError";
export const UnknownSymbolError = "UnknownSymbolError";
export const WrongAlphabetError = "WrongAlphabetError";

export class ErrorExpressionHelper {
    static readonly EXPRESSION_TYPE = "Error";
    static readonly CATEGORY = null as any;
    static readonly PRECEDENCE = 0;

    static create (errorType: string, errorMessage: string, errorData?: string, id?: number) {
        return {
            id: id,
            errorType,
            errorMessage,
            errorData,
            category: this.CATEGORY,
            type: this.EXPRESSION_TYPE
        } as ErrorExpression;
    }

    static pushRelatedExpressionIdsToArray (errorExpression: ErrorExpression, expressions: NormalizedStateIdCategory<Expression>, accumulator: number[]): void {
        pushToArrayIfAbsent(errorExpression.id, accumulator);
    }

    static getLatexString (errorExpression: ErrorExpression, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return `\\mathbf{\\color{#DD4444} ${ExpressionRouter.replaceSpecialLatexSymbols(errorExpression.errorData)}}`;
    }

    static getSimpleString (errorExpression: ErrorExpression, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return `${errorExpression.errorData}`;
    }

    static getUniqueString (errorExpression: ErrorExpression): string {
        return `${this.EXPRESSION_TYPE}(${
            errorExpression.errorType.replace(",", "\\,"
            )}, ${
            errorExpression.errorMessage.replace(",", "\\,"
            )}, ${
            errorExpression.errorData.replace(",", "\\,"
            )})`;
    }

    static getNormalizedUniqueString (errorExpression: ErrorExpression, expressions: NormalizedStateIdCategory<Expression>): string {
        return this.getUniqueString(errorExpression);
    }

    static parseString (expression: string, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>, vocabulary: Vocabulary): ExpressionUpdate | null {
        return null;
    }

    static invalidExpressionParse (expression: string, expectedExpressionCategory: ExpressionCategory, expressions: NormalizedStateIdCategory<Expression>) {
        const parsedExpression = this.create(InvalidExpressionError, `"${expression}" is an invalid ${expectedExpressionCategory} expression`, expression);

        return ExpressionRouter.getExpressionUpdate(parsedExpression, expressions);
    }
}