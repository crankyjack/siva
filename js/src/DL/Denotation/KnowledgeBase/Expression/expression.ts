export default interface Expression {
    id: number,
    category: ExpressionCategory
    type: string
}

export enum ExpressionCategory {
    CONCEPT = "concept",
    ROLE = "role",
    INDIVIDUAL = "individual",
    ABOX_AXIOM = "abox axiom",
    TBOX_AXIOM = "tbox axiom",
    RBOX_AXIOM = "rbox axiom"
}
