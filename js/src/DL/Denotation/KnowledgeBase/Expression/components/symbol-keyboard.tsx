import {Button, Row} from "react-bootstrap";
import * as React from "react";
import {MouseEvent} from "react";
import autobind from "autobind-decorator";
import {ConceptComplementHelper} from "../concept-complement";
import {ExistentialQuantificationHelper} from "../existential-quantification";
import {ValueRestrictionHelper} from "../value-restriction";
import {IntersectionHelper} from "../intersection";
import {UnionHelper} from "../union";
import {SubsumptionHelper} from "../subsumption";
import {TopHelper} from "../top";
import {BottomHelper} from "../bottom";

export interface SymbolKeyboardProps {
    onSymbolClick: (symbol: string) => void
}

export default class SymbolKeyboard extends React.PureComponent<SymbolKeyboardProps> {
    @autobind
    handleSymbolMouseDown (symbol: string, event: MouseEvent<Button>) {
        event.preventDefault();
        this.props.onSymbolClick(symbol);
    }

    render () {
        return (
            <Row
                className="dl-keyboard-row"
            >
                <div
                    className="dl-keyboard"
                >
                    {
                        Object.keys(DLKeyboardSymbols).map(key => {
                            return (
                                <SymbolKey
                                    key={key}
                                    symbolKey={key}
                                    onMouseDown={this.handleSymbolMouseDown}
                                />
                            );
                        })
                    }
                </div>
            </Row>
        );
    }
}

interface SymbolKeyProps {
    symbolKey: string,
    onMouseDown: (symbol: string, event: MouseEvent<Button>) => void
}

class SymbolKey extends React.PureComponent<SymbolKeyProps> {
    @autobind
    handleMouseDown (event: MouseEvent<Button>) {
        this.props.onMouseDown(DLKeyboardSymbols[this.props.symbolKey], event);
    }

    render () {
        return (
            <Button
                className="dl-keyboard-char"
                onMouseDown={this.handleMouseDown}
            >
                {DLKeyboardSymbols[this.props.symbolKey]}
            </Button>
        );
    }
}

const DLKeyboardSymbols: {[index: string]: string} = {
    TOP: TopHelper.SPECIAL_SYMBOLS.TOP.symbol,
    BOTTOM: BottomHelper.SPECIAL_SYMBOLS.BOTTOM.symbol,
    NEGATION: ConceptComplementHelper.SPECIAL_SYMBOLS.NEGATION.symbol,
    EXISTS: ExistentialQuantificationHelper.SPECIAL_SYMBOLS.EXISTS.symbol,
    FORALL: ValueRestrictionHelper.SPECIAL_SYMBOLS.FORALL.symbol,
    INTERSECTION: IntersectionHelper.SPECIAL_SYMBOLS.INTERSECTION.symbol,
    UNION: UnionHelper.SPECIAL_SYMBOLS.UNION.symbol,
    SUBSUMPTION: SubsumptionHelper.SPECIAL_SYMBOLS.SUBSUMPTION.symbol,
    COLON: ":"
};