import * as React from "react";
import {FormEvent, KeyboardEvent} from "react";
import {Button, FormControl, InputGroup} from "react-bootstrap";
import {ExpressionCategory} from "../expression";
import autobind from "autobind-decorator";

export interface ExpressionEditControlProps {
    label?: string
    initialValue?: string,
    onCancel?: () => void
    onConfirm?: () => void
    onFocus?: (expressionEditControl: ExpressionEditControl) => void
    onUpdate?: (newValue: string) => void
    onMount?: (expressionEditControl: ExpressionEditControl) => void
    expectedExpressionCategory: ExpressionCategory
}

export interface ExpressionEditControlState {
}

export default class ExpressionEditControl extends React.PureComponent<ExpressionEditControlProps, ExpressionEditControlState> {
    input: HTMLInputElement;

    constructor (props: ExpressionEditControlProps) {
        super(props);

        this.state = {};
    }

    componentDidMount (): void {
        !this.props.onMount || this.props.onMount(this);
    }

    @autobind
    handleFocus () {
        !this.props.onFocus || this.props.onFocus(this);
    }

    @autobind
    insert (text: string) {
        const prefix = this.input.value.substring(0, this.input.selectionStart);
        const suffix = this.input.value.substring(this.input.selectionEnd);
        this.input.value = prefix + text + suffix;
        this.input.setSelectionRange(prefix.length + 1, prefix.length + 1);
        this.input.focus();
    }

    @autobind
    handleConfirm (e: FormEvent<HTMLFormElement>) {
        !e || e.preventDefault();
        !this.props.onConfirm || this.props.onConfirm();
    }

    @autobind
    handleCancel () {
        !this.props.onCancel || this.props.onCancel();
    }

    @autobind
    handleChange () {
        !this.props.onUpdate || this.props.onUpdate(this.input.value);
    }

    @autobind
    handleKeyDown (event: KeyboardEvent<FormControl>) {
        if (event.keyCode === 27) {
            this.handleCancel();
        }
    }

    render () {
        const body = (
            <InputGroup>
                {
                    this.props.label ?
                        <InputGroup.Addon>
                            {this.props.label}
                        </InputGroup.Addon> :
                        null
                }
                <FormControl
                    inputRef={input => this.input = input!}
                    type="text"
                    defaultValue={this.props.initialValue || ""}
                    onFocus={this.handleFocus}
                    onKeyDown={this.handleKeyDown}
                    onChange={this.handleChange}

                />
                {
                    this.props.onCancel || this.props.onConfirm ?
                        <InputGroup.Button>
                            {
                                this.props.onCancel ?
                                    <Button
                                        onClick={this.handleCancel}
                                    >
                                        &times;
                                    </Button> :
                                    null
                            }
                            {
                                this.props.onConfirm ?
                                    <Button
                                        type="submit"
                                    >
                                        Confirm
                                    </Button> :
                                    null
                            }
                        </InputGroup.Button> :
                        null
                }
            </InputGroup>
        );

        if (this.props.onConfirm) {
            return (
                <form
                    className={`expression-edit-control-form`}
                    onSubmit={this.handleConfirm}
                >
                    {body}
                </form>
            );
        }

        return body;
    }
}