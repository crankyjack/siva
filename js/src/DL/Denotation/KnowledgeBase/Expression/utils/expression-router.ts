import Expression, {ExpressionCategory} from "../expression";
import {ErrorExpressionHelper} from "../error-expression";
import {appendNormalizedToState, findInNormalizedStateIdCategory, NormalizedStateIdCategory} from "../../../../../Common/utils/util";
import {VocabularySymbol} from "../../../Vocabulary/vocabulary-symbol";
import Vocabulary from "../../../Vocabulary/vocabulary";

export default class ExpressionRouter {
    private static helpers: Map<string, ExpressionHelper> = new Map();

    static validateSymbol (symbol: string) {
        return /^[a-z][a-z0-9]*$/i.test(symbol);
    }

    static registerHelper (helper: ExpressionHelper) {
        this.helpers.set(helper.EXPRESSION_TYPE, helper);
    }

    static doesNeedParentheses (type: string, parentType?: string) {
        return type !== "Error" && parentType && parentType !== type && (this.helpers.get(type)!.PRECEDENCE) >= (this.helpers.get(parentType)!.PRECEDENCE);
    }

    static stripGlobalParentheses (expression: string) {
        while (expression.length && expression.charAt(0) === "(" && expression.charAt(expression.length - 1) == ")") {
            let count = 1;

            for (let i = 1; i < expression.length - 1; i++) {
                const char = expression.charAt(i);
                if (char === "(") {
                    count++;
                }
                if (char === ")") {
                    count--;

                    if (count === 0) {
                        return expression;
                    }
                }

            }
            expression = expression.substr(1, expression.length - 2);
        }
        return expression;
    }

    static getHelper (expressionType: string) {
        return this.helpers.get(expressionType);
    }

    static pushRelatedExpressionIdsToArray (expressionId: number, expressions: NormalizedStateIdCategory<Expression>, accumulator: number[]): void {
        const expression = expressions.byId[expressionId];
        const helper = this.getHelper(expression.type)!;

        helper.pushRelatedExpressionIdsToArray(expression, expressions, accumulator);
    }

    static getRelatedExpressionIds (expressionId: number, expressions: NormalizedStateIdCategory<Expression>): number[] {
        const result: number[] = [];

        this.pushRelatedExpressionIdsToArray(expressionId, expressions, result);

        return result;
    }

    static getSimpleString (expressionId: number, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>, parentType?: string): string {

        const expression = expressions.byId[expressionId];
        const helper = this.getHelper(expression.type)!;

        let result = helper.getSimpleString(expression, expressions, vocabularySymbols);

        if (ExpressionRouter.doesNeedParentheses(expression.type, parentType)) {
            result = `(${result})`;
        }
        return result;
    }

    static getLatexString (expressionId: number, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>, parentType?: string): string {
        const expression = expressions.byId[expressionId];
        const helper = this.getHelper(expression.type)!;

        let result = helper.getLatexString(expression, expressions, vocabularySymbols);

        if (ExpressionRouter.doesNeedParentheses(expression.type, parentType)) {
            result = `(${result})`;
        }
        return result;
    }

    static getUniqueString (expression: Expression): string {
        const helper = this.getHelper(expression.type)!;
        return helper.getUniqueString(expression);
    }

    static getNormalizedUniqueString (expressionId: number, expressions: NormalizedStateIdCategory<Expression>): string {
        const expression = expressions.byId[expressionId];
        const helper = this.getHelper(expression.type)!;

        return helper.getNormalizedUniqueString(expression, expressions);
    }

    static replaceSpecialLatexSymbols (expression: string) {
        for (const helper of this.helpers.values()) {
            if (helper.SPECIAL_SYMBOLS) {
                for (const key in helper.SPECIAL_SYMBOLS) {
                    expression = expression.replace(helper.SPECIAL_SYMBOLS[key].symbol, ` ${helper.SPECIAL_SYMBOLS[key].latex} `);
                }
            }
        }
        return expression;
    }

    static parseString (expression: string, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>, vocabulary: Vocabulary, expectedExpressionCategory: ExpressionCategory): ExpressionUpdate {
        const helpers = Array.from(this.helpers.values())
            .filter(helper => helper.CATEGORY === expectedExpressionCategory)
            .sort((helper1, helper2) => helper2.PRECEDENCE - helper1.PRECEDENCE);

        while (true) {
            const cleanedExpression = ExpressionRouter.stripGlobalParentheses(expression.trim()).trim();
            if (cleanedExpression === expression) {
                break;
            }
            expression = cleanedExpression;
        }

        for (const helper of helpers) {
            const result = helper.parseString(expression, expressions, vocabularySymbols, vocabulary);
            if (result) {
                return result;
            }
        }

        return ErrorExpressionHelper.invalidExpressionParse(expression, expectedExpressionCategory, expressions);
    }

    static getExpressionUpdate (expression: Expression, expressions: NormalizedStateIdCategory<Expression>): ExpressionUpdate {
        const identifier = this.getUniqueString(expression);

        let desired = findInNormalizedStateIdCategory(expressions, exp =>
            this.getUniqueString(exp) === identifier
        );

        if (!desired) {
            desired = expression;
            expressions = appendNormalizedToState(expressions, [], expression);
        }

        return {
            id: desired.id,
            expressions: expressions
        };
    }
}

ExpressionRouter.registerHelper(ErrorExpressionHelper);

export interface ExpressionHelper {
    EXPRESSION_TYPE: string;
    CATEGORY: ExpressionCategory;
    PRECEDENCE: ExpressionPrecedence;
    SPECIAL_SYMBOLS?: {
        [index: string]: {
            latex: string
            symbol: string
        }
    }

    pushRelatedExpressionIdsToArray (expression: Expression, expressions: NormalizedStateIdCategory<Expression>, accumulator: number[]): void;

    getLatexString (expression: Expression, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string;

    parseString (expression: string, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>, vocabulary: Vocabulary): ExpressionUpdate | null;

    getSimpleString (expression: Expression, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string;

    getUniqueString (expression: Expression): string;

    getNormalizedUniqueString (expression: Expression, expressions: NormalizedStateIdCategory<Expression>): string
}

export enum ExpressionPrecedence {
    VOCABULARY_SYMBOL = 1,
    UNARY = 2,
    BINARY_HIGH = 3,
    BINARY_LOW = 4,
    AXIOM = 5
}

export interface ExpressionUpdate {
    id: number,
    expressions: NormalizedStateIdCategory<Expression>
}
