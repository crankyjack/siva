import {filterNormalizedStateIdCategory, mapNormalizedStateIdCategory, NormalizedStateIdCategory} from "../../../../../Common/utils/util";
import {ExpressionHelper, ExpressionUpdate} from "./expression-router";
import TableauNode from "../../../../../Tableau/tableau-node";
import {Tableau} from "../../../../../Tableau/tableau";
import {VocabularySymbol} from "../../../Vocabulary/vocabulary-symbol";
import {default as Expression} from "../expression";
import {TableauRuleHelper} from "../../../../../Tableau/utils/tableau-rule-router";
import TABLEAU_ACTIONS from "../../../../../Tableau/flux/tableau-actions";
import ConfirmationModal from "../../../../../Common/components/confirmation-modal";
import CTreeEdge from "../../../../CTreeTableauReasoning/CompletionTree/Edge/c-tree-edge";
import CTreeNode from "../../../../CTreeTableauReasoning/CompletionTree/Node/c-tree-node";
import CTreeView from "../../../../CTreeTableauReasoning/CompletionTree/components/c-tree-view";

export default class ConceptRouter {
    private static helpers: Map<string, ExpressionHelper & ConceptHelper> = new Map();

    static registerHelper (helper: ExpressionHelper & ConceptHelper) {
        this.helpers.set(helper.EXPRESSION_TYPE, helper);
    }

    static getHelper (conceptType: string) {
        return this.helpers.get(conceptType);
    }

    static getNNFExpressionUpdate (conceptId: number, expressions: NormalizedStateIdCategory<Expression>): ExpressionUpdate {
        const concept = expressions.byId[conceptId];
        const helper = this.getHelper(concept.type)!;

        return helper.getNNFExpressionUpdate(concept, expressions);
    }

    static getNegatedNNFExpressionUpdate (conceptId: number, expressions: NormalizedStateIdCategory<Expression>): ExpressionUpdate {
        const concept = expressions.byId[conceptId];
        const helper = this.getHelper(concept.type)!;

        return helper.getNegatedNNFExpressionUpdate(concept, expressions);
    }

    static getNewApplicableTableauRuleNodes (cTreeNodeId: number, conceptId: number, cTreeNodes: NormalizedStateIdCategory<CTreeNode>, cTreeEdges: NormalizedStateIdCategory<CTreeEdge>, expressions: NormalizedStateIdCategory<Expression>) {
        const concept = expressions.byId[conceptId];
        const helper = this.getHelper(concept.type)!;
        return helper.getNewApplicableCTreeTableauConceptRuleNodes(cTreeNodeId, conceptId, cTreeNodes, cTreeEdges, expressions);
    }

    static isTableauRuleApplicable (cTreeNodeId: number, conceptId: number, cTreeNodes: NormalizedStateIdCategory<CTreeNode>, cTreeEdges: NormalizedStateIdCategory<CTreeEdge>, expressions: NormalizedStateIdCategory<Expression>) {
        return this.getNewApplicableTableauRuleNodes(cTreeNodeId, conceptId, cTreeNodes, cTreeEdges, expressions).length > 0;
    }

    static doActiveTableauNodeChildrenMatchConcept (concept: Expression, cTreeNodeId: number, activeTableauNode: TableauNode, tableauNodes: NormalizedStateIdCategory<TableauNode>) {
        const helper = this.getHelper(concept.type)! as any as TableauRuleHelper;

        return Boolean(activeTableauNode.childrenIds.find(childId => {
            const child = tableauNodes.byId[childId];
            return child.actionType === helper.TABLEAU_ACTION_TYPE &&
                (child as ConceptTableauRuleNode).conceptId === concept.id &&
                (child as ConceptTableauRuleNode).cTreeNodeId === cTreeNodeId;
        }));
    }

    static getCurrentConceptTableauRuleNodes (tableau: Tableau, cTreeNodeId: number, conceptId: number, tableauNodes: NormalizedStateIdCategory<TableauNode>, cTreeNodes: NormalizedStateIdCategory<CTreeNode>, cTreeEdges: NormalizedStateIdCategory<CTreeEdge>, expressions: NormalizedStateIdCategory<Expression>): ConceptTableauRuleNode[] {
        const concept = expressions.byId[conceptId];
        const helper = this.getHelper(concept.type)!;
        if (helper.TABLEAU_NODE_TYPE === TableauNodeType.NONE) {
            return [];
        }

        const childNodes = filterNormalizedStateIdCategory(tableauNodes, tableauNodes.byId[tableau.activeNodeId].childrenIds);

        const possibleResult = filterNormalizedStateIdCategory(childNodes, node => {
            const conceptTableauRuleNode = node as ConceptTableauRuleNode;
            return conceptTableauRuleNode.conceptId === conceptId &&
                conceptTableauRuleNode.cTreeNodeId === cTreeNodeId;
        });

        if (possibleResult.allIds.length) {
            return mapNormalizedStateIdCategory(possibleResult, node => node as ConceptTableauRuleNode);
        }

        return this.getNewApplicableTableauRuleNodes(cTreeNodeId, conceptId, cTreeNodes, cTreeEdges, expressions);
    }

    static getConceptTableauRuleDescription (conceptId: number, cTreeNode: CTreeNode, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        const concept = expressions.byId[conceptId];

        const helper = this.getHelper(concept.type)!;
        return helper.getConceptTableauRuleDescription(concept, cTreeNode, expressions, vocabularySymbols);
    }

    static onConceptTableauExpand (cTreeId: number, cTreeNodeId: number, conceptId: number, cTreeView: CTreeView) {
        const concept = cTreeView.props.expressions.byId[conceptId];
        const helper = this.getHelper(concept.type)!;

        const activeNode = cTreeView.tableauNodes.byId[cTreeView.props.tableau.activeNodeId];

        if (this.isTableauRuleApplicable(cTreeNodeId, conceptId, cTreeView.cTreeNodes, cTreeView.cTreeEdges, cTreeView.props.expressions)) {
            if (activeNode.childrenIds.length > 0 && !this.doActiveTableauNodeChildrenMatchConcept(concept, cTreeNodeId, activeNode, cTreeView.tableauNodes)) {
                cTreeView.props.fillAndShowModal(ConfirmationModal, {
                    title: "Confirmation of different tableau rule application order",
                    message: "You are about to apply tableau rules in a different order than in the previous traversal. This will invalidate the part of the tableau that does not match.",
                    onConfirm: () => {
                        cTreeView.props.dispatch(TABLEAU_ACTIONS.CREATE_PRUNE_ACTIVE_CHILDREN(cTreeView.props.tableau.id));
                        helper.onConceptTableauExpand(cTreeNodeId, conceptId, cTreeView);
                    }
                });
            }
            else {
                helper.onConceptTableauExpand(cTreeNodeId, conceptId, cTreeView);
            }
        }
    }
}

export interface ConceptHelper {
    TABLEAU_NODE_TYPE: TableauNodeType;

    getConceptTableauRuleDescription (concept: Expression, cTreeNode: CTreeNode, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string;

    getNewApplicableCTreeTableauConceptRuleNodes (cTreeNodeId: number, conceptId: number, cTreeNodes: NormalizedStateIdCategory<CTreeNode>, cTreeEdges: NormalizedStateIdCategory<CTreeEdge>, expressions: NormalizedStateIdCategory<Expression>): ConceptTableauRuleNode[]

    getNNFExpressionUpdate (concept: Expression, expressions: NormalizedStateIdCategory<Expression>): ExpressionUpdate

    getNegatedNNFExpressionUpdate (concept: Expression, expressions: NormalizedStateIdCategory<Expression>): ExpressionUpdate

    onConceptTableauExpand(cTreeNodeId: number, conceptId: number, cTreeView: CTreeView): void;
}

export interface CTreeNodeTableauRuleNode extends TableauNode {
    cTreeNodeId: number
}

export interface ConceptTableauRuleNode extends CTreeNodeTableauRuleNode {
    conceptId: number
}

export enum TableauNodeType {
    AND = "AND",
    OR = "OR",
    NONE = "NONE"
}