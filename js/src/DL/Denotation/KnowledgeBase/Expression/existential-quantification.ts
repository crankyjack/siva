import Expression, {ExpressionCategory} from "./expression";
import {default as ExpressionRouter, ExpressionPrecedence, ExpressionUpdate} from "./utils/expression-router";
import ConceptRouter, {ConceptTableauRuleNode, TableauNodeType} from "./utils/concept-router";
import {ErrorExpressionHelper, InvalidExpressionError} from "./error-expression";
import {ValueRestrictionHelper} from "./value-restriction";
import {findFirstGlobalCharOccurence, findInNormalizedStateIdCategory, NormalizedStateIdCategory, pushToArrayIfAbsent, removeNormalizedFromState, unsetInState} from "../../../../Common/utils/util";
import {VocabularySymbol} from "../../Vocabulary/vocabulary-symbol";
import Vocabulary from "../../Vocabulary/vocabulary";
import {TableauState} from "../../../../Tableau/tableau";
import TableauRuleRouter from "../../../../Tableau/utils/tableau-rule-router";
import CTreeEdge from "../../../CTreeTableauReasoning/CompletionTree/Edge/c-tree-edge";
import CTreeNode from "../../../CTreeTableauReasoning/CompletionTree/Node/c-tree-node";
import CTREE_HELPER_FUNCTIONS from "../../../CTreeTableauReasoning/CompletionTree/utils/c-tree-helper-functions";
import CTreeView from "../../../CTreeTableauReasoning/CompletionTree/components/c-tree-view";
import ModeExistentialQuantificationExpand from "../../../CTreeTableauReasoning/CompletionTree/Mode/components/mode-existential-quantification-expand";
import {CTreeState, CTreeTableauNodeDescriptionState} from "../../../CTreeTableauReasoning/CompletionTree/c-tree";

export default interface ExistentialQuantification extends Expression {
    roleId: number,
    roleFillerId: number
}

export class ExistentialQuantificationHelper {
    static readonly TABLEAU_NODE_TYPE = TableauNodeType.AND;
    static readonly TABLEAU_ACTION_TYPE = "CTreeTableauExistentialQuantificationRule";
    static readonly EXPRESSION_TYPE = "ExistentialQuantification";
    static readonly CATEGORY = ExpressionCategory.CONCEPT;
    static readonly PRECEDENCE = ExpressionPrecedence.BINARY_HIGH;
    static readonly SPECIAL_SYMBOLS = {
        EXISTS: {
            symbol: "\u2203",
            latex: "\\exists"
        }
    };

    static create (roleId: number, roleFillerId: number, id?: number) {
        return {
            id: id,
            roleId,
            roleFillerId,
            category: this.CATEGORY,
            type: this.EXPRESSION_TYPE
        } as ExistentialQuantification;
    }

    static pushRelatedExpressionIdsToArray (existentialQuantification: ExistentialQuantification, expressions: NormalizedStateIdCategory<Expression>, accumulator: number[]): void {
        if (pushToArrayIfAbsent(existentialQuantification.id, accumulator)) {
            ExpressionRouter.pushRelatedExpressionIdsToArray(existentialQuantification.roleId, expressions, accumulator);
            ExpressionRouter.pushRelatedExpressionIdsToArray(existentialQuantification.roleFillerId, expressions, accumulator);
        }
    }

    static getLatexString (existentialQuantification: ExistentialQuantification, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return `${this.SPECIAL_SYMBOLS.EXISTS.latex} ${ExpressionRouter.getLatexString(existentialQuantification.roleId, expressions, vocabularySymbols, this.EXPRESSION_TYPE)}.${ExpressionRouter.getLatexString(existentialQuantification.roleFillerId, expressions, vocabularySymbols, this.EXPRESSION_TYPE)}`;
    }

    static getSimpleString (existentialQuantification: ExistentialQuantification, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return `${this.SPECIAL_SYMBOLS.EXISTS.symbol}${ExpressionRouter.getSimpleString(existentialQuantification.roleId, expressions, vocabularySymbols, this.EXPRESSION_TYPE)}.${ExpressionRouter.getSimpleString(existentialQuantification.roleFillerId, expressions, vocabularySymbols, this.EXPRESSION_TYPE)}`;
    }

    static getUniqueString (existentialQuantification: ExistentialQuantification): string {
        return `${this.EXPRESSION_TYPE}(${existentialQuantification.roleId}, ${existentialQuantification.roleFillerId})`;
    }

    static getNormalizedUniqueString (existentialQuantification: ExistentialQuantification, expressions: NormalizedStateIdCategory<Expression>): string {
        return `${this.EXPRESSION_TYPE}(${ExpressionRouter.getNormalizedUniqueString(existentialQuantification.roleId, expressions)},${ExpressionRouter.getNormalizedUniqueString(existentialQuantification.roleFillerId, expressions)})`;
    }

    static parseString (expression: string, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>, vocabulary: Vocabulary): ExpressionUpdate | null {
        if (expression.charAt(0) === this.SPECIAL_SYMBOLS.EXISTS.symbol) {
            const dotIndex = findFirstGlobalCharOccurence(".", expression);
            if (dotIndex === -1) {
                return ExpressionRouter.getExpressionUpdate(ErrorExpressionHelper.create(InvalidExpressionError, `The ${this.SPECIAL_SYMBOLS.EXISTS.symbol} symbol is expected to be followed by an unbracketed dot symbol`, expression), expressions);
            }

            const parsedRole = ExpressionRouter.parseString(expression.substr(1, dotIndex - 1), expressions, vocabularySymbols, vocabulary, ExpressionCategory.ROLE);
            const parsedRoleFiller = ExpressionRouter.parseString(expression.substr(dotIndex + 1), parsedRole.expressions, vocabularySymbols, vocabulary, ExpressionCategory.CONCEPT);

            const parsedExpression = this.create(parsedRole.id, parsedRoleFiller.id);

            return ExpressionRouter.getExpressionUpdate(parsedExpression, parsedRoleFiller.expressions);
        }
        return null;
    }

    static getNNFExpressionUpdate (existentialQuantification: ExistentialQuantification, expressions: NormalizedStateIdCategory<Expression>): ExpressionUpdate {
        const nnfRoleFillerExpressionUpdate = ConceptRouter.getNNFExpressionUpdate(existentialQuantification.roleFillerId, expressions);

        return ExpressionRouter.getExpressionUpdate(this.create(existentialQuantification.roleId, nnfRoleFillerExpressionUpdate.id), nnfRoleFillerExpressionUpdate.expressions);
    }

    static getNegatedNNFExpressionUpdate (existentialQuantification: ExistentialQuantification, expressions: NormalizedStateIdCategory<Expression>): ExpressionUpdate {
        const negNnfRoleFillerExpressionUpdate = ConceptRouter.getNegatedNNFExpressionUpdate(existentialQuantification.roleFillerId, expressions);

        return ExpressionRouter.getExpressionUpdate(ValueRestrictionHelper.create(existentialQuantification.roleId, negNnfRoleFillerExpressionUpdate.id), negNnfRoleFillerExpressionUpdate.expressions);
    }

    static getNewApplicableCTreeTableauConceptRuleNodes (cTreeNodeId: number, conceptId: number, cTreeNodes: NormalizedStateIdCategory<CTreeNode>, cTreeEdges: NormalizedStateIdCategory<CTreeEdge>, expressions: NormalizedStateIdCategory<Expression>): ExistentialQuantificationTableauRuleNode[] {
        const existentialQuantification = expressions.byId[conceptId] as ExistentialQuantification;

        const satisfyingEdge = findInNormalizedStateIdCategory(cTreeEdges, cTreeEdge => {
            return cTreeEdge.startNodeId === cTreeNodeId &&
                cTreeEdge.labelRoles.indexOf(existentialQuantification.roleId) !== -1 &&
                typeof CTREE_HELPER_FUNCTIONS.getEqualConceptInNodeLabel(cTreeNodes.byId[cTreeEdge.endNodeId], existentialQuantification.roleFillerId, expressions) !== "undefined";
        });

        if (satisfyingEdge === null) {
            return [{
                id: NaN,
                actionType: this.TABLEAU_ACTION_TYPE,
                childrenIds: [],
                state: undefined,
                conceptId: existentialQuantification.id,
                cTreeNodeId: cTreeNodeId,
                roleId: existentialQuantification.roleId,
                newCTreeNodeId: NaN,
                roleFillerId: existentialQuantification.roleFillerId
            }];
        }

        return [];
    }

    static getConceptTableauRuleDescription (existentialQuantification: ExistentialQuantification, cTreeNode: CTreeNode, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        const roleFillerString = ExpressionRouter.getSimpleString(existentialQuantification.roleFillerId, expressions, vocabularySymbols);
        const roleString = ExpressionRouter.getSimpleString(existentialQuantification.roleId, expressions, vocabularySymbols);
        return `Create a new anonymous completion tree node, insert "${roleFillerString}" into its label and connect it with a new edge that begins in the current node and has "${roleString}" in its label.`;
    }

    static onConceptTableauExpand (cTreeNodeId: number, conceptId: number, cTreeView: CTreeView): void {
        cTreeView.setMode(ModeExistentialQuantificationExpand, {
            cTreeNodeId: cTreeNodeId,
            conceptId: conceptId
        });
    }

    static applyTableauRule<S extends TableauState> (existentialQuantificationTableauRuleNode: ExistentialQuantificationTableauRuleNode, state: S & CTreeState): S {
        const cTree = CTREE_HELPER_FUNCTIONS.getNodeCTree(existentialQuantificationTableauRuleNode.cTreeNodeId, state.DLCTrees)!;

        state = CTREE_HELPER_FUNCTIONS.addNodetoCTree(state, cTree.id, existentialQuantificationTableauRuleNode.newCTreeNodeId);
        let cTreeEdge;
        [state, cTreeEdge] = CTREE_HELPER_FUNCTIONS.connectNodes(state, cTree.id, existentialQuantificationTableauRuleNode.cTreeNodeId, existentialQuantificationTableauRuleNode.newCTreeNodeId);

        state = CTREE_HELPER_FUNCTIONS.extendEdgeLabel(state, cTreeEdge.id, existentialQuantificationTableauRuleNode.roleId);
        state = CTREE_HELPER_FUNCTIONS.extendNodeLabel(state, existentialQuantificationTableauRuleNode.newCTreeNodeId, existentialQuantificationTableauRuleNode.roleFillerId);
        return state;
    }

    static revertTableauRule<S extends TableauState> (existentialQuantificationTableauRuleNode: ExistentialQuantificationTableauRuleNode, state: S & CTreeState): S {
        const cTree = CTREE_HELPER_FUNCTIONS.getNodeCTree(existentialQuantificationTableauRuleNode.cTreeNodeId, state.DLCTrees)!;

        let cTreeEdge = findInNormalizedStateIdCategory(state.DLCTreeEdges,
            edge =>
                edge.startNodeId === existentialQuantificationTableauRuleNode.cTreeNodeId &&
                edge.endNodeId === existentialQuantificationTableauRuleNode.newCTreeNodeId,
            state.DLCTrees.byId[cTree.id].edges
        )!;

        state = removeNormalizedFromState(state, ["DLCTreeEdges"], cTreeEdge.id);
        state = unsetInState(state, ["DLCTrees", "byId", cTree.id, "edges", cTreeEdge.id]);

        state = CTREE_HELPER_FUNCTIONS.removeNodeFromCTree(state, cTree.id, existentialQuantificationTableauRuleNode.newCTreeNodeId);
        state = CTREE_HELPER_FUNCTIONS.removeFromNodeLabel(state, existentialQuantificationTableauRuleNode.newCTreeNodeId, existentialQuantificationTableauRuleNode.roleFillerId);
        return state;
    }

    static getTableauNodeLabelText (existentialQuantificationTableauRuleNode: ExistentialQuantificationTableauRuleNode, tableauNodeDescriptionState: CTreeTableauNodeDescriptionState): string {
        const cTreeNodeName = tableauNodeDescriptionState.DLCTreeNodes.byId[existentialQuantificationTableauRuleNode.cTreeNodeId] ?
            CTREE_HELPER_FUNCTIONS.getNodeName(tableauNodeDescriptionState.DLCTreeNodes.byId[existentialQuantificationTableauRuleNode.cTreeNodeId].nameOrIndividualSymbolId, tableauNodeDescriptionState.DLVocabularySymbols) :
            "";

        const conceptString = ExpressionRouter.getSimpleString(existentialQuantificationTableauRuleNode.conceptId, tableauNodeDescriptionState.DLExpressions, tableauNodeDescriptionState.DLVocabularySymbols);

        return `<b>\u2022 ${cTreeNodeName}</b>\n${this.SPECIAL_SYMBOLS.EXISTS.symbol}-rule`;
    }
}

export interface ExistentialQuantificationTableauRuleNode extends ConceptTableauRuleNode {
    roleId: number,
    roleFillerId: number,
    newCTreeNodeId: number
}

ExpressionRouter.registerHelper(ExistentialQuantificationHelper);
ConceptRouter.registerHelper(ExistentialQuantificationHelper);
TableauRuleRouter.registerHelper(ExistentialQuantificationHelper);