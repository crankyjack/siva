import ConceptRouter, {ConceptTableauRuleNode, TableauNodeType} from "./utils/concept-router";
import {default as ExpressionRouter, ExpressionPrecedence, ExpressionUpdate} from "./utils/expression-router";
import Expression, {ExpressionCategory} from "./expression";
import {ErrorExpressionHelper, UnknownSymbolError, WrongAlphabetError} from "./error-expression";
import {ConceptComplementHelper} from "./concept-complement";
import {NormalizedStateIdCategory, pushToArrayIfAbsent} from "../../../../Common/utils/util";
import {VocabularySymbol} from "../../Vocabulary/vocabulary-symbol";
import Vocabulary, {findVocabularySymbolBySymbol, VocabularyAlphabet} from "../../Vocabulary/vocabulary";
import CTreeEdge from "../../../CTreeTableauReasoning/CompletionTree/Edge/c-tree-edge";
import CTreeNode from "../../../CTreeTableauReasoning/CompletionTree/Node/c-tree-node";
import CTreeView from "../../../CTreeTableauReasoning/CompletionTree/components/c-tree-view";

export default interface AtomicConcept extends Expression {
    symbolId: number
}

export class AtomicConceptHelper {
    static readonly TABLEAU_NODE_TYPE = TableauNodeType.NONE;
    static readonly EXPRESSION_TYPE = "AtomicConcept";
    static readonly CATEGORY = ExpressionCategory.CONCEPT;
    static readonly PRECEDENCE = ExpressionPrecedence.VOCABULARY_SYMBOL;

    static create (symbolId: number, id?: number) {
        return {
            id: id,
            symbolId,
            category: this.CATEGORY,
            type: this.EXPRESSION_TYPE
        } as AtomicConcept;
    }

    static formatName (name: string) {
        if (name.length > 0) {
            name = name.charAt(0).toUpperCase() + name.slice(1);
        }
        return name;
    }

    static pushRelatedExpressionIdsToArray (atomicConcept: AtomicConcept, expressions: NormalizedStateIdCategory<Expression>, accumulator: number[]): void {
        pushToArrayIfAbsent(atomicConcept.id, accumulator);
    }

    static getLatexString (atomicConcept: AtomicConcept, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return `\\text{\\texttt{${vocabularySymbols.byId[atomicConcept.symbolId].symbol}}}`;
    }

    static getSimpleString (atomicConcept: AtomicConcept, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return vocabularySymbols.byId[atomicConcept.symbolId].symbol;
    }

    static getUniqueString (atomicConcept: AtomicConcept): string {
        return `${this.EXPRESSION_TYPE}(${atomicConcept.symbolId})`;
    }

    static getNormalizedUniqueString (atomicConcept: AtomicConcept, expressions: NormalizedStateIdCategory<Expression>): string {
        return `${this.EXPRESSION_TYPE}(${atomicConcept.symbolId})`;
    }

    static parseString (expression: string, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>, vocabulary: Vocabulary): ExpressionUpdate | null {
        if (ExpressionRouter.validateSymbol(expression)) {
            let parsedExpression: Expression;
            expression = this.formatName(expression);

            const symbolInformation = findVocabularySymbolBySymbol(expression, vocabularySymbols, vocabulary);

            if (symbolInformation) {
                if (symbolInformation.vocabularyAlphabet === VocabularyAlphabet.ATOMIC_CONCEPT) {
                    parsedExpression = this.create(symbolInformation.vocabularySymbol.id);
                }
                else {
                    parsedExpression = ErrorExpressionHelper.create(WrongAlphabetError, `The symbol "${expression}" does not belong to the alphabet of atomic concept names`, expression);
                }
            }
            else {
                parsedExpression = ErrorExpressionHelper.create(UnknownSymbolError, `The vocabulary doesn't contain an atomic concept named ${expression}`, expression);
            }

            return ExpressionRouter.getExpressionUpdate(parsedExpression, expressions);
        }

        return null;
    }

    static getNNFExpressionUpdate (atomicConcept: AtomicConcept, expressions: NormalizedStateIdCategory<Expression>): ExpressionUpdate {
        return {
            id: atomicConcept.id,
            expressions: expressions
        };
    }

    static getNegatedNNFExpressionUpdate (atomicConcept: AtomicConcept, expressions: NormalizedStateIdCategory<Expression>): ExpressionUpdate {
        return ExpressionRouter.getExpressionUpdate(ConceptComplementHelper.create(atomicConcept.id), expressions);
    }

    static getNewApplicableCTreeTableauConceptRuleNodes (cTreeNodeId: number, conceptId: number, cTreeNodes: NormalizedStateIdCategory<CTreeNode>, cTreeEdges: NormalizedStateIdCategory<CTreeEdge>, expressions: NormalizedStateIdCategory<Expression>): ConceptTableauRuleNode[] {
        return [];
    }

    static getConceptTableauRuleDescription (atomicConcept: AtomicConcept, cTreeNode: CTreeNode, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return `No tableau rule is applicable to ${this.getSimpleString(atomicConcept, expressions, vocabularySymbols)}.`;
    }

    static onConceptTableauExpand (cTreeNodeId: number, conceptId: number, cTreeView: CTreeView): void {
    }
}

ExpressionRouter.registerHelper(AtomicConceptHelper);
ConceptRouter.registerHelper(AtomicConceptHelper);
