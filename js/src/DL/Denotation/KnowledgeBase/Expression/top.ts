import Expression, {ExpressionCategory} from "./expression";
import {ConceptTableauRuleNode, default as ConceptRouter, TableauNodeType} from "./utils/concept-router";
import {default as ExpressionRouter, ExpressionPrecedence, ExpressionUpdate} from "./utils/expression-router";
import {NormalizedStateIdCategory, pushToArrayIfAbsent} from "../../../../Common/utils/util";
import Vocabulary from "../../Vocabulary/vocabulary";
import {BottomHelper} from "./bottom";
import {VocabularySymbol} from "../../Vocabulary/vocabulary-symbol";
import CTreeNode from "../../../CTreeTableauReasoning/CompletionTree/Node/c-tree-node";
import CTreeEdge from "../../../CTreeTableauReasoning/CompletionTree/Edge/c-tree-edge";
import CTreeView from "../../../CTreeTableauReasoning/CompletionTree/components/c-tree-view";

export default interface Top extends Expression {}

export class TopHelper {
    static readonly TABLEAU_NODE_TYPE = TableauNodeType.NONE;
    static readonly EXPRESSION_TYPE = "Top";
    static readonly CATEGORY = ExpressionCategory.CONCEPT;
    static readonly PRECEDENCE = ExpressionPrecedence.VOCABULARY_SYMBOL;
    static readonly SPECIAL_SYMBOLS = {
        TOP: {
            symbol: "\u22A4",
            latex: "\\top"
        }
    };

    static create (id?: number) {
        return {
            id: id,
            category: this.CATEGORY,
            type: this.EXPRESSION_TYPE
        } as Top;
    }

    static pushRelatedExpressionIdsToArray (top: Top, expressions: NormalizedStateIdCategory<Expression>, accumulator: number[]): void {
        pushToArrayIfAbsent(top.id, accumulator);
    }

    static getLatexString (top: Top, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return this.SPECIAL_SYMBOLS.TOP.latex;
    }

    static getSimpleString (top: Top, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return this.SPECIAL_SYMBOLS.TOP.symbol;
    }

    static getUniqueString (top: Top): string {
        return `${this.EXPRESSION_TYPE}`;
    }

    static getNormalizedUniqueString (top: Top, expressions: NormalizedStateIdCategory<Expression>): string {
        return `${this.EXPRESSION_TYPE}`;
    }

    static parseString (expression: string, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>, vocabulary: Vocabulary): ExpressionUpdate | null {
        if (expression === this.SPECIAL_SYMBOLS.TOP.symbol) {
            const parsedExpression = this.create();
            return ExpressionRouter.getExpressionUpdate(parsedExpression, expressions);
        }

        return null;
    }

    static getNNFExpressionUpdate (top: Top, expressions: NormalizedStateIdCategory<Expression>): ExpressionUpdate {
        return {
            id: top.id,
            expressions: expressions
        };
    }

    static getNegatedNNFExpressionUpdate (top: Top, expressions: NormalizedStateIdCategory<Expression>): ExpressionUpdate {
        return ExpressionRouter.getExpressionUpdate(BottomHelper.create(), expressions);
    }

    static getNewApplicableCTreeTableauConceptRuleNodes (cTreeNodeId: number, conceptId: number, cTreeNodes: NormalizedStateIdCategory<CTreeNode>, cTreeEdges: NormalizedStateIdCategory<CTreeEdge>, expressions: NormalizedStateIdCategory<Expression>): ConceptTableauRuleNode[] {
        return [];
    }

    static getConceptTableauRuleDescription (top: Top, cTreeNode: CTreeNode, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return `No tableau rule is applicable to ${this.getSimpleString(top, expressions, vocabularySymbols)}.`;
    }

    static onConceptTableauExpand (cTreeNodeId: number, conceptId: number, cTreeView: CTreeView): void {
    }
}

ExpressionRouter.registerHelper(TopHelper);
ConceptRouter.registerHelper(TopHelper);
