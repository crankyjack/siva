import Expression, {ExpressionCategory} from "./expression";
import {default as ExpressionRouter, ExpressionPrecedence, ExpressionUpdate} from "./utils/expression-router";
import {ErrorExpressionHelper, UnknownSymbolError, WrongAlphabetError} from "./error-expression";
import {NormalizedStateIdCategory, pushToArrayIfAbsent} from "../../../../Common/utils/util";
import {VocabularySymbol} from "../../Vocabulary/vocabulary-symbol";
import Vocabulary, {findVocabularySymbolBySymbol, VocabularyAlphabet} from "../../Vocabulary/vocabulary";

export default interface AtomicRole extends Expression {
    symbolId: number
}

export class AtomicRoleHelper {
    static readonly EXPRESSION_TYPE = "AtomicRole";
    static readonly CATEGORY = ExpressionCategory.ROLE;
    static readonly PRECEDENCE = ExpressionPrecedence.VOCABULARY_SYMBOL;

    static create (symbolId: number, id?: number) {
        return {
            id: id,
            symbolId,
            category: this.CATEGORY,
            type: this.EXPRESSION_TYPE
        } as AtomicRole;
    }

    static formatName (name: string) {
        if (name.length > 0) {
            name = name.charAt(0).toLowerCase() + name.slice(1);
        }
        return name;
    }

    static pushRelatedExpressionIdsToArray (atomicRole: AtomicRole, expressions: NormalizedStateIdCategory<Expression>, accumulator: number[]): void {
        pushToArrayIfAbsent(atomicRole.id, accumulator);
    }

    static getLatexString (atomicRole: AtomicRole, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return `\\text{\\texttt{${vocabularySymbols.byId[atomicRole.symbolId].symbol}}}`;
    }

    static getSimpleString (atomicRole: AtomicRole, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return vocabularySymbols.byId[atomicRole.symbolId].symbol;
    }

    static getUniqueString (atomicRole: AtomicRole): string {
        return `${this.EXPRESSION_TYPE}(${atomicRole.symbolId})`;
    }

    static getNormalizedUniqueString (atomicRole: AtomicRole, expressions: NormalizedStateIdCategory<Expression>): string {
        return `${this.EXPRESSION_TYPE}(${atomicRole.symbolId})`;
    }

    static parseString (expression: string, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>, vocabulary: Vocabulary): ExpressionUpdate | null {
        if (ExpressionRouter.validateSymbol(expression)) {
            let parsedExpression: Expression;
            expression = this.formatName(expression);

            const symbolInformation = findVocabularySymbolBySymbol(expression, vocabularySymbols, vocabulary);

            if (symbolInformation) {
                if (symbolInformation.vocabularyAlphabet === VocabularyAlphabet.ATOMIC_ROLE) {
                    parsedExpression = this.create(symbolInformation.vocabularySymbol.id);
                }
                else {
                    parsedExpression = ErrorExpressionHelper.create(WrongAlphabetError, `The symbol "${expression}" does not belong to the alphabet of atomic role names`, expression);
                }
            }
            else {
                parsedExpression = ErrorExpressionHelper.create(UnknownSymbolError, `The vocabulary doesn't contain an atomic role named ${expression}`, expression);
            }

            return ExpressionRouter.getExpressionUpdate(parsedExpression, expressions);
        }

        return null;
    }
}

ExpressionRouter.registerHelper(AtomicRoleHelper);