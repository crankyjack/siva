import Expression, {ExpressionCategory} from "./expression";
import {default as ExpressionRouter, ExpressionPrecedence, ExpressionUpdate} from "./utils/expression-router";
import ConceptRouter, {ConceptTableauRuleNode, TableauNodeType} from "./utils/concept-router";
import {ErrorExpressionHelper, InvalidExpressionError} from "./error-expression";
import {ExistentialQuantificationHelper, ExistentialQuantificationTableauRuleNode} from "./existential-quantification";
import {filterNormalizedStateIdCategory, findFirstGlobalCharOccurence, mapNormalizedStateIdCategory, NormalizedStateIdCategory, pushToArrayIfAbsent} from "../../../../Common/utils/util";
import {VocabularySymbol} from "../../Vocabulary/vocabulary-symbol";
import Vocabulary from "../../Vocabulary/vocabulary";
import CTreeEdge from "../../../CTreeTableauReasoning/CompletionTree/Edge/c-tree-edge";
import CTreeNode from "../../../CTreeTableauReasoning/CompletionTree/Node/c-tree-node";
import CTREE_HELPER_FUNCTIONS from "../../../CTreeTableauReasoning/CompletionTree/utils/c-tree-helper-functions";
import CTreeView from "../../../CTreeTableauReasoning/CompletionTree/components/c-tree-view";
import DL_CTREE_TABLEAU_WORKSPACE_ACTIONS from "../../../CTreeTableauReasoning/CompletionTree/DLCTreeTableauWorkspace/flux/dl-ctree-tableau-workspace-actions";
import {TableauState} from "../../../../Tableau/tableau";
import {CTreeState, CTreeTableauNodeDescriptionState} from "../../../CTreeTableauReasoning/CompletionTree/c-tree";
import TableauRuleRouter from "../../../../Tableau/utils/tableau-rule-router";

export default interface ValueRestriction extends Expression {
    roleId: number,
    roleFillerId: number
}

export class ValueRestrictionHelper {
    static readonly TABLEAU_NODE_TYPE = TableauNodeType.AND;
    static readonly TABLEAU_ACTION_TYPE = "CTreeTableauValueRestrictionRule";
    static readonly EXPRESSION_TYPE = "ValueRestriction";
    static readonly CATEGORY = ExpressionCategory.CONCEPT;
    static readonly PRECEDENCE = ExpressionPrecedence.BINARY_HIGH;
    static readonly SPECIAL_SYMBOLS = {
        FORALL: {
            symbol: "\u2200",
            latex: "\\forall"
        }
    };

    static create (roleId: number, roleFillerId: number, id?: number) {
        return {
            id: id,
            roleId,
            roleFillerId,
            category: this.CATEGORY,
            type: this.EXPRESSION_TYPE
        } as ValueRestriction;
    }

    static pushRelatedExpressionIdsToArray (valueRestriction: ValueRestriction, expressions: NormalizedStateIdCategory<Expression>, accumulator: number[]): void {
        if (pushToArrayIfAbsent(valueRestriction.id, accumulator)) {
            ExpressionRouter.pushRelatedExpressionIdsToArray(valueRestriction.roleId, expressions, accumulator);
            ExpressionRouter.pushRelatedExpressionIdsToArray(valueRestriction.roleFillerId, expressions, accumulator);
        }
    }

    static getLatexString (valueRestriction: ValueRestriction, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return `${this.SPECIAL_SYMBOLS.FORALL.latex} ${ExpressionRouter.getLatexString(valueRestriction.roleId, expressions, vocabularySymbols, this.EXPRESSION_TYPE)}.${ExpressionRouter.getLatexString(valueRestriction.roleFillerId, expressions, vocabularySymbols, this.EXPRESSION_TYPE)}`;
    }

    static getSimpleString (valueRestriction: ValueRestriction, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return `${this.SPECIAL_SYMBOLS.FORALL.symbol}${ExpressionRouter.getSimpleString(valueRestriction.roleId, expressions, vocabularySymbols, this.EXPRESSION_TYPE)}.${ExpressionRouter.getSimpleString(valueRestriction.roleFillerId, expressions, vocabularySymbols, this.EXPRESSION_TYPE)}`;
    }

    static getUniqueString (valueRestriction: ValueRestriction): string {
        return `${this.EXPRESSION_TYPE}(${valueRestriction.roleId}, ${valueRestriction.roleFillerId})`;
    }

    static getNormalizedUniqueString (valueRestriction: ValueRestriction, expressions: NormalizedStateIdCategory<Expression>): string {
        return `${this.EXPRESSION_TYPE}(${ExpressionRouter.getNormalizedUniqueString(valueRestriction.roleId, expressions)},${ExpressionRouter.getNormalizedUniqueString(valueRestriction.roleFillerId, expressions)})`;
    }

    static parseString (expression: string, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>, vocabulary: Vocabulary): ExpressionUpdate | null {
        if (expression.charAt(0) === this.SPECIAL_SYMBOLS.FORALL.symbol) {
            const dotIndex = findFirstGlobalCharOccurence(".", expression);
            if (dotIndex === -1) {
                return ExpressionRouter.getExpressionUpdate(ErrorExpressionHelper.create(InvalidExpressionError, `The ${this.SPECIAL_SYMBOLS.FORALL.symbol} symbol is expected to be followed by an unbracketed dot symbol`, expression), expressions);
            }

            const parsedRole = ExpressionRouter.parseString(expression.substr(1, dotIndex - 1), expressions, vocabularySymbols, vocabulary, ExpressionCategory.ROLE);
            const parsedRoleFiller = ExpressionRouter.parseString(expression.substr(dotIndex + 1), parsedRole.expressions, vocabularySymbols, vocabulary, ExpressionCategory.CONCEPT);

            const parsedExpression = this.create(parsedRole.id, parsedRoleFiller.id);

            return ExpressionRouter.getExpressionUpdate(parsedExpression, parsedRoleFiller.expressions);
        }
        return null;
    }

    static getNNFExpressionUpdate (valueRestriction: ValueRestriction, expressions: NormalizedStateIdCategory<Expression>): ExpressionUpdate {
        const nnfRoleFillerExpressionUpdate = ConceptRouter.getNNFExpressionUpdate(valueRestriction.roleFillerId, expressions);

        return ExpressionRouter.getExpressionUpdate(this.create(valueRestriction.roleId, nnfRoleFillerExpressionUpdate.id), nnfRoleFillerExpressionUpdate.expressions);
    }

    static getNegatedNNFExpressionUpdate (valueRestriction: ValueRestriction, expressions: NormalizedStateIdCategory<Expression>): ExpressionUpdate {
        const negNnfRoleFillerExpressionUpdate = ConceptRouter.getNegatedNNFExpressionUpdate(valueRestriction.roleFillerId, expressions);

        return ExpressionRouter.getExpressionUpdate(ExistentialQuantificationHelper.create(valueRestriction.roleId, negNnfRoleFillerExpressionUpdate.id), negNnfRoleFillerExpressionUpdate.expressions);
    }

    static getNewApplicableCTreeTableauConceptRuleNodes (cTreeNodeId: number, conceptId: number, cTreeNodes: NormalizedStateIdCategory<CTreeNode>, cTreeEdges: NormalizedStateIdCategory<CTreeEdge>, expressions: NormalizedStateIdCategory<Expression>): ValueRestrictionTableauRuleNode[] {
        const valueRestriction = expressions.byId[conceptId] as ValueRestriction;
        const edgesWithEndingsToExtend = filterNormalizedStateIdCategory(cTreeEdges, cTreeEdge => {
            return cTreeEdge.startNodeId === cTreeNodeId &&
                cTreeEdge.labelRoles.indexOf(valueRestriction.roleId) !== -1 &&
                typeof CTREE_HELPER_FUNCTIONS.getEqualConceptInNodeLabel(cTreeNodes.byId[cTreeEdge.endNodeId], valueRestriction.roleFillerId, expressions) === "undefined";
        });

        const nodesToExtend = mapNormalizedStateIdCategory(edgesWithEndingsToExtend, edge => edge.endNodeId);

        if (nodesToExtend.length > 0) {
            return [{
                id: NaN,
                actionType: this.TABLEAU_ACTION_TYPE,
                childrenIds: [],
                state: undefined,
                conceptId: valueRestriction.id,
                cTreeNodeId: cTreeNodeId,
                roleFillerId: valueRestriction.roleFillerId,
                nodesToExtend: nodesToExtend
            }];
        }

        return [];
    }

    static getConceptTableauRuleDescription (valueRestriction: ValueRestriction, cTreeNode: CTreeNode, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        const roleFillerString = ExpressionRouter.getSimpleString(valueRestriction.roleFillerId, expressions, vocabularySymbols);
        const roleString = ExpressionRouter.getSimpleString(valueRestriction.roleId, expressions, vocabularySymbols);
        return `Insert "${roleFillerString}" into labels of nodes, that are connected  with an edge that begins in the current node and has "${roleString}" in its label.`;
    }

    static onConceptTableauExpand (cTreeNodeId: number, conceptId: number, cTreeView: CTreeView): void {
        cTreeView.props.dispatch(DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.CREATE_APPLY_CONCEPT_TABLEAU_AND_RULE(cTreeNodeId, conceptId));
    }

    static applyTableauRule<S extends TableauState> (valueRestrictionTableauRuleNode: ValueRestrictionTableauRuleNode, state: S & CTreeState): S {
        for (const nodeId of valueRestrictionTableauRuleNode.nodesToExtend) {
            state = CTREE_HELPER_FUNCTIONS.extendNodeLabel(state, nodeId, valueRestrictionTableauRuleNode.roleFillerId);
        }

        return state;
    }

    static revertTableauRule<S extends TableauState> (valueRestrictionTableauRuleNode: ValueRestrictionTableauRuleNode, state: S & CTreeState): S {
        for (const nodeId of valueRestrictionTableauRuleNode.nodesToExtend) {
            state = CTREE_HELPER_FUNCTIONS.removeFromNodeLabel(state, nodeId, valueRestrictionTableauRuleNode.roleFillerId);
        }

        return state;
    }

    static getTableauNodeLabelText (existentialQuantificationTableauRuleNode: ExistentialQuantificationTableauRuleNode, tableauNodeDescriptionState: CTreeTableauNodeDescriptionState): string {
        const cTreeNodeName = tableauNodeDescriptionState.DLCTreeNodes.byId[existentialQuantificationTableauRuleNode.cTreeNodeId] ?
            CTREE_HELPER_FUNCTIONS.getNodeName(tableauNodeDescriptionState.DLCTreeNodes.byId[existentialQuantificationTableauRuleNode.cTreeNodeId].nameOrIndividualSymbolId, tableauNodeDescriptionState.DLVocabularySymbols) :
            "";

        const conceptString = ExpressionRouter.getSimpleString(existentialQuantificationTableauRuleNode.conceptId, tableauNodeDescriptionState.DLExpressions, tableauNodeDescriptionState.DLVocabularySymbols);

        return `<b>\u2022 ${cTreeNodeName}</b>\n${this.SPECIAL_SYMBOLS.FORALL.symbol}-rule`;
    }
}

interface ValueRestrictionTableauRuleNode extends ConceptTableauRuleNode {
    roleFillerId: number,
    nodesToExtend: number[]
}

ExpressionRouter.registerHelper(ValueRestrictionHelper);
ConceptRouter.registerHelper(ValueRestrictionHelper);
TableauRuleRouter.registerHelper(ValueRestrictionHelper);