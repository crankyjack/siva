import {Reducer} from "redux";
import DL_DENOTATION_ACTIONS from "./knowledge-base-actions";
import {DLDenotationState} from "../../denotation";
import {AnyAction, default as ACTIONS} from "../../../../App/flux/actions";
import {appendNormalizedToState, initNormalizedCategoryInState} from "../../../../Common/utils/util";
import DL_DENOTATION_HELPER_FUNCTIONS from "../../utils/denotation-helper-functions";
import ExpressionRouter from "../Expression/utils/expression-router";
import {AxiomBoxProperties} from "../knowledge-base";
import {ExpressionCategory} from "../Expression/expression";
import {ConceptSatisfiabilityCheckingHelper} from "../../../Problem/concept-satisfiability-checking";
import ProblemRouter from "../../../../Problem/utils/problem-router";
import {InstanceCheckingHelper} from "../../../Problem/instance-checking";
import Individual from "../Expression/individual";
import {KnowledgeBaseConsistencyCheckingHelper} from "../../../Problem/knowledge-base-consistency-checking";

const KnowledgeBaseReducer: Reducer<DLDenotationState> = (state: DLDenotationState, payload: AnyAction) => {
    payloadSwitch:
        switch (payload.type) {
            case ACTIONS.CREATE_WORKSPACE: {
                const action = payload as ACTIONS.PAYLOAD_CREATE_WORKSPACE;

                state = initNormalizedCategoryInState(state, "DLKnowledgeBases");
                state = initNormalizedCategoryInState(state, "DLExpressions");

                break payloadSwitch;
            }
            case DL_DENOTATION_ACTIONS.PARSE_AND_SAVE_EXPRESSION: {
                const action = payload as DL_DENOTATION_ACTIONS.PAYLOAD_PARSE_AND_SAVE_EXPRESSION;

                state = DL_DENOTATION_HELPER_FUNCTIONS.parseAndSaveExpression(state, action.expressionString, action.vocabularyId, action.expressionCategory)[1];

                break payloadSwitch;
            }
            case DL_DENOTATION_ACTIONS.DELETE_AXIOM: {
                const action = payload as DL_DENOTATION_ACTIONS.PAYLOAD_DELETE_AXIOM;

                state = DL_DENOTATION_HELPER_FUNCTIONS.deleteKnowledgeBaseAxiom(state, action.knowledgeBaseId, action.axiomId);

                break payloadSwitch;
            }
            case DL_DENOTATION_ACTIONS.SAVE_AXIOM: {
                const action = payload as DL_DENOTATION_ACTIONS.PAYLOAD_SAVE_AXIOM;
                const vocabulary = DL_DENOTATION_HELPER_FUNCTIONS.getVocabulary(state, action.knowledgeBaseId);

                const parseResult = ExpressionRouter.parseString(action.axiom, state.DLExpressions, state.DLVocabularySymbols, vocabulary, AxiomBoxProperties[action.axiomBox].AXIOM_CATEGORY);

                state = DL_DENOTATION_HELPER_FUNCTIONS.saveParsedAxiom(state, action.knowledgeBaseId, action.axiomBox, parseResult, action.axiomId);

                break payloadSwitch;
            }
            case DL_DENOTATION_ACTIONS.DEFINE_CONCEPT_SATISFIABILITY_CHECKING: {
                const action = payload as DL_DENOTATION_ACTIONS.PAYLOAD_DEFINE_CONCEPT_SATISFIABILITY_CHECKING;
                const knowledgeBase = state.DLKnowledgeBases.byId[action.knowledgeBaseId];

                let conceptId;
                [conceptId, state] = DL_DENOTATION_HELPER_FUNCTIONS.parseAndSaveExpression(state, action.conceptString, knowledgeBase.vocabularyId, ExpressionCategory.CONCEPT);

                const problem = ConceptSatisfiabilityCheckingHelper.create(conceptId, knowledgeBase.id);
                state = appendNormalizedToState(state, ["Problems"], problem);

                state = ProblemRouter.handleWorkspaceProblemAssign(problem, action.workspaceId, state);
                break payloadSwitch;
            }
            case DL_DENOTATION_ACTIONS.DEFINE_INSTANCE_CHECKING: {
                const action = payload as DL_DENOTATION_ACTIONS.PAYLOAD_DEFINE_INSTANCE_CHECKING;
                const knowledgeBase = state.DLKnowledgeBases.byId[action.knowledgeBaseId];

                let individualId;
                let conceptId;
                [individualId, state] = DL_DENOTATION_HELPER_FUNCTIONS.parseAndSaveExpression(state, action.individualString, knowledgeBase.vocabularyId, ExpressionCategory.INDIVIDUAL);
                [conceptId, state] = DL_DENOTATION_HELPER_FUNCTIONS.parseAndSaveExpression(state, action.conceptString, knowledgeBase.vocabularyId, ExpressionCategory.CONCEPT);

                const problem = InstanceCheckingHelper.create((state.DLExpressions.byId[individualId] as Individual).symbolId, conceptId, knowledgeBase.id);
                state = appendNormalizedToState(state, ["Problems"], problem);

                state = ProblemRouter.handleWorkspaceProblemAssign(problem, action.workspaceId, state);
                break payloadSwitch;
            }
            case DL_DENOTATION_ACTIONS.DEFINE_KNOWLEDGE_BASE_CONSISTENCY_CHECKING: {
                const action = payload as DL_DENOTATION_ACTIONS.PAYLOAD_DEFINE_KNOWLEDGE_BASE_CONSISTENCY_CHECKING;
                const knowledgeBase = state.DLKnowledgeBases.byId[action.knowledgeBaseId];

                const problem = KnowledgeBaseConsistencyCheckingHelper.create(knowledgeBase.id);
                state = appendNormalizedToState(state, ["Problems"], problem);

                state = ProblemRouter.handleWorkspaceProblemAssign(problem, action.workspaceId, state);
                break payloadSwitch;
            }
        }
    return state;
};

export default KnowledgeBaseReducer;