import * as React from "react";
import {CSSProperties, SyntheticEvent} from "react";
import {Button, Col, Nav, NavItem, Panel, Row, Tab, TabContainer} from "react-bootstrap";
import {DLDenotationConstants} from "../utils/denotation-constants";
import KnowledgeBaseView from "../KnowledgeBase/components/knowledge-base-view";
import VocabularyView from "../Vocabulary/components/vocabulary-view";
import autobind from "autobind-decorator";
import KnowledgeBase from "../KnowledgeBase/knowledge-base";
import Vocabulary from "../Vocabulary/vocabulary";
import {VocabularySymbol} from "../Vocabulary/vocabulary-symbol";
import DL_DENOTATION_HELPER_FUNCTIONS from "../utils/denotation-helper-functions";
import {NormalizedStateIdCategory, updateComponentNormalizedStateIfNotDeepEqual} from "../../../Common/utils/util";
import {MainComponent} from "../../../App/components/main-component";
import {Dispatch} from "redux";
import MainState from "../../../App/flux/main-state";
import Latex from "react-latex";
import Expression from "../KnowledgeBase/Expression/expression";

export interface VKBPanelViewProps {
    knowledgeBase: KnowledgeBase,
    vocabulary: Vocabulary,
    vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>,
    fillAndShowModal: typeof MainComponent.prototype.fillAndShowModal,
    expressions: NormalizedStateIdCategory<Expression>,
    onReady?: ((knowledgeBase: KnowledgeBase) => void) | undefined,
    editable: boolean,
    dispatch: Dispatch<MainState>
    style: CSSProperties
}

export interface VKBPanelViewState {
    activeKey: string,
    vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>
}

export default class VKBPanelView extends React.PureComponent<VKBPanelViewProps, VKBPanelViewState> {
    constructor (props: VKBPanelViewProps) {
        super(props);

        this.state = {
            activeKey: DLDenotationConstants.VOCABULARY.TAB.KEY,
            vocabularySymbols: props.vocabularySymbols
        };
    }

    @autobind
    handleTabSelect (key: SyntheticEvent<TabContainer>) {
        this.setState({
            activeKey: key as any as string
        });
    }

    componentWillReceiveProps (nextProps: Readonly<VKBPanelViewProps>, nextContext: any): void {
        updateComponentNormalizedStateIfNotDeepEqual(this, "vocabularySymbols", nextProps.vocabularySymbols);
    }

    @autobind
    handleReady () {
        !this.props.onReady || this.props.onReady(this.props.knowledgeBase);
    }

    render () {
        const expressionErrors = DL_DENOTATION_HELPER_FUNCTIONS.checkKnowledgeBaseForErrors(this.props.knowledgeBase, this.props.expressions);

        return (
            <Tab.Container
                activeKey={this.state.activeKey}
                onSelect={this.handleTabSelect}>
                <Panel
                    style={this.props.style}
                    className={`panel-buttons-toggle-header ${DLDenotationConstants.TAB_CONTAINER.CLASS.BASE}`}
                    header={
                        <Nav className="nav-buttons-toggle">
                            <NavItem eventKey={DLDenotationConstants.VOCABULARY.TAB.KEY}>
                                <Button
                                    active={this.state.activeKey === DLDenotationConstants.VOCABULARY.TAB.KEY}>
                                    {
                                        DLDenotationConstants.VOCABULARY.TAB.NAME
                                    }
                                    {
                                        <Latex children="$N$"/>
                                    }
                                </Button>
                            </NavItem>
                            <NavItem eventKey={DLDenotationConstants.KNOWLEDGE_BASE.TAB.KEY}>
                                <Button
                                    active={this.state.activeKey === DLDenotationConstants.KNOWLEDGE_BASE.TAB.KEY}>
                                    {
                                        DLDenotationConstants.KNOWLEDGE_BASE.TAB.NAME
                                    }
                                    {
                                        <Latex children="$\mathcal{K}$"/>
                                    }
                                </Button>
                            </NavItem>
                        </Nav>
                    }
                >
                    <Tab.Content
                        className="clearfix"
                        animation
                    >
                        <Tab.Pane
                            eventKey={DLDenotationConstants.VOCABULARY.TAB.KEY}
                            className={`${DLDenotationConstants.TAB_CONTAINER.CLASS.BASE}-pane-${DLDenotationConstants.VOCABULARY.TAB.KEY}`}
                        >
                            <VocabularyView
                                vocabulary={this.props.vocabulary}
                                vocabularySymbols={this.state.vocabularySymbols}
                                fillAndShowModal={this.props.fillAndShowModal}
                                knowledgeBaseId={this.props.knowledgeBase.id}
                                dispatch={this.props.dispatch}
                                editable={this.props.editable}
                            />
                        </Tab.Pane>
                        <Tab.Pane
                            eventKey={DLDenotationConstants.KNOWLEDGE_BASE.TAB.KEY}
                            className={`${DLDenotationConstants.TAB_CONTAINER.CLASS.BASE}-pane-${DLDenotationConstants.KNOWLEDGE_BASE.TAB.KEY}`}
                        >
                            <KnowledgeBaseView
                                expressions={this.props.expressions}
                                vocabularySymbols={this.props.vocabularySymbols}
                                vocabulary={this.props.vocabulary}
                                knowledgeBase={this.props.knowledgeBase}
                                fillAndShowModal={this.props.fillAndShowModal}
                                dispatch={this.props.dispatch}
                                knowledgeBaseEditable={this.props.editable}
                            />
                        </Tab.Pane>
                    </Tab.Content>
                    <Row>
                        <Col
                            className="text-center">
                            {
                                !this.props.onReady ||
                                    (<Button
                                            bsStyle="primary"
                                            title={expressionErrors.length ?
                                                "The knowledge base contains errors and can not be locked." :
                                                "You won't be able to make any further changes to the knowledge base after you start reasoning."}
                                            onClick={this.handleReady}
                                            disabled={expressionErrors.length !== 0}
                                        >
                                            Run simulation
                                        </Button>
                                    )
                            }
                        </Col>
                    </Row>
                </Panel>
            </Tab.Container>
        );
    }
}