import {Reducer} from "redux";
import VOCABULARY_ACTIONS from "./vocabulary-actions";
import {AnyAction, default as ACTIONS} from "../../../../App/flux/actions";
import {getNormalizedStateIdCategoryIterator, initNormalizedCategoryInState, removeNormalizedFromState, setInState, unsetInState} from "../../../../Common/utils/util";
import DL_DENOTATION_HELPER_FUNCTIONS, {KnowledgeBaseStringMap} from "../../utils/denotation-helper-functions";
import {VocabularyAlphabet} from "../vocabulary";
import {DLDenotationState} from "../../denotation";

const VocabularyReducer: Reducer<DLDenotationState> = (state: DLDenotationState, payload: AnyAction) => {
    payloadSwitch:
        switch (payload.type) {
            case ACTIONS.CREATE_WORKSPACE: {
                const action = payload as ACTIONS.PAYLOAD_CREATE_WORKSPACE;

                state = initNormalizedCategoryInState(state, "DLExpressions");
                state = initNormalizedCategoryInState(state, "DLVocabularies");
                state = initNormalizedCategoryInState(state, "DLVocabularySymbols");

                break payloadSwitch;
            }
            case VOCABULARY_ACTIONS.CREATE_SYMBOL: {
                const action = payload as VOCABULARY_ACTIONS.PAYLOAD_CREATE_SYMBOL;

                const knowledgeBaseSimpleStringMaps: {[id: number]: KnowledgeBaseStringMap} = {};

                for (let knowledgeBase of getNormalizedStateIdCategoryIterator(state.DLKnowledgeBases)) {
                    if (knowledgeBase.vocabularyId === action.vocabularyId) {
                        knowledgeBaseSimpleStringMaps[knowledgeBase.id] = DL_DENOTATION_HELPER_FUNCTIONS.getKnowledgeBaseSimpleStringMap(state, knowledgeBase.id);
                    }
                }

                state = DL_DENOTATION_HELPER_FUNCTIONS.addVocabularySymbol(state, action.vocabularyId, action.alphabet, action.symbol);

                for (let knowledgeBase of getNormalizedStateIdCategoryIterator(state.DLKnowledgeBases)) {
                    if (knowledgeBase.vocabularyId === action.vocabularyId) {
                        state = DL_DENOTATION_HELPER_FUNCTIONS.reevaluateKnowledgeBaseAxioms(state, knowledgeBase.id, knowledgeBaseSimpleStringMaps[knowledgeBase.id]);
                    }
                }

                break payloadSwitch;
            }
            case VOCABULARY_ACTIONS.RENAME_SYMBOL: {
                const action = payload as VOCABULARY_ACTIONS.PAYLOAD_RENAME_SYMBOL;

                state = setInState(state, ["DLVocabularySymbols", "byId", action.vocabularySymbolId, "symbol"], action.newSymbol, true);

                break payloadSwitch;
            }
            case VOCABULARY_ACTIONS.DELETE_SYMBOL: {
                const action = payload as VOCABULARY_ACTIONS.PAYLOAD_DELETE_SYMBOL;
                const vocabulary = state.DLVocabularies.byId[action.vocabularyId];

                const knowledgeBaseSimpleStringMaps: {[id: number]: KnowledgeBaseStringMap} = {};

                for (let knowledgeBase of getNormalizedStateIdCategoryIterator(state.DLKnowledgeBases)) {
                    if (knowledgeBase.vocabularyId === action.vocabularyId) {
                        knowledgeBaseSimpleStringMaps[knowledgeBase.id] = DL_DENOTATION_HELPER_FUNCTIONS.getKnowledgeBaseSimpleStringMap(state, knowledgeBase.id);
                    }
                }

                for (const alphabet in VocabularyAlphabet) {
                    const alphabetKey = VocabularyAlphabet[alphabet];
                    const symbolsIds = (vocabulary as any)[alphabetKey] as number[];

                    for (const id of symbolsIds) {
                        if (id === action.vocabularySymbolId) {
                            state = unsetInState(state, ["DLVocabularies", "byId", vocabulary.id, alphabetKey, action.vocabularySymbolId]);
                            break;
                        }
                    }
                }

                state = removeNormalizedFromState(state, ["DLVocabularySymbols"], action.vocabularySymbolId);

                for (let knowledgeBase of getNormalizedStateIdCategoryIterator(state.DLKnowledgeBases)) {
                    if (knowledgeBase.vocabularyId === action.vocabularyId) {
                        state = DL_DENOTATION_HELPER_FUNCTIONS.reevaluateKnowledgeBaseAxioms(state, knowledgeBase.id, knowledgeBaseSimpleStringMaps[knowledgeBase.id]);
                    }
                }

                break payloadSwitch;
            }
        }
    return state;
};

export default VocabularyReducer;