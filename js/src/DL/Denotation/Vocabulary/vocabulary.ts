import {appendNormalizedToState, NormalizedStateIdCategory} from "../../../Common/utils/util";
import {VocabularySymbol} from "./vocabulary-symbol";
import {DLDenotationState} from "../denotation";

export default interface Vocabulary {
    id: number,
    atomicConcepts: number[],
    atomicRoles: number[],
    individuals: number[],
}

export function createNewEmptyVocabulary (state: DLDenotationState) {
    const result = {
        id: NaN,
        atomicConcepts: [],
        atomicRoles: [],
        individuals: []
    } as Vocabulary;

    state = appendNormalizedToState(state, ["DLVocabularies"], result);

    return [state, result] as [DLDenotationState, Vocabulary];
}

export function findVocabularySymbolBySymbol (symbol: string, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>, vocabulary: Vocabulary) {
    for (const alphabet in VocabularyAlphabet) {
        const alphabetKey = VocabularyAlphabet[alphabet];
        const symbolsIds = (vocabulary as any)[alphabetKey] as number[];

        for (const id of symbolsIds) {
            if (vocabularySymbols.byId[id].symbol === symbol) {
                return {
                    vocabularyAlphabet: alphabetKey,
                    vocabularySymbol: vocabularySymbols.byId[id]
                };
            }
        }
    }
    return null;
}

export enum VocabularyAlphabet {
    ATOMIC_CONCEPT = "atomicConcepts",
    ATOMIC_ROLE = "atomicRoles",
    INDIVIDUAL = "individuals"
}