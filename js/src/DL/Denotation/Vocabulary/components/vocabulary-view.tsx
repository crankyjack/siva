import * as React from "react";
import VocabularyColumnView from "./vocabulary-column-view";
import Vocabulary, {VocabularyAlphabet} from "../vocabulary";
import autobind from "autobind-decorator";
import {NormalizedStateIdCategory} from "../../../../Common/utils/util";
import {VocabularySymbol} from "../vocabulary-symbol";
import {Dispatch} from "redux";
import MainState from "../../../../App/flux/main-state";
import {MainComponent} from "../../../../App/components/main-component";
import {AtomicConceptHelper} from "../../KnowledgeBase/Expression/atomic-concept";
import {AtomicRoleHelper} from "../../KnowledgeBase/Expression/atomic-role";
import {IndividualHelper} from "../../KnowledgeBase/Expression/individual";
import VOCABULARY_ACTIONS from "../flux/vocabulary-actions";

export const atomicConceptSymbolToLatex = (item: string) => `$\\texttt{${item}}$`;
export const atomicRoleSymbolToLatex = (item: string) => `$\\texttt{${item}}$`;
export const individualSymbolToLatex = (item: string) => `$\\text{\\textit{${item}}}$`;

export interface VocabularyViewProps {
    vocabulary: Vocabulary,
    vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>,
    fillAndShowModal: typeof MainComponent.prototype.fillAndShowModal,
    knowledgeBaseId: number
    dispatch: Dispatch<MainState>
    editable: boolean
}

export interface VocabularyViewState {

}

export default class VocabularyView extends React.PureComponent<VocabularyViewProps, VocabularyViewState> {
    @autobind
    dispatchCreateAtomicConcept (name: string) {
        this.props.dispatch(VOCABULARY_ACTIONS.CREATE_CREATE_SYMBOL(this.props.vocabulary.id, VocabularyAlphabet.ATOMIC_CONCEPT, name));
    }

    @autobind
    dispatchCreateAtomicRole (name: string) {
        this.props.dispatch(VOCABULARY_ACTIONS.CREATE_CREATE_SYMBOL(this.props.vocabulary.id, VocabularyAlphabet.ATOMIC_ROLE, name));
    }

    @autobind
    dispatchCreateIndividual (name: string) {
        this.props.dispatch(VOCABULARY_ACTIONS.CREATE_CREATE_SYMBOL(this.props.vocabulary.id, VocabularyAlphabet.INDIVIDUAL, name));
    }

    render () {
        return (
            <div
                className="tab-pane-content"
            >
                <VocabularyColumnView
                    bsColumnWidth={4}
                    vocabularyId={this.props.vocabulary.id}
                    latexHeading="$\text{Atomic concepts (}N_C\text{)}$"
                    symbolIds={this.props.vocabulary.atomicConcepts}
                    symbolToLatex={atomicConceptSymbolToLatex}
                    fillAndShowModal={this.props.fillAndShowModal}
                    symbolType="atomic concept"
                    vocabularySymbols={this.props.vocabularySymbols}
                    appendHandler={this.dispatchCreateAtomicConcept}
                    nameFormatHint={"The name should start with an uppercase letter and will be formatted"}
                    nameFormatConvert={AtomicConceptHelper.formatName}
                    vocabularyEditable={this.props.editable}
                    dispatch={this.props.dispatch}
                />
                <VocabularyColumnView
                    bsColumnWidth={4}
                    vocabularyId={this.props.vocabulary.id}
                    latexHeading="$\text{Atomic roles (}N_R\text{)}$"
                    symbolIds={this.props.vocabulary.atomicRoles}
                    symbolToLatex={atomicRoleSymbolToLatex}
                    fillAndShowModal={this.props.fillAndShowModal}
                    symbolType="atomic role"
                    vocabularySymbols={this.props.vocabularySymbols}
                    appendHandler={this.dispatchCreateAtomicRole}
                    nameFormatHint={"The name should start with a lowercase letter and will be formatted"}
                    nameFormatConvert={AtomicRoleHelper.formatName}
                    vocabularyEditable={this.props.editable}
                    dispatch={this.props.dispatch}
                />
                <VocabularyColumnView
                    bsColumnWidth={4}
                    vocabularyId={this.props.vocabulary.id}
                    latexHeading="$\text{Individuals (}N_I\text{)}$"
                    symbolIds={this.props.vocabulary.individuals}
                    symbolToLatex={individualSymbolToLatex}
                    fillAndShowModal={this.props.fillAndShowModal}
                    symbolType="individual"
                    vocabularySymbols={this.props.vocabularySymbols}
                    appendHandler={this.dispatchCreateIndividual}
                    nameFormatHint={""}
                    nameFormatConvert={IndividualHelper.formatName}
                    vocabularyEditable={this.props.editable}
                    dispatch={this.props.dispatch}
                />
            </div>
        );
    }
}