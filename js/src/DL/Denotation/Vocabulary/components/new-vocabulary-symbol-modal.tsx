import * as React from "react";
import autobind from "autobind-decorator";
import {FormControl} from "react-bootstrap";
import {ModalComponentProps, ModalComponentState} from "../../../../Common/modal-component";
import SingleInputModal, {FormValidationInformation} from "../../../../Common/components/single-input-modal";
import ExpressionRouter from "../../KnowledgeBase/Expression/utils/expression-router";

export interface NewVocabularySymbolModalProps extends ModalComponentProps {
    type: string,
    takenNames: Set<string>,
    submitHandler: (name: string) => void,
    formatHint: string,
    formatConvert: (name: string) => string
}

export interface NewVocabularySymbolModalState extends ModalComponentState {
    newName: string
}

export default class NewVocabularySymbolModal extends SingleInputModal<NewVocabularySymbolModalProps, NewVocabularySymbolModalState> {
    constructor (props: NewVocabularySymbolModalProps) {
        super(props);

        this.state = {
            open: true,
            newName: ""
        };
    }

    protected getModalTitleText (): string {
        return `Create a new ${this.props.type}`;
    }

    protected getInputLabelText (): string {
        return "Name:";
    }

    protected getSubmitButtonText (): string {
        return "Create";
    }

    protected getInputDefaultText (): string {
        return "";
    }

    protected getInputPlaceholderText (): string {
        return `Enter new ${this.props.type} name`;
    }

    protected getFormValidationInformation (): FormValidationInformation {
        const convertedName = this.props.formatConvert(this.state.newName);

        let result: FormValidationInformation = {
            hint: "",
            validationState: undefined,
            buttonBsStyle: null,
            buttonDisabled: false
        };

        if (!ExpressionRouter.validateSymbol(convertedName)) {
            result.hint = 'The name must consist of alhpanumeric characters only and has to start with a letter';
            result.validationState = "error";
            result.buttonBsStyle = "danger";
            result.buttonDisabled = true;
        }
        else if (this.props.takenNames.has(this.state.newName)) {
            result.hint = "This symbol is already being used in the vocabulary";
            result.validationState = "error";
            result.buttonBsStyle = "danger";
            result.buttonDisabled = true;
        }
        else if (this.state.newName !== convertedName) {
            result.hint = this.props.formatHint;
            result.validationState = "warning";
            result.buttonBsStyle = "warning";
        }
        else {
            result.validationState = "success";
            result.buttonBsStyle = "success";
        }

        return result;
    }

    @autobind
    protected nameInputChangeHandler (e: React.FormEvent<FormControl>): void {
        this.setState({
            newName: (e.currentTarget as any as {value: string}).value.trim()
        });
    }

    protected onSubmit (): void {
        this.props.submitHandler(this.props.formatConvert(this.state.newName));
    }
}