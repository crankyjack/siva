import * as React from "react";
import {Col, Glyphicon, ListGroup, ListGroupItem} from "react-bootstrap";
import Latex from "react-latex";
import NewVocabularySymbolModal from "./new-vocabulary-symbol-modal";
import autobind from "autobind-decorator";
import {filterNormalizedStateIdCategory, mapNormalizedStateIdCategory, NormalizedStateIdCategory} from "../../../../Common/utils/util";
import {VocabularySymbol} from "../vocabulary-symbol";
import {MainComponent} from "../../../../App/components/main-component";
import VocabularySymbolView from "./vocabulary-symbol-view";
import MainState from "../../../../App/flux/main-state";
import {Dispatch} from "redux";

export interface VocabularyColumnViewProps {
    vocabularyId: number
    bsColumnWidth: number
    latexHeading: string
    symbolIds: number[]
    symbolToLatex: (item: any) => string
    fillAndShowModal: typeof MainComponent.prototype.fillAndShowModal
    vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>
    symbolType: string
    appendHandler: (name: string) => void
    nameFormatHint: string,
    nameFormatConvert: (name: string) => string
    vocabularyEditable: boolean
    dispatch: Dispatch<MainState>
}

export interface VocabularyColumnViewState {

}

export default class VocabularyColumnView extends React.PureComponent<VocabularyColumnViewProps, VocabularyColumnViewState> {
    @autobind
    plusClickHandler () {
        this.props.fillAndShowModal(NewVocabularySymbolModal, {
            type: this.props.symbolType,
            takenNames: new Set(mapNormalizedStateIdCategory(this.props.vocabularySymbols, voabularySymbol => voabularySymbol.symbol)),
            submitHandler: this.props.appendHandler,
            formatHint: this.props.nameFormatHint,
            formatConvert: this.props.nameFormatConvert
        } as Partial<VocabularyColumnViewProps>);
    }

    getFilteredVocabularySymbols () {
        return filterNormalizedStateIdCategory(this.props.vocabularySymbols, this.props.symbolIds);
    }

    render () {
        let listGroupItems;

        if (this.props.symbolIds.length > 0) {
            listGroupItems = mapNormalizedStateIdCategory(this.getFilteredVocabularySymbols(), vocabularySymbol => (
                <VocabularySymbolView
                    key={vocabularySymbol.id}
                    vocabularyId={this.props.vocabularyId}
                    symbolType={this.props.symbolType}
                    vocabularySymbol={vocabularySymbol}
                    vocabularySymbols={this.props.vocabularySymbols}
                    symbolToLatex={this.props.symbolToLatex}
                    editable={this.props.vocabularyEditable}
                    fillAndShowModal={this.props.fillAndShowModal}
                    nameFormatConvert={this.props.nameFormatConvert}
                    nameFormatHint={this.props.nameFormatHint}
                    dispatch={this.props.dispatch}
                />
            ));
        }
        else {
            listGroupItems = (
                <ListGroupItem
                    key="empty"
                    className="list-group-item-empty"
                    children="(empty)"
                />
            );
        }
        return (
            <Col
                className="vocabulary-alphabet text-center"
                xs={this.props.bsColumnWidth}
            >
                <Latex
                    children={this.props.latexHeading}
                />

                <div
                    className="list-wrapper"
                >
                    <ListGroup
                        className={this.props.symbolIds.length > 0 ? "" : "empty"}
                    >
                        {
                            listGroupItems
                        }
                    </ListGroup>
                    {
                        !this.props.vocabularyEditable ||
                        <Glyphicon
                            className="add-icon"
                            glyph="plus-sign"
                            onClick={this.plusClickHandler}
                        />
                    }
                </div>
            </Col>
        );
    }
}