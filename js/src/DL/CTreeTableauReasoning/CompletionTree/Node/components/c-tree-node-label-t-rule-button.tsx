import * as React from "react";
import {CTreeConstants, CTreeEventTarget} from "../../utils/c-tree-constants";
import {getRoundedRectanglePath, NormalizedStateIdCategory} from "../../../../../Common/utils/util";
import {default as CTreeView} from "../../components/c-tree-view";
import {VocabularySymbol} from "../../../../Denotation/Vocabulary/vocabulary-symbol";
import CTreeNode from "../c-tree-node";
import CTreeEdge from "../../Edge/c-tree-edge";
import {Dispatch} from "redux";
import {CTreeContradictions} from "../../utils/c-tree-helper-functions";
import CTreeViewMode from "../../Mode/components/c-tree-view-mode";
import MainState from "../../../../../App/flux/main-state";
import Expression from "../../../../Denotation/KnowledgeBase/Expression/expression";
import TRuleAxiomChoiceModal, {TRuleAxiomChoiceModalData} from "./t-rule-axiom-choice-modal";

export interface CTreeNodeLabelTRuleButtonProps {
    wrappedTextData: WrappedTextData[]
    onMouseEvent: typeof CTreeViewMode.prototype.handleMouseEvent
    cTreeId: number
    cTreeNodeId: number
    expressions: NormalizedStateIdCategory<Expression>
    cTreeNodes: NormalizedStateIdCategory<CTreeNode>
    cTreeEdges: NormalizedStateIdCategory<CTreeEdge>
    vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>
    dispatch: Dispatch<MainState>
    applicableTRuleExists: boolean
    cTreeContradictions: CTreeContradictions
    cTreeNodeBlockedBy: number | undefined
}

export interface WrappedTextData {
    text: string,
    x: number,
    y: number,
    width: number,
    textHeight: number
}

export default class CTreeNodeLabelTRuleButton extends React.PureComponent<CTreeNodeLabelTRuleButtonProps> {
    firstBlock: SVGPathElement;
    classes: string[] = [];

    updateClasses (props: CTreeNodeLabelTRuleButtonProps) {
        this.classes = [];
        if (!props.applicableTRuleExists) {
            this.classes.push("disabled");
        }
    }

    componentWillUpdate (nextProps: Readonly<CTreeNodeLabelTRuleButtonProps>, nextState: Readonly<{}>, nextContext: any): void {
        this.updateClasses(nextProps);
    }

    componentWillMount (): void {
        this.updateClasses(this.props);
    }

    chooseTRuleToApply (cTreeView: CTreeView) {
        if (this.props.applicableTRuleExists) {
            cTreeView.props.fillAndShowModal(TRuleAxiomChoiceModal, {
                tableauId: cTreeView.props.tableau.id,
                activeTableauNodeId: cTreeView.props.tableau.activeNodeId,
                tableauNodes: cTreeView.tableauNodes,
                cTreeNodeId: this.props.cTreeNodeId,
                expressions: this.props.expressions,
                vocabularySymbols: this.props.vocabularySymbols,
                tBoxAxiomIds: cTreeView.props.knowledgeBase.tBoxAxiomIds,
                cTreeNodes: this.props.cTreeNodes,
                cTreeEdges: this.props.cTreeEdges,
                dispatch: this.props.dispatch
            } as TRuleAxiomChoiceModalData);
        }
    }

    render () {
        const svgElements = [];

        const nodeLabelConceptMouseEventHandler = (e: React.MouseEvent<Element>) => this.props.onMouseEvent(e, CTreeEventTarget.NODE_LABEL_ITEM, this);

        for (let i = 0; i < this.props.wrappedTextData.length; i++) {
            const data = this.props.wrappedTextData[i];
            const binaryIsFirst = i === 0 ? 1 : 0;
            svgElements.push(
                <path
                    key={`path-${data.text}-${i}`}
                    {...(i === 0 ?
                            {
                                ref: block => {
                                    if (block) {
                                        this.firstBlock = block;
                                    }
                                }
                            } :
                            {}
                    )}
                    d={getRoundedRectanglePath(
                        data.x + binaryIsFirst * (-data.textHeight - 2 * CTreeConstants.NODE.LABEL.CONCEPT.PADDING.HORIZONTAL.DEFAULT),
                        data.y,
                        data.width + binaryIsFirst * (data.textHeight + 2 * CTreeConstants.NODE.LABEL.CONCEPT.PADDING.HORIZONTAL.DEFAULT) + 2 * CTreeConstants.NODE.LABEL.CONCEPT.PADDING.HORIZONTAL.DEFAULT,
                        data.textHeight + 2 * CTreeConstants.NODE.LABEL.CONCEPT.PADDING.VERTICAL.DEFAULT,
                        i === 0 ? CTreeConstants.NODE.LABEL.CONCEPT.RADIUS.DEFAULT : 0,
                        i === this.props.wrappedTextData.length - 1 ? CTreeConstants.NODE.LABEL.CONCEPT.RADIUS.DEFAULT : 0,
                        i === this.props.wrappedTextData.length - 1 ? CTreeConstants.NODE.LABEL.CONCEPT.RADIUS.DEFAULT : 0,
                        i === 0 ? CTreeConstants.NODE.LABEL.CONCEPT.RADIUS.DEFAULT : 0)}
                    onClick={nodeLabelConceptMouseEventHandler}
                />
            );

            if (binaryIsFirst) {
                svgElements.push(
                    <path
                        key={`plus-symbol-bg`}
                        className="plus-symbol"
                        d={getRoundedRectanglePath(
                            data.x - data.textHeight - 2 * CTreeConstants.NODE.LABEL.CONCEPT.PADDING.HORIZONTAL.DEFAULT,
                            data.y,
                            data.textHeight + 2 * CTreeConstants.NODE.LABEL.CONCEPT.PADDING.HORIZONTAL.DEFAULT,
                            data.textHeight + 2 * CTreeConstants.NODE.LABEL.CONCEPT.PADDING.VERTICAL.DEFAULT,
                            i === 0 ? CTreeConstants.NODE.LABEL.CONCEPT.RADIUS.DEFAULT : 0,
                            i === this.props.wrappedTextData.length - 1 ? CTreeConstants.NODE.LABEL.CONCEPT.RADIUS.DEFAULT : 0,
                            i === this.props.wrappedTextData.length - 1 ? CTreeConstants.NODE.LABEL.CONCEPT.RADIUS.DEFAULT : 0,
                            i === 0 ? CTreeConstants.NODE.LABEL.CONCEPT.RADIUS.DEFAULT : 0)}
                        pointerEvents="none"
                    />
                );
                svgElements.push(
                    <text
                        className="plus-symbol"
                        textAnchor="middle"
                        dominantBaseline="middle"
                        key={`plus-symbol`}
                        x={data.x + data.textHeight / 2 - data.textHeight - 2 * CTreeConstants.NODE.LABEL.CONCEPT.PADDING.HORIZONTAL.DEFAULT + CTreeConstants.NODE.LABEL.CONCEPT.PADDING.VERTICAL.DEFAULT}
                        y={data.y + data.textHeight / 2 + CTreeConstants.NODE.LABEL.CONCEPT.PADDING.VERTICAL.DEFAULT}
                        pointerEvents="none"
                    >
                        +
                    </text>
                );
            }

            svgElements.push(
                <text
                    textAnchor="middle"
                    dominantBaseline="text-before-edge"
                    key={`text-${data.text}-${i}`}
                    x={data.x + (data.width / 2) + CTreeConstants.NODE.LABEL.CONCEPT.PADDING.HORIZONTAL.DEFAULT}
                    y={data.y + CTreeConstants.NODE.LABEL.CONCEPT.PADDING.VERTICAL.DEFAULT}
                    pointerEvents="none"
                >
                    {data.text}
                </text>
            );
        }
        return (
            <g
                className={["ctree-node-label-t-rule-button-group", ...this.classes].join(" ")}
            >
                {svgElements}
            </g>
        );
    }
}