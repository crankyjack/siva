import * as React from "react";
import {CTreeConstants, CTreeEventTarget} from "../../utils/c-tree-constants";
import {default as CTreeView} from "../../components/c-tree-view";
import CTreeDraggable from "../../utils/c-tree-draggable";
import autobind from "autobind-decorator";
import {NormalizedStateIdCategory, preventDefault} from "../../../../../Common/utils/util";
import CTreeNode from "../c-tree-node";
import CTreeEdge from "../../Edge/c-tree-edge";
import {Dispatch} from "redux";
import CTreeNodeLabelContentView from "./c-tree-node-label-content-view";
import {VocabularySymbol} from "../../../../Denotation/Vocabulary/vocabulary-symbol";
import {CTreeContradictions} from "../../utils/c-tree-helper-functions";
import CTreeViewMode from "../../Mode/components/c-tree-view-mode";
import MainState from "../../../../../App/flux/main-state";
import Expression from "../../../../Denotation/KnowledgeBase/Expression/expression";

export interface CTreeNodeLabelViewProps {
    cTreeId: number
    cTreeNodeId: number
    measureSVGText: typeof CTreeView.prototype.measureSVGText
    expressions: NormalizedStateIdCategory<Expression>
    cTreeNodes: NormalizedStateIdCategory<CTreeNode>
    cTreeEdges: NormalizedStateIdCategory<CTreeEdge>
    vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>
    onMouseEvent: typeof CTreeViewMode.prototype.handleMouseEvent,
    dispatch: Dispatch<MainState>
    applicableTRuleExists: boolean
    cTreeContradictions: CTreeContradictions
    cTreeNodeBlockedBy: number | undefined
}

export interface CTreeNodeLabelViewState {
    width: number,
    height: number
}

export default class CTreeNodeLabelView extends React.PureComponent<CTreeNodeLabelViewProps, CTreeNodeLabelViewState> {

    constructor (props: CTreeNodeLabelViewProps) {
        super(props);

        this.state = {
            width: this.props.cTreeNodes.byId[this.props.cTreeNodeId].labelWidth,
            height: this.props.cTreeNodes.byId[this.props.cTreeNodeId].labelHeight
        };
    }

    get cTreeNode () {
        return this.props.cTreeNodes.byId[this.props.cTreeNodeId];
    }

    @autobind
    setSize (width: number, height: number): void {
        this.setState({
            width: width,
            height: height
        });
    }

    render () {
        const labelMouseEventHandler = (e: React.MouseEvent<Element>) => this.props.onMouseEvent(e, CTreeEventTarget.NODE_LABEL, this);

        return (
            <g
                transform={`translate(${CTreeConstants.NODE.LABEL.X.DEFAULT},${CTreeConstants.NODE.LABEL.Y.DEFAULT})`}
            >
                <rect
                    className={CTreeConstants.NODE.LABEL.BACKGROUND.CLASS.BASE}
                    rx={CTreeConstants.NODE.LABEL.BACKGROUND.RADIUS.X.DEFAULT}
                    ry={CTreeConstants.NODE.LABEL.BACKGROUND.RADIUS.Y.DEFAULT}
                    width={this.state.width}
                    height={this.state.height}
                    onDragStart={preventDefault}
                />
                <clipPath
                    id={`ctree-node-label-clip-${this.props.cTreeNodeId}`}
                >
                    <rect
                        x={CTreeConstants.NODE.LABEL.BACKGROUND.BORDER.WIDTH.DEFAULT}
                        y={CTreeConstants.NODE.LABEL.BACKGROUND.BORDER.WIDTH.DEFAULT}
                        rx={CTreeConstants.NODE.LABEL.BACKGROUND.RADIUS.X.DEFAULT}
                        ry={CTreeConstants.NODE.LABEL.BACKGROUND.RADIUS.Y.DEFAULT}
                        width={this.state.width - 2 * CTreeConstants.NODE.LABEL.BACKGROUND.BORDER.WIDTH.DEFAULT}
                        height={this.state.height - 2 * CTreeConstants.NODE.LABEL.BACKGROUND.BORDER.WIDTH.DEFAULT}
                    />
                </clipPath>
                <g
                    clipPath={`url(#ctree-node-label-clip-${this.props.cTreeNodeId})`}
                >
                    <g
                        transform={`translate(${CTreeConstants.NODE.LABEL.BACKGROUND.PADDING.X.DEFAULT},${CTreeConstants.NODE.LABEL.BACKGROUND.PADDING.Y.DEFAULT})`}
                        onClick={labelMouseEventHandler}
                        onContextMenu={labelMouseEventHandler}
                        onMouseMove={labelMouseEventHandler}
                        onMouseDown={labelMouseEventHandler}
                        onMouseUp={labelMouseEventHandler}
                    >
                        {
                            <CTreeNodeLabelContentView
                                applicableTRuleExists={this.props.applicableTRuleExists}
                                cTreeId={this.props.cTreeId}
                                measureSVGText={this.props.measureSVGText}
                                cTreeNodeId={this.props.cTreeNodeId}
                                expressions={this.props.expressions}
                                cTreeNodes={this.props.cTreeNodes}
                                cTreeEdges={this.props.cTreeEdges}
                                vocabularySymbols={this.props.vocabularySymbols}
                                onMouseEvent={this.props.onMouseEvent}
                                dispatch={this.props.dispatch}
                                width={this.state.width}
                                height={this.state.height}
                                cTreeContradictions={this.props.cTreeContradictions}
                                cTreeNodeBlockedBy={this.props.cTreeNodeBlockedBy}
                            />
                        }
                    </g>
                </g>

                <CTreeNodeLabelResizerView
                    nodeX={this.cTreeNode.x}
                    nodeY={this.cTreeNode.y}
                    onMouseEvent={this.props.onMouseEvent}
                    onDrag={this.setSize}
                    width={this.state.width}
                    height={this.state.height}
                />
            </g>
        );
    }
}


export interface CTreeNodeLabelResizerViewProps {
    nodeX: number,
    nodeY: number,
    onMouseEvent: typeof CTreeViewMode.prototype.handleMouseEvent,
    onDrag: CTreeDraggable["onDrag"],
    width: number,
    height: number
}


export class CTreeNodeLabelResizerView extends React.PureComponent<CTreeNodeLabelResizerViewProps> implements CTreeDraggable {
    onDrag (cTreeX: number, cTreeY: number): void {
        const width = Math.max(cTreeX - this.props.nodeX - CTreeConstants.NODE.LABEL.X.DEFAULT, CTreeConstants.NODE.LABEL.WIDTH.MIN);
        const height = Math.max(cTreeY - this.props.nodeY - CTreeConstants.NODE.LABEL.Y.DEFAULT, CTreeConstants.NODE.LABEL.HEIGHT.MIN);

        this.props.onDrag(width, height);
    }

    onDragEnd (): void {

    }

    render () {
        const labelResizeMouseEvent = (e: React.MouseEvent<Element>) => this.props.onMouseEvent(e, CTreeEventTarget.NODE_LABEL_RESIZE, this);

        const radius = CTreeConstants.NODE.LABEL.RESIZE_CIRCLE.RADIUS.DEFAULT;

        return <circle
            className={CTreeConstants.NODE.LABEL.RESIZE_CIRCLE.CLASS.BASE}
            cx={this.props.width - radius * 0.25}
            cy={this.props.height - radius * 0.25}
            r={radius}
            onClick={labelResizeMouseEvent}
            onContextMenu={labelResizeMouseEvent}
            onMouseMove={labelResizeMouseEvent}
            onMouseDown={labelResizeMouseEvent}
            onMouseUp={labelResizeMouseEvent}
            onDragStart={preventDefault}
        />;
    }
}