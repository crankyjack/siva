import * as React from "react";
import {ellipsize} from "../../../../../Common/utils/util";
import autobind from "autobind-decorator";
import {FormControl} from "react-bootstrap";
import {ModalComponentProps, ModalComponentState} from "../../../../../Common/modal-component";
import SingleInputModal, {FormValidationInformation} from "../../../../../Common/components/single-input-modal";
import ExpressionRouter from "../../../../Denotation/KnowledgeBase/Expression/utils/expression-router";

export interface CTreeNodeRenameModalProps extends ModalComponentProps {
    id: number
    type: string
    takenNames: Set<string>
    submitHandler: (name: string) => void
    formatHint: string
    formatConvert: (name: string) => string
    originalName: string
}

export interface CTreeNodeRenameModalState extends ModalComponentState {
    newName: string,
}

export default class CTreeNodeRenameModal extends SingleInputModal<CTreeNodeRenameModalProps, CTreeNodeRenameModalState> {
    constructor (props: CTreeNodeRenameModalProps) {
        super(props);

        this.state = {
            open: true,
            newName: this.props.originalName
        };
    }

    protected getModalTitleText (): string {
        return `Rename node "${ellipsize(this.props.originalName, 15)}"`;
    }

    protected getInputLabelText (): string {
        return "New name:";
    }

    protected getSubmitButtonText (): string {
        return "Rename";
    }

    protected getInputDefaultText (): string {
        return this.props.originalName;
    }

    protected getInputPlaceholderText (): string {
        return "Enter new node name";
    }

    protected getFormValidationInformation (): FormValidationInformation {
        let result: FormValidationInformation = {
            hint: "",
            validationState: undefined,
            buttonBsStyle: null,
            buttonDisabled: false
        };

        if (!ExpressionRouter.validateSymbol(this.state.newName)) {
            result.hint = 'The name can contain only alhpanumeric characters and has to start with an alphabet character';
            result.validationState = "error";
            result.buttonBsStyle = "danger";
            result.buttonDisabled = true;
        }
        else if (this.state.newName === this.props.originalName) {
            result.buttonBsStyle = "primary";
        }
        else if (this.props.takenNames.has(this.state.newName)) {
            result.hint = "This name is already taken by another node and will be modified.";
            result.validationState = "warning";
            result.buttonBsStyle = "warning";
        }
        else {
            result.validationState = "success";
            result.buttonBsStyle = "success";
        }

        return result;
    }

    @autobind
    protected nameInputChangeHandler (e: React.FormEvent<FormControl>): void {
        this.setState({
            newName: (e.currentTarget as any as HTMLInputElement).value.trim()
        });
    }

    protected onSubmit (): void {
        this.props.submitHandler(this.state.newName);
    }
}