import * as React from "react";
import CTreeNode from "../c-tree-node";
import {Dispatch} from "redux";
import {getWrappedSVGData, NormalizedStateIdCategory, SentenceData} from "../../../../../Common/utils/util";
import {default as CTreeView} from "../../components/c-tree-view";
import CTreeEdge from "../../Edge/c-tree-edge";
import {CTreeConstants} from "../../utils/c-tree-constants";
import CTreeNodeLabelConceptView from "./c-tree-node-label-concept-view";
import {VocabularySymbol} from "../../../../Denotation/Vocabulary/vocabulary-symbol";
import {CTreeContradictions} from "../../utils/c-tree-helper-functions";
import CTreeViewMode from "../../Mode/components/c-tree-view-mode";
import MainState from "../../../../../App/flux/main-state";
import Expression from "../../../../Denotation/KnowledgeBase/Expression/expression";
import ExpressionRouter from "../../../../Denotation/KnowledgeBase/Expression/utils/expression-router";
import CTreeNodeLabelTRuleButton from "./c-tree-node-label-t-rule-button";

export interface CTreeNodeLabelContentViewProps {
    cTreeId: number
    cTreeNodeId: number
    measureSVGText: typeof CTreeView.prototype.measureSVGText
    expressions: NormalizedStateIdCategory<Expression>
    cTreeNodes: NormalizedStateIdCategory<CTreeNode>
    cTreeEdges: NormalizedStateIdCategory<CTreeEdge>
    vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>
    onMouseEvent: typeof CTreeViewMode.prototype.handleMouseEvent
    dispatch: Dispatch<MainState>
    width: number
    height: number
    applicableTRuleExists: boolean
    cTreeContradictions: CTreeContradictions
    cTreeNodeBlockedBy: number | undefined
}

export default class CTreeNodeLabelContentView extends React.Component<CTreeNodeLabelContentViewProps> {

    shouldComponentUpdate (nextProps: Readonly<CTreeNodeLabelContentViewProps>, nextState: Readonly<{}>, nextContext: any): boolean {
        return this.props.applicableTRuleExists !== nextProps.applicableTRuleExists ||
            this.props.cTreeNodeBlockedBy !== nextProps.cTreeNodeBlockedBy ||
            this.props.cTreeContradictions !== nextProps.cTreeContradictions ||
            this.props.measureSVGText !== nextProps.measureSVGText ||
            this.props.vocabularySymbols !== nextProps.vocabularySymbols ||
            this.props.onMouseEvent !== nextProps.onMouseEvent ||
            this.props.width !== nextProps.width ||
            this.props.height !== nextProps.height ||
            this.props.cTreeNodes.allIds !== nextProps.cTreeNodes.allIds ||
            Boolean(this.props.cTreeNodes.allIds.find(oldId => this.props.cTreeNodes.byId[oldId].labelConcepts !== nextProps.cTreeNodes.byId[oldId].labelConcepts)) ||
            this.props.cTreeEdges.allIds !== nextProps.cTreeEdges.allIds ||
            Boolean(this.props.cTreeEdges.allIds.find(oldId => this.props.cTreeEdges.byId[oldId].labelRoles !== nextProps.cTreeEdges.byId[oldId].labelRoles));
    }

    render () {
        const lineHeight = this.props.measureSVGText("A").height;

        const sentences = this.props.cTreeNodes.byId[this.props.cTreeNodeId].labelConcepts.map(id => {
            return {
                id: id,
                text: ExpressionRouter.getSimpleString(id, this.props.expressions, this.props.vocabularySymbols)
            };
        }) as SentenceData[];
        sentences.unshift({
            id: 0,
            text: "T-rule",
            leftSpacing: lineHeight + 2 * CTreeConstants.NODE.LABEL.CONCEPT.PADDING.HORIZONTAL.DEFAULT
        });

        const widthLimit = this.props.width - 2 * CTreeConstants.NODE.LABEL.BACKGROUND.PADDING.X.DEFAULT;

        const data = getWrappedSVGData(sentences, this.props.measureSVGText, ",", widthLimit, true, CTreeConstants.NODE.LABEL.CONCEPT.PADDING.HORIZONTAL.DEFAULT);

        const result = [];
        let currentWrappedConceptData = [];
        let currentConceptId;

        const rowHeight = lineHeight + 2 * CTreeConstants.NODE.LABEL.CONCEPT.PADDING.VERTICAL.DEFAULT + CTreeConstants.NODE.LABEL.CONCEPT.SPACING.DEFAULT;

        for (let i = 0; i < data.length; i++) {
            const sentenceEnd = i + 1 >= data.length || data[i].id !== data[i + 1].id;

            if (data[i].id !== "separator") {
                let extraWidth = 0;
                if (!sentenceEnd) {
                    extraWidth = widthLimit - data[i].x - data[i].width - CTreeConstants.NODE.LABEL.CONCEPT.PADDING.HORIZONTAL.DEFAULT;
                }

                currentWrappedConceptData.push({
                    text: data[i].text,
                    x: data[i].x - CTreeConstants.NODE.LABEL.CONCEPT.PADDING.HORIZONTAL.DEFAULT,
                    y: data[i].rowNum * rowHeight,
                    width: data[i].width + extraWidth,
                    textHeight: lineHeight
                });

                if (currentWrappedConceptData.length > 0 && sentenceEnd) {
                    if (data[i].id === 0) {
                        result.push(
                            <CTreeNodeLabelTRuleButton
                                applicableTRuleExists={this.props.applicableTRuleExists}
                                key={data[i].id}
                                wrappedTextData={currentWrappedConceptData}
                                cTreeNodeId={this.props.cTreeNodeId}
                                cTreeId={this.props.cTreeId}
                                expressions={this.props.expressions}
                                cTreeNodes={this.props.cTreeNodes}
                                cTreeEdges={this.props.cTreeEdges}
                                vocabularySymbols={this.props.vocabularySymbols}
                                onMouseEvent={this.props.onMouseEvent}
                                dispatch={this.props.dispatch}
                                cTreeContradictions={this.props.cTreeContradictions}
                                cTreeNodeBlockedBy={this.props.cTreeNodeBlockedBy}
                            />
                        );
                    }
                    else {
                        result.push(
                            <CTreeNodeLabelConceptView
                                key={data[i].id}
                                conceptId={data[i].id as number}
                                wrappedTextData={currentWrappedConceptData}
                                cTreeNodeId={this.props.cTreeNodeId}
                                cTreeId={this.props.cTreeId}
                                expressions={this.props.expressions}
                                cTreeNodes={this.props.cTreeNodes}
                                cTreeEdges={this.props.cTreeEdges}
                                vocabularySymbols={this.props.vocabularySymbols}
                                onMouseEvent={this.props.onMouseEvent}
                                dispatch={this.props.dispatch}
                                cTreeContradictions={this.props.cTreeContradictions}
                                cTreeNodeBlockedBy={this.props.cTreeNodeBlockedBy}
                            />
                        );
                    }
                    currentWrappedConceptData = [];
                }
            }
            else {
                currentConceptId = undefined;
                result.push(
                    <text
                        textAnchor="start"
                        dominantBaseline="text-before-edge"
                        key={`separator-${Math.ceil((result.length - 1) / 2)}`}
                        x={data[i].x}
                        y={data[i].rowNum * rowHeight + 2 * CTreeConstants.NODE.LABEL.CONCEPT.PADDING.VERTICAL.DEFAULT}
                    >
                        {data[i].text}
                    </text>
                );
            }
        }

        return result;
    }
}