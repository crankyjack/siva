export default interface CTreeNode {
    id: number;
    origin: CTREE_NODE_ORIGIN
    nameOrIndividualSymbolId: string | number;
    x: number;
    y: number;
    labelWidth: number,
    labelHeight: number,
    labelConcepts: number[]
}

export enum CTREE_NODE_ORIGIN {
    PROBLEM_INPUT,
    PROBLEM_INITIALIZATION,
    KNOWLEDGE_BASE_INITIALIZATION,
    REASONING_PROCESS
}