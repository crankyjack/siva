import CTreeViewMode from "./c-tree-view-mode";
import CTreeNodeView from "../../Node/components/c-tree-node-view";
import * as React from "react";
import {CTreeNodeLabelResizerView} from "../../Node/components/c-tree-node-label-view";
import CTreeView from "../../components/c-tree-view";
import CTreeNodeLabelConceptView from "../../Node/components/c-tree-node-label-concept-view";
import CTreeNodeLabelTRuleButton from "../../Node/components/c-tree-node-label-t-rule-button";

export default class ModeDefault extends CTreeViewMode {
    getCSSClass (): string[] {
        return ["mode-default"];
    }

    handleNodeNameMouseEvent (e: React.MouseEvent<SVGTextElement>, cTreeNodeView: CTreeNodeView): void {
        switch (e.type) {
            case "click":
                cTreeNodeView.showRenameModal();
                break;
        }
    }

    handleNodeMouseEvent (e: React.MouseEvent<SVGCircleElement>, cTreeNodeView: CTreeNodeView): void {
        switch (e.type) {
            case "mousedown":
                if (e.button === 0) {
                    this.cTreeView.setDraggedView(cTreeNodeView);
                }
                break;
            case "contextmenu":
                e.preventDefault();
                cTreeNodeView.showContextMenu(e.pageX, e.pageY);
                break;
        }
    }

    handleNodeLabelResizerMouseEvent (e: React.MouseEvent<SVGCircleElement>, cTreeNodeLabelResizerView: CTreeNodeLabelResizerView): void {
        switch (e.type) {
            case "mousedown":
                if (e.button === 0) {
                    this.cTreeView.setDraggedView(cTreeNodeLabelResizerView);
                }
                break;
        }
    }


    handleCTreeMouseEvent (e: React.MouseEvent<SVGSVGElement>, cTreeView: CTreeView): void {
        switch (e.type) {
            case "mouseup":
                if (e.button === 0) {
                    this.cTreeView.setDraggedView(undefined);
                }
        }
    }

    handleNodeLabelTRuleButtonMouseEvent (e: React.MouseEvent<SVGTextElement>, cTreeNodeLabelTRuleButton: CTreeNodeLabelTRuleButton): void {
        switch (e.type) {
            case "click":
                cTreeNodeLabelTRuleButton.chooseTRuleToApply(this.cTreeView);
                break;
        }
    }

    handleNodeLabelConceptMouseEvent (e: React.MouseEvent<SVGTextElement>, cTreeNodeLabelConceptView: CTreeNodeLabelConceptView): void {
        switch (e.type) {
            case "click":
                cTreeNodeLabelConceptView.onExpand(this.cTreeView);
                break;
            case "mouseenter":
                this.cTreeView.setPopoverData(cTreeNodeLabelConceptView.getPopoverData());
                break;
            case "mouseleave":
                this.cTreeView.setPopoverData(undefined);
                break;
        }
    }

    onStart (): void {
    }

    onStop (): void {
    }

}
