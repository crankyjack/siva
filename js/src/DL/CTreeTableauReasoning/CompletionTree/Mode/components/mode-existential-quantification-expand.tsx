import CTreeViewMode from "./c-tree-view-mode";
import {default as CTreeView} from "../../components/c-tree-view";
import * as React from "react";
import CTreeNewEdgeView from "../../Edge/components/c-tree-new-edge-view";
import CTreeNewNodeView from "../../Node/components/c-tree-new-node-view";
import {CTreeConstants} from "../../utils/c-tree-constants";
import {getSVGEventCoordinates} from "../../../../../Common/utils/util";
import DL_CTREE_TABLEAU_WORKSPACE_ACTIONS from "../../DLCTreeTableauWorkspace/flux/dl-ctree-tableau-workspace-actions";

export interface ModeExistentialQuantificationExpandData {
    cTreeNodeId: number,
    conceptId: number
}

export default class ModeExistentialQuantificationExpand extends CTreeViewMode {
    protected newEdgeView: CTreeNewEdgeView;
    protected newNodeView: CTreeNewNodeView;
    protected cTreeNodeId: number;
    protected conceptId: number;

    constructor (cTreeView: CTreeView, data: ModeExistentialQuantificationExpandData) {
        super(cTreeView);

        this.cTreeNodeId = data.cTreeNodeId;
        this.conceptId = data.conceptId;
    }

    getCSSClass (): string[] {
        return ["mode-existential-quantification-expand"];
    }

    handleCTreeMouseEvent (e: React.MouseEvent<SVGSVGElement>, cTreeView: CTreeView): void {
        switch (e.type) {
            case "mousemove": {
                const coords = getSVGEventCoordinates(e);
                this.newNodeView.setPosition(coords.x, coords.y);
                this.newEdgeView.setEnd(coords.x, coords.y, CTreeConstants.NODE.RADIUS.DEFAULT);
                break;
            }
            case "click": {
                if (e.button === 0) {
                    const coords = getSVGEventCoordinates(e);
                    this.cTreeView.props.dispatch(DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.CREATE_APPLY_EXISTENTIAL_QUANTIFICATION_TABLEAU_RULE(this.cTreeNodeId, this.conceptId, coords.x, coords.y));
                    this.stop();
                }
                break;
            }
        }
    }

    onStart (): void {
        this.cTreeView.toggleEdgeCreation(cTreeNewEdgeView => {
            cTreeNewEdgeView.setStart(this.cTreeNodeId);
            this.newEdgeView = cTreeNewEdgeView;
        });
        this.cTreeView.toggleNodeCreation(cTreeNewNodeView => {
            this.newNodeView = cTreeNewNodeView;
        });
    }

    onStop (): void {
        if (this.newEdgeView) {
            this.cTreeView.toggleEdgeCreation(this.newEdgeView.props.id);
            delete this.newEdgeView;
        }
        if (this.newNodeView) {
            this.cTreeView.toggleNodeCreation(this.newNodeView.props.id);
            delete this.newNodeView;
        }
    }

    reset (): void {
        this.onStop();
        this.onStart();
    }
}