import CTreeViewMode from "./c-tree-view-mode";
import * as React from "react";
import CTreeView from "../../components/c-tree-view";
import CTreeNodeView from "../../Node/components/c-tree-node-view";
import CTreeNewNodeView from "../../Node/components/c-tree-new-node-view";
import {getSVGEventCoordinates} from "../../../../../Common/utils/util";

export interface ModeSingleNodeProblemInitializationData {
    onStartCallback?: () => void
    nodeName?: string
    onNodeCreate: (x: number, y: number, cTreeId: number) => void
}

export default class ModeSingleNodeInitialization extends CTreeViewMode {
    protected newNodeView?: CTreeNewNodeView;
    protected nodeName?: string;
    protected onStartCallback?: () => void;
    onNodeCreate: (x: number, y: number, cTreeId: number) => void;

    constructor (cTreeView: CTreeView, data: ModeSingleNodeProblemInitializationData) {
        super(cTreeView);

        this.nodeName = data.nodeName;
        this.onNodeCreate = data.onNodeCreate;
        this.onStartCallback = data.onStartCallback;
    }

    getCSSClass (): string[] {
        return ["mode-single-node-initialization"];
    }

    createNode (x: number, y: number, cTreeId: number) {
        this.onNodeCreate(x, y, cTreeId);
        this.stop();
    }

    handleCTreeMouseEvent (e: React.MouseEvent<SVGSVGElement>, cTreeView: CTreeView): void {
        switch (e.type) {
            case "mousemove": {
                if (this.newNodeView) {
                    const coords = getSVGEventCoordinates(e);
                    this.newNodeView.setPosition(coords.x, coords.y);
                }
                break;
            }
            case "click": {
                if (e.button === 0) {
                    const coords = getSVGEventCoordinates(e);
                    this.createNode(coords.x, coords.y, cTreeView.id);
                }
                break;
            }
        }
    }

    handleNodeMouseEvent (e: React.MouseEvent<SVGCircleElement>, cTreeNodeView: CTreeNodeView): void {
        e.stopPropagation();
    }

    onStart (): void {
        this.cTreeView.toggleNodeCreation(cTreeNewNodeView => {
            this.newNodeView = cTreeNewNodeView;
            cTreeNewNodeView.setName(this.nodeName);
        });

        !this.onStartCallback || this.onStartCallback();
    }

    onStop (): void {
        if (this.newNodeView) {
            this.cTreeView.toggleNodeCreation(this.newNodeView.props.id);
        }
    }
}