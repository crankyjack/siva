import CTreeViewMode from "./c-tree-view-mode";
import * as React from "react";
import CTreeView from "../../components/c-tree-view";
import CTreeNodeView from "../../Node/components/c-tree-node-view";
import CTREE_ACTIONS from "../../flux/c-tree-actions";
import CTreeNewNodeView from "../../Node/components/c-tree-new-node-view";
import CTREE_HELPER_FUNCTIONS from "../../utils/c-tree-helper-functions";
import {CTREE_NODE_ORIGIN} from "../../Node/c-tree-node";
import {getSVGEventCoordinates} from "../../../../../Common/utils/util";

export default class ModeNodeCreating extends CTreeViewMode {
    private newNodeView?: CTreeNewNodeView;

    getCSSClass (): string[] {
        return ["mode-node-create"];
    }

    createNode (x: number, y: number, cTreeId: number) {
        this.cTreeView.props.dispatch(CTREE_ACTIONS.CREATE_CREATE_NODE(cTreeId, CTREE_NODE_ORIGIN.REASONING_PROCESS, x, y));

        this.stop();
    }

    handleCTreeMouseEvent (e: React.MouseEvent<SVGSVGElement>, cTreeView: CTreeView) {
        switch (e.type) {
            case "mousemove":
                if (this.newNodeView) {
                    const coords = getSVGEventCoordinates(e);
                    this.newNodeView.setPosition(coords.x, coords.y);
                }
                break;

            case "click":
                if (e.button === 0) {
                    const coords = getSVGEventCoordinates(e);
                    this.createNode(coords.x, coords.y, cTreeView.id);
                }
                break;
        }
    }

    handleNodeMouseEvent (e: React.MouseEvent<SVGCircleElement>, cTreeNodeView: CTreeNodeView): void {
        e.stopPropagation();
    }

    onStart (): void {
        this.cTreeView.toggleNodeCreation(cTreeNewNodeView => {
            this.newNodeView = cTreeNewNodeView;
            this.newNodeView.setName(CTREE_HELPER_FUNCTIONS.getNewNodeDefaultName(this.cTreeView.props.cTree, this.cTreeView.cTreeNodes, this.cTreeView.vocabularySymbols));
        });
    }

    onStop (): void {
        if (this.newNodeView) {
            this.cTreeView.toggleNodeCreation(this.newNodeView.props.id);
            delete this.newNodeView;
        }
    }
}