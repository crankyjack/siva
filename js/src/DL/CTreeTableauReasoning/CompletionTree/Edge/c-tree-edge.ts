export default interface CTreeEdge {
    id: number,
    startNodeId: number,
    endNodeId: number,
    labelRoles: number[]
}
