import {TableauNodeState} from "../../../../Tableau/tableau-node";

export namespace CTreeConstants {
    export const EDGE = {
        CLASS: {
            BASE: "ctree-edge",
            NEW: "ctree-edge-new"
        },
        NAME: {
            CLASS: {
                BASE: "ctree-edge-label"
            }
        },
        STROKE_WIDTH: {
            BASE: 3
        }
    };
    export const NODE = {
        CLASS: {
            BASE: "ctree-node",
            NEW: "ctree-node-new"
        },
        GROUP: {
            CLASS: {
                CLASH: "ctree-node-group-clash",
                BLOCKED: "ctree-node-group-blocked",
                T_RULE_APPLICABLE: "ctree-node-group-t-rule-applicable"
            }
        },
        NAME: {
            CLASS: {
                BASE: "ctree-node-name"
            },
            D: {
                X: {
                    INIT: 0,
                    DEFAULT: 15,
                    HOVER: 15
                },
                Y: {
                    INIT: 0,
                    DEFAULT: -8,
                    HOVER: -8
                }
            }
        },
        T_RULE_APPLICABLE: {
            EXTRA_RADIUS: {
                DEFAULT: 3
            }
        },
        INDIVIDUAL: {
            STROKE_WIDTH: {
                DEFAULT: 1.5
            },
            STROKE: {
                DEFAULT: "#ffffffa0"
            }
        },
        LABEL: {
            CLASS: {
                BASE: "ctree-node-label"
            },
            X: {
                DEFAULT: 15
            },
            Y: {
                DEFAULT: 8
            },
            WIDTH: {
                DEFAULT: 150,
                MIN: 50
            },
            HEIGHT: {
                DEFAULT: 55,
                MIN: 28
            },
            RESIZE_CIRCLE: {
                CLASS: {
                    BASE: "ctree-node-label-resizer"
                },
                RADIUS: {
                    DEFAULT: 8
                }
            },
            CONCEPT: {
                RADIUS: {
                    DEFAULT: 2
                },
                PADDING: {
                    HORIZONTAL: {
                        DEFAULT: 3.5
                    },
                    VERTICAL: {
                        DEFAULT: 3
                    }
                },
                SPACING: {
                    DEFAULT: 3
                }
            },
            BACKGROUND: {
                CLASS: {
                    BASE: "ctree-node-label-bg"
                },
                BORDER: {
                    WIDTH: {
                        DEFAULT: 1
                    }
                },
                PADDING: {
                    X: {
                        DEFAULT: 4
                    },
                    Y: {
                        DEFAULT: 4
                    }
                },
                RADIUS: {
                    X: {
                        DEFAULT: 2
                    },
                    Y: {
                        DEFAULT: 2
                    }
                }
            }
        },
        RADIUS: {
            INIT: 0,
            DEFAULT: 12,
            HOVER: 14
        }
    };
    export const LAYER = {
        NODES: {
            ID: "nodes_layer"
        },
        EDGES: {
            ID: "edges_layer"
        }
    };
    export const MARKER = {
        ARROWHEAD: {
            START: {
                ID: "marker-arrowhead-start"
            },
            END: {
                ID: "marker-arrowhead-end"
            }
        }
    };
    export const TRANSITION = {
        DURATION: {
            QUICK: 300
        }
    };
    export const CTREE = {
        CLASS: {
            BASE: "ctree",
            TABLEAU_NODE_STATE: {
                [TableauNodeState.NEUTRAL]: "ctree--reasoning",
                [TableauNodeState.SUCCESS]: "ctree--complete-clash-free",
                [TableauNodeState.FAILURE]: "ctree--contains-clash"
            }
        },
        WRAPPER: {
            CLASS: {
                BASE: "ctree-wrapper"
            }
        }
    };
    export const STATUS_BAR = {
        CLASS: {
            BASE: "ctree-status-bar"
        }
    };
}

export enum CTreeEventTarget {
    CTREE,
    NODE,
    NODE_NAME,
    NODE_LABEL,
    NODE_LABEL_ITEM,
    NODE_LABEL_RESIZE,
    EDGE,
    EDGE_LABEL
}