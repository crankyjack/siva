export default interface CTreeDraggable {
    onDrag (cTreeX: number, cTreeY: number): void;

    onDragEnd (): void;
}