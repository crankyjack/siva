import * as React from "react";
import {ReactNode} from "react";
import {CTreeConstants, CTreeEventTarget} from "../utils/c-tree-constants";
import CTreeNodeView from "../Node/components/c-tree-node-view";
import autobind from "autobind-decorator";
import CTreeEdgeView from "../Edge/components/c-tree-edge-view";
import CTreeNode from "../Node/c-tree-node";
import CTree, {CTreeReasoningProcessStage} from "../c-tree";
import CTreeViewDefs from "./c-tree-view-defs";
import CTreeDraggable from "../utils/c-tree-draggable";
import CTreeEdge from "../Edge/c-tree-edge";
import {Dispatch} from "redux";
import CTreeNewEdgeView from "../Edge/components/c-tree-new-edge-view";
import {VocabularySymbol} from "../../../Denotation/Vocabulary/vocabulary-symbol";
import {Button, ButtonToolbar, Glyphicon, Panel, Popover, Sizes} from "react-bootstrap";
import CTreeNewNodeView from "../Node/components/c-tree-new-node-view";
import KnowledgeBase from "../../../Denotation/KnowledgeBase/knowledge-base";
import Vocabulary from "../../../Denotation/Vocabulary/vocabulary";
import CTREE_HELPER_FUNCTIONS, {CTreeBlockedNodes, CTreeContradictions} from "../utils/c-tree-helper-functions";
import deepEqual from "deep-equal";
import {Tableau} from "../../../../Tableau/tableau";
import TableauNode from "../../../../Tableau/tableau-node";
import TableauModal from "../../../../Tableau/components/tableau-modal";
import DLCTreeSolvableProblemRouter from "../../../Problem/utils/dl-ctree-solvable-problem-router";
import {appendNormalizedToState, BoundingBox2D, FunctionWithId, getNormalizedStateIdCategoryIterator, initNormalizedCategoryInState, mapNormalizedStateIdCategory, Measure2D, measureSVGText, noop, NormalizedStateIdCategory, removeNormalizedFromState, updateBoundingBox, updateComponentNormalizedStateIfNotDeepEqual} from "../../../../Common/utils/util";
import {MainComponent} from "../../../../App/components/main-component";
import Problem from "../../../../Problem/problem";
import MainState from "../../../../App/flux/main-state";
import CTreeViewMode, {CTreeViewModeClass} from "../Mode/components/c-tree-view-mode";
import ModeDefault from "../Mode/components/mode-default";
import ModeKnowledgeBaseInitialization from "../Mode/components/mode-knowledge-base-initialization";
import CTreeStatusBar from "./c-tree-status-bar";
import Expression from "../../../Denotation/KnowledgeBase/Expression/expression";
import TABLEAU_HELPER_FUNCTIONS from "../../../../Tableau/utils/tableau-helper-functions";
import TABLEAU_ACTIONS from "../../../../Tableau/flux/tableau-actions";
import ConceptRouter, {ConceptTableauRuleNode} from "../../../Denotation/KnowledgeBase/Expression/utils/concept-router";

export interface CTreeViewProps {
    knowledgeBase: KnowledgeBase
    vocabulary: Vocabulary
    expressions: NormalizedStateIdCategory<Expression>
    filteredVocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>
    cTree: CTree
    filteredCTreeNodes: NormalizedStateIdCategory<CTreeNode>
    filteredCTreeEdges: NormalizedStateIdCategory<CTreeEdge>
    tableau: Tableau
    filteredTableauNodes: NormalizedStateIdCategory<TableauNode>
    fillAndShowModal: typeof MainComponent.prototype.fillAndShowModal
    updateModal: typeof MainComponent.prototype.updateModal
    fillAndShowContextMenu: typeof MainComponent.prototype.fillAndShowContextMenu
    problem: Problem
    onCancel: () => void
    dispatch: Dispatch<MainState>
}

export interface CTreeViewState {
    mode: CTreeViewMode,
    popoverData: PopoverData | undefined,
    newEdges: NormalizedStateIdCategory<FunctionWithId<(cTreeNewEdgeView: CTreeNewEdgeView) => void>>
    newNodes: NormalizedStateIdCategory<FunctionWithId<(cTreeNewEdgeView: CTreeNewNodeView) => void>>
    vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>
    cTreeNodes: NormalizedStateIdCategory<CTreeNode>
    cTreeEdges: NormalizedStateIdCategory<CTreeEdge>
    tableauNodes: NormalizedStateIdCategory<TableauNode>
    currentTableauNodeParentId: null | number
    cTreeContradictions: CTreeContradictions
    cTreeBlockedNodes: CTreeBlockedNodes
    tableauModalId: number | undefined
    wrapperWidth: number
    wrapperHeight: number
    viewportBoundingBox?: BoundingBox2D
}

export interface PopoverData {
    arrowOffsetLeft?: number | string
    arrowOffsetTop?: number | string
    bsSize?: Sizes
    bsStyle?: string
    placement?: string
    positionLeft?: number | string
    positionTop?: number | string
    title?: ReactNode
    children?: ReactNode
}

export default class CTreeView extends React.PureComponent<CTreeViewProps, CTreeViewState> {
    refs: {
        measureTextNode: SVGTextElement
        svgContainer: HTMLDivElement
    };

    private newNodeId = 1;
    private newEdgeId = 1;
    draggedView: CTreeDraggable | undefined;

    constructor (props: CTreeViewProps) {
        super(props);

        let state = {
            cTreeNodes: props.filteredCTreeNodes,
            cTreeEdges: props.filteredCTreeEdges,
            vocabularySymbols: props.filteredVocabularySymbols,
            tableauNodes: props.filteredTableauNodes,
            popoverData: undefined,
            cTreeContradictions: CTREE_HELPER_FUNCTIONS.getCTreeContradictions(this.props.cTree, this.props.expressions, this.props.filteredCTreeNodes),
            cTreeBlockedNodes: CTREE_HELPER_FUNCTIONS.getCTreeBlockedNodes(this.props.cTree, this.props.expressions, this.props.filteredCTreeNodes, this.props.filteredCTreeEdges),
            mode: new ModeDefault(this),
            wrapperWidth: 0,
            wrapperHeight: 0

        } as any as CTreeViewState;

        state = initNormalizedCategoryInState(state, "newNodes");
        state = initNormalizedCategoryInState(state, "newEdges");

        this.state = state;
    }

    getReasoningProcessStageDefaultModeInstance (reasoningProcessStage = this.props.cTree.reasoningProcessStage) {
        switch (reasoningProcessStage) {
            case CTreeReasoningProcessStage.PROBLEM_INTIALIZATION: {
                return DLCTreeSolvableProblemRouter.getCTreeProblemInitializationMode(this);
            }
            case CTreeReasoningProcessStage.KNOWLEDGE_BASE_INITIALIZATION: {
                return new ModeKnowledgeBaseInitialization(this);
            }
            case CTreeReasoningProcessStage.TABLEAU_RULE_APPLICATION: {
                return new ModeDefault(this);
            }
        }
    }

    componentWillReceiveProps (nextProps: Readonly<CTreeViewProps>, nextContext: any): void {
        if (this.props.cTree.reasoningProcessStage !== nextProps.cTree.reasoningProcessStage) {
            this.setMode(undefined, {}, nextProps.cTree.reasoningProcessStage);
        }

        let conceptLabelsChanged = !deepEqual(this.state.cTreeNodes.allIds, nextProps.filteredCTreeNodes.allIds);

        if (!conceptLabelsChanged) {
            for (const node of getNormalizedStateIdCategoryIterator(this.state.cTreeNodes)) {
                if (node.labelConcepts !== nextProps.filteredCTreeNodes.byId[node.id].labelConcepts) {
                    conceptLabelsChanged = true;
                    break;
                }
            }
        }

        if (conceptLabelsChanged) {
            const cTreeContradictions = CTREE_HELPER_FUNCTIONS.getCTreeContradictions(nextProps.cTree, nextProps.expressions, nextProps.filteredCTreeNodes);
            if (!deepEqual(cTreeContradictions, this.state.cTreeContradictions)) {
                this.setState({
                    cTreeContradictions: cTreeContradictions
                });
            }
        }

        if (conceptLabelsChanged || !deepEqual(this.state.cTreeEdges.allIds, nextProps.filteredCTreeEdges.allIds)) {
            const cTreeBlockedNodes = CTREE_HELPER_FUNCTIONS.getCTreeBlockedNodes(nextProps.cTree, nextProps.expressions, nextProps.filteredCTreeNodes, nextProps.filteredCTreeEdges);
            if (!deepEqual(cTreeBlockedNodes, this.state.cTreeBlockedNodes)) {
                this.setState({
                    cTreeBlockedNodes: cTreeBlockedNodes
                });
            }
        }

        if (typeof this.state.tableauModalId !== "undefined" && (this.props.tableau !== nextProps.tableau || !deepEqual(this.props.filteredTableauNodes, this.tableauNodes))) {
            this.props.updateModal(this.state.tableauModalId, {
                tableau: nextProps.tableau,
                tableauNodes: nextProps.filteredTableauNodes
            });
        }

        if (this.props.tableau !== nextProps.tableau) {
            this.updateCurrentTableauNodeParentId(nextProps);
        }

        updateComponentNormalizedStateIfNotDeepEqual(this, "vocabularySymbols", nextProps.filteredVocabularySymbols);
        updateComponentNormalizedStateIfNotDeepEqual(this, "cTreeNodes", nextProps.filteredCTreeNodes, this.updateViewportBoundingBox);
        updateComponentNormalizedStateIfNotDeepEqual(this, "cTreeEdges", nextProps.filteredCTreeEdges);
        updateComponentNormalizedStateIfNotDeepEqual(this, "tableauNodes", nextProps.filteredTableauNodes);
    }

    componentWillMount (): void {
        this.setMode(undefined);
        this.updateCurrentTableauNodeParentId(this.props);
    }

    get id () {
        return this.props.cTree.id;
    }

    get vocabularySymbols () {
        return this.state.vocabularySymbols;
    }

    get cTreeNodes () {
        return this.state.cTreeNodes;
    }

    get cTreeEdges () {
        return this.state.cTreeEdges;
    }

    get tableauNodes () {
        return this.state.tableauNodes;
    }

    @autobind
    setDraggedView (view: CTreeDraggable | undefined) {
        if (this.draggedView) {
            this.draggedView.onDragEnd();
        }
        this.draggedView = view;
    }

    @autobind
    getSVGClass () {
        const result = [CTreeConstants.CTREE.CLASS.BASE];

        const activeTableauNodeState = this.tableauNodes.byId[this.props.tableau.activeNodeId].state;

        if (typeof activeTableauNodeState !== "undefined") {
            result.push(CTreeConstants.CTREE.CLASS.TABLEAU_NODE_STATE[activeTableauNodeState]);
        }

        if (this.state.mode) {
            result.push(...this.state.mode.getCSSClass());
        }


        return result;
    }

    @autobind
    setMode (modeClass: CTreeViewModeClass | undefined, data?: any, reasoningState = this.props.cTree.reasoningProcessStage): void {
        if (modeClass === this.state.mode.constructor) {
            return;
        }

        this.state.mode.onStop();

        this.setState(() => ({
            mode: modeClass ? new modeClass(this, ...data) : this.getReasoningProcessStageDefaultModeInstance(reasoningState)
        }), () => {
            this.state.mode.onStart();
        });
    }

    @autobind
    toggleMode (modeClass: CTreeViewModeClass, data?: any) {
        if (modeClass !== this.state.mode.constructor) {
            this.setMode(modeClass, data);
        }
        else {
            this.setMode(undefined);
        }
    }

    @autobind
    measureSVGText (text: string): Measure2D {
        return measureSVGText(text, this.refs.measureTextNode);
    }

    componentDidMount (): void {
        this.setState({
            wrapperWidth: this.refs.svgContainer.clientWidth,
            wrapperHeight: this.refs.svgContainer.clientHeight
        }, () => {
            this.updateViewportBoundingBox();
        });

        if (this.refs.measureTextNode) {
            this.forceUpdate();
        }
    }

    @autobind
    viewTableau () {
        this.props.fillAndShowModal(TableauModal, {
            tableau: this.props.tableau,
            tableauNodes: this.tableauNodes,
            tableauNodeDescriptionState: {
                DLExpressions: this.props.expressions,
                DLCTreeNodes: this.cTreeNodes,
                DLVocabularySymbols: this.vocabularySymbols
            },
            onShowModal: (id: number) => {
                this.setState({tableauModalId: id});
            },
            onRemoveModal: () => {
                this.setState({tableauModalId: undefined});
            }
        });
    }

    @autobind
    cancelReasoning () {
        this.props.onCancel();
    }

    getRealBoundingBox () {
        let boundingBox: BoundingBox2D;

        if (this.cTreeNodes.allIds.length > 0) {
            const firstNode = this.cTreeNodes.byId[this.cTreeNodes.allIds[0]];
            boundingBox = {
                left: firstNode.x,
                top: firstNode.y,
                right: firstNode.x,
                bottom: firstNode.y
            };
        }
        else {
            boundingBox = {
                left: 0,
                top: 0,
                right: 0,
                bottom: 0
            };
        }

        for (const cTreeNode of getNormalizedStateIdCategoryIterator(this.cTreeNodes)) {
            updateBoundingBox(boundingBox, cTreeNode, 0);

            const nodeLabelBBox = {
                left: cTreeNode.x + CTreeConstants.NODE.LABEL.X.DEFAULT,
                right: cTreeNode.x + CTreeConstants.NODE.LABEL.X.DEFAULT + cTreeNode.labelWidth,
                top: cTreeNode.y + CTreeConstants.NODE.LABEL.Y.DEFAULT,
                bottom: cTreeNode.y + CTreeConstants.NODE.LABEL.Y.DEFAULT + cTreeNode.labelHeight
            };

            updateBoundingBox(boundingBox, nodeLabelBBox, 0);
        }

        return boundingBox;
    }

    updateCurrentTableauNodeParentId (props: CTreeViewProps) {
        this.setState(() => {
            const parentNode = TABLEAU_HELPER_FUNCTIONS.getNodeParent(props.tableau.activeNodeId, props.tableau, props.filteredTableauNodes);

            return {currentTableauNodeParentId: parentNode ? parentNode.id : null};
        });
    }

    @autobind
    updateViewportBoundingBox () {
        const realBoundingBox = this.getRealBoundingBox();

        const realWidth = realBoundingBox.right - realBoundingBox.left;
        const realHeight = realBoundingBox.bottom - realBoundingBox.top;

        if (!this.state.viewportBoundingBox) {
            const widthDeficit = Math.max(this.state.wrapperWidth - realWidth, 0);
            const heightDeficit = Math.max(this.state.wrapperHeight - realHeight, 0);

            return this.setState({
                viewportBoundingBox: {
                    left: realBoundingBox.left - widthDeficit / 2 - this.state.wrapperWidth / 4,
                    right: realBoundingBox.right + widthDeficit / 2 + this.state.wrapperWidth / 4,
                    top: realBoundingBox.top - heightDeficit / 2 - this.state.wrapperHeight / 4,
                    bottom: realBoundingBox.bottom + heightDeficit / 2 + this.state.wrapperHeight / 4
                }
            });
        }

        const resultBoundingBox = {...realBoundingBox};

        const trimmedViewportBoundingBox = {
            left: this.state.viewportBoundingBox.left + this.state.wrapperWidth / 4,
            right: this.state.viewportBoundingBox.right - this.state.wrapperWidth / 4,
            top: this.state.viewportBoundingBox.top + this.state.wrapperHeight / 4,
            bottom: this.state.viewportBoundingBox.bottom - this.state.wrapperHeight / 4
        };

        const trimmedWidth = trimmedViewportBoundingBox.right - trimmedViewportBoundingBox.left;
        const trimmedHeight = trimmedViewportBoundingBox.bottom - trimmedViewportBoundingBox.top;

        if (trimmedWidth < realWidth) {
            // result X coordinates are equal to real
        }
        else {
            if (realBoundingBox.left < trimmedViewportBoundingBox.left) {
                resultBoundingBox.right = resultBoundingBox.left + Math.max(realWidth, this.state.wrapperWidth);
            }
            else if (realBoundingBox.right > trimmedViewportBoundingBox.right) {
                resultBoundingBox.left = resultBoundingBox.right - Math.max(realWidth, this.state.wrapperWidth);
            }
            else {
                if (realWidth >= this.state.wrapperWidth) {
                    // result X coordinates are equal to real
                }
                else {
                    const leftDifference = realBoundingBox.left - trimmedViewportBoundingBox.left;
                    const rightDifference = trimmedViewportBoundingBox.right - realBoundingBox.right;

                    resultBoundingBox.left -= (this.state.wrapperWidth - realWidth) * (leftDifference / (leftDifference + rightDifference || 1));
                    resultBoundingBox.right += (this.state.wrapperWidth - realWidth) * (rightDifference / (leftDifference + rightDifference || 1));

                }
            }
        }

        if (trimmedHeight < realHeight) {
            // result Y coordinates are equal to real
        }
        else {
            if (realBoundingBox.top < trimmedViewportBoundingBox.top) {
                resultBoundingBox.bottom = resultBoundingBox.top + Math.max(realHeight, this.state.wrapperHeight);
            }
            else if (realBoundingBox.bottom > trimmedViewportBoundingBox.bottom) {
                resultBoundingBox.top = resultBoundingBox.bottom - Math.max(realHeight, this.state.wrapperHeight);
            }
            else {
                if (realHeight >= this.state.wrapperHeight) {
                    // result Y coordinates are equal to real
                }
                else {
                    const topDifference = realBoundingBox.top - trimmedViewportBoundingBox.top;
                    const bottomDifference = trimmedViewportBoundingBox.bottom - realBoundingBox.bottom;

                    resultBoundingBox.top -= (this.state.wrapperHeight - realHeight) * (topDifference / (topDifference + bottomDifference || 1));
                    resultBoundingBox.bottom += (this.state.wrapperHeight - realHeight) * (bottomDifference / (topDifference + bottomDifference || 1));
                }
            }
        }
        return this.setState({
            viewportBoundingBox: {
                left: resultBoundingBox.left - this.state.wrapperWidth / 4,
                right: resultBoundingBox.right + this.state.wrapperWidth / 4,
                top: resultBoundingBox.top - this.state.wrapperHeight / 4,
                bottom: resultBoundingBox.bottom + this.state.wrapperHeight / 4
            }
        });
    }

    tableauRedo = () => {
        const activeTableauNode = this.props.filteredTableauNodes.byId[this.props.tableau.activeNodeId];
        if (activeTableauNode.childrenIds.length > 0) {
            if (activeTableauNode.childrenIds.length === 1) {
                this.props.dispatch(TABLEAU_ACTIONS.CREATE_TRACE_TABLEAU_TO_NODE(activeTableauNode.childrenIds[0], this.props.tableau.id));
            }
            else {
                const activeTableauNodeChild = this.props.filteredTableauNodes.byId[activeTableauNode.childrenIds[0]];
                const conceptTableauRuleNode = activeTableauNodeChild as ConceptTableauRuleNode;

                if (typeof conceptTableauRuleNode.conceptId !== "undefined") {
                    const concept = this.props.expressions.byId[conceptTableauRuleNode.conceptId];

                    const conceptHelper = ConceptRouter.getHelper(concept.type);
                    if (typeof conceptHelper !== "undefined") {
                        conceptHelper.onConceptTableauExpand(conceptTableauRuleNode.cTreeNodeId, conceptTableauRuleNode.conceptId, this);
                    }
                }
            }
        }
    };

    tableauUndo = () => {
        if (this.state.currentTableauNodeParentId !== null) {
            this.props.dispatch(TABLEAU_ACTIONS.CREATE_TRACE_TABLEAU_TO_NODE(this.state.currentTableauNodeParentId, this.props.tableau.id));
        }
    };

    render () {
        const mouseEventHandler = this.state.mode ?
            (e: React.MouseEvent<Element>) => this.state.mode.handleMouseEvent(e, CTreeEventTarget.CTREE, this) :
            noop;

        const CTreeProblemDescriptionClass = DLCTreeSolvableProblemRouter.getCTreeProblemDescriptionClass(this);

        return (
            <Panel
                className={CTreeConstants.CTREE.WRAPPER.CLASS.BASE}
                header={
                    <div>
                        <CTreeProblemDescriptionClass
                            cTreeView={this}
                        />
                        <ButtonToolbar>
                            <Button
                                onClick={this.tableauUndo}
                                disabled={this.state.currentTableauNodeParentId === null}
                            >
                                <Glyphicon glyph="step-backward"/>
                            </Button>
                            <Button
                                onClick={this.viewTableau}
                            >
                                View tableau
                            </Button>
                            <Button
                                onClick={this.tableauRedo}
                                disabled={this.props.filteredTableauNodes.byId[this.props.tableau.activeNodeId].childrenIds.length === 0}
                            >
                                <Glyphicon glyph="step-forward"/>
                            </Button>
                            <div
                                className="panel-heading-divider"
                            >
                            </div>
                            <Button
                                onClick={this.cancelReasoning}
                            >
                                <Glyphicon glyph="remove"/>
                            </Button>
                        </ButtonToolbar>
                    </div>
                }
            >
                <div
                    ref="svgContainer"
                    className="svg-container"
                >
                    <div
                        className="svg-wrapper"
                    >
                        <svg
                            shapeRendering="geometricPrecision"
                            textRendering="geometricPrecision"
                            className={this.getSVGClass().join(" ")}
                            onClick={mouseEventHandler}
                            onContextMenu={mouseEventHandler}
                            onMouseMove={mouseEventHandler}
                            onMouseDown={mouseEventHandler}
                            onMouseUp={mouseEventHandler}
                            viewBox={this.state.viewportBoundingBox ? `${this.state.viewportBoundingBox.left} ${this.state.viewportBoundingBox.top} ${this.state.viewportBoundingBox.right - this.state.viewportBoundingBox.left} ${this.state.viewportBoundingBox.bottom - this.state.viewportBoundingBox.top}` : undefined}
                            width={this.state.viewportBoundingBox ? this.state.viewportBoundingBox.right - this.state.viewportBoundingBox.left : undefined}
                            height={this.state.viewportBoundingBox ? this.state.viewportBoundingBox.bottom - this.state.viewportBoundingBox.top : undefined}
                        >
                            <CTreeViewDefs/>
                            {
                                this.refs.measureTextNode ?
                                    [
                                        <g
                                            key={CTreeConstants.LAYER.EDGES.ID}
                                            id={CTreeConstants.LAYER.EDGES.ID}
                                        >
                                            {
                                                mapNormalizedStateIdCategory(this.cTreeEdges, edge => {
                                                    return <CTreeEdgeView
                                                        key={edge.id}
                                                        cTreeEdge={edge}
                                                        startNode={this.cTreeNodes.byId[edge.startNodeId]}
                                                        endNode={this.cTreeNodes.byId[edge.endNodeId]}
                                                        expressions={this.props.expressions}
                                                        vocabularySymbols={this.vocabularySymbols}
                                                    />;
                                                })
                                            }
                                            {
                                                mapNormalizedStateIdCategory(this.state.newEdges, onEdgeMount => {
                                                    return (<CTreeNewEdgeView
                                                        key={onEdgeMount.id}
                                                        id={onEdgeMount.id}
                                                        onMount={onEdgeMount}
                                                        cTreeNodes={this.cTreeNodes}
                                                    />);
                                                })
                                            }
                                        </g>,
                                        <g
                                            key={CTreeConstants.LAYER.NODES.ID}
                                            id={CTreeConstants.LAYER.NODES.ID}
                                        >
                                            {
                                                mapNormalizedStateIdCategory(this.cTreeNodes, node => {
                                                    return <CTreeNodeView
                                                        key={node.id}
                                                        expressions={this.props.expressions}
                                                        cTreeNodes={this.cTreeNodes}
                                                        cTreeEdges={this.cTreeEdges}
                                                        tBoxAxiomIds={this.props.knowledgeBase.tBoxAxiomIds}
                                                        vocabularySymbols={this.vocabularySymbols}
                                                        cTreeNodeId={node.id}
                                                        onMouseEvent={this.state.mode.handleMouseEvent}
                                                        fillAndShowModal={this.props.fillAndShowModal}
                                                        fillAndShowContextMenu={this.props.fillAndShowContextMenu}
                                                        measureSVGText={this.measureSVGText}
                                                        cTreeId={this.id}
                                                        dispatch={this.props.dispatch}
                                                        cTreeContradictions={this.state.cTreeContradictions}
                                                        cTreeNodeBlockedBy={this.state.cTreeBlockedNodes[node.id]}
                                                        tableau={this.props.tableau}
                                                        tableauNodes={this.tableauNodes}
                                                    />;
                                                })
                                            }
                                            {
                                                mapNormalizedStateIdCategory(this.state.newNodes, onNodeMount => {
                                                    return (<CTreeNewNodeView
                                                        key={onNodeMount.id}
                                                        id={onNodeMount.id}
                                                        onMount={onNodeMount}
                                                    />);
                                                })
                                            }
                                        </g>
                                    ] :
                                    null
                            }
                            <text
                                ref="measureTextNode"
                                visibility="hidden"
                                pointerEvents="none"
                            />
                        </svg>
                    </div>
                </div>
                {
                    this.state.popoverData ?
                        <Popover
                            {...this.state.popoverData}
                            id={`ctree-${this.id}-popover`}
                            style={{
                                transform: "translate(-50%,-100%)",
                                pointerEvents: "none"
                            }}/> :
                        null
                }
                <CTreeStatusBar
                    statusType={this.props.cTree.statusType}
                    message={this.props.cTree.statusMessage}
                />
            </Panel>
        );
    }

    toggleEdgeCreation (onMountOrId: ((cTreeNewEdgeView: CTreeNewEdgeView) => void) | number) {
        if (typeof onMountOrId === "number") {
            this.setState(prevState => {
                return {
                    newEdges: removeNormalizedFromState(prevState.newEdges, [], onMountOrId)
                };
            });
        }
        else {
            this.setState(prevState => {
                const onMountWithId = onMountOrId as FunctionWithId<(cTreeNewEdgeView: CTreeNewEdgeView) => void>;
                onMountWithId.id = this.newEdgeId++;

                return {
                    newEdges: appendNormalizedToState(prevState.newEdges, [], onMountOrId as FunctionWithId<(cTreeNewEdgeView: CTreeNewEdgeView) => void>)
                };
            });
        }
    }

    toggleNodeCreation (onMountOrId: ((cTreeNewNodeView: CTreeNewNodeView) => void) | number) {
        if (typeof onMountOrId === "number") {
            this.setState(prevState => {
                return {
                    newNodes: removeNormalizedFromState(prevState.newNodes, [], onMountOrId)
                };
            });
        }
        else {
            this.setState(prevState => {
                const onMountWithId = onMountOrId as FunctionWithId<(cTreeNewNodeView: CTreeNewNodeView) => void>;
                onMountWithId.id = this.newNodeId++;

                return {
                    newNodes: appendNormalizedToState(prevState.newNodes, [], onMountWithId)
                };
            });
        }
    }

    setPopoverData (popoverData: PopoverData | undefined) {
        this.setState({
            popoverData: popoverData
        });
    }
}