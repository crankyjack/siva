import * as React from "react";
import {CTreeConstants} from "../utils/c-tree-constants";
import {CTreeStatusBarStatusType} from "../c-tree";
import {Glyphicon} from "react-bootstrap";

export interface CTreeStatusBarProps {
    statusType: CTreeStatusBarStatusType,
    message: string
}

const CTreeStatusBarGlyphicon = {
    "neutral": "info-sign",
    "positive": "ok-sign",
    "negative": "remove-sign",
    "none": ""
};

export default class CTreeStatusBar extends React.PureComponent<CTreeStatusBarProps> {
    render () {
        return (
            <div
                className={`${CTreeConstants.STATUS_BAR.CLASS.BASE} ${this.props.statusType}`}
            >
                <Glyphicon
                    glyph={CTreeStatusBarGlyphicon[this.props.statusType]}
                />
                <span>
                    {this.props.message}
                </span>
            </div>
        );
    }
}