import * as React from "react";
import {CSSProperties, MouseEvent} from "react";
import "../../../../Problem/utils/dl-problem-import-list";
import {createNewEmptyDLCTreeTableauWorkspace, default as DLCTreeTableauWorkspace, DL_CTREE_TABLEAU_WORKSPACE_TYPE, DLCTreeTableauWorkspaceState} from "../dl-ctree-tableau-workspace";
import DLCTreeTableauWorkspaceReducer from "../flux/dl-ctree-tableau-workspace-reducer";
import autobind from "autobind-decorator";
import {WorkspaceProps, WorkspaceView} from "../../../../../Workspace/workspace";
import KnowledgeBase from "../../../../Denotation/KnowledgeBase/knowledge-base";
import DL_CTREE_TABLEAU_WORKSPACE_ACTIONS from "../flux/dl-ctree-tableau-workspace-actions";
import CTreeNode from "../../Node/c-tree-node";
import CTreeEdge from "../../Edge/c-tree-edge";
import CTreeView from "../../components/c-tree-view";
import DLProblemChoiceModal from "../../../../Problem/components/dl-problem-choice-modal";
import {filterNormalizedStateIdCategory, getHTMLElementPageCoords, NormalizedStateIdCategory} from "../../../../../Common/utils/util";
import {Tableau} from "../../../../../Tableau/tableau";
import TableauNode from "../../../../../Tableau/tableau-node";
import Problem from "../../../../../Problem/problem";
import TABLEAU_HELPER_FUNCTIONS from "../../../../../Tableau/utils/tableau-helper-functions";
import VKBPanelView from "../../../../Denotation/component/vkb-panel-view";
import PanelSeparator from "../../../../../Common/components/panel-separator";
import ConfirmationModal from "../../../../../Common/components/confirmation-modal";

export interface DLCTreeTableauWorkspaceViewState {
    panelSeparatorDragged: boolean,
    knowledgeBaseStyle: CSSProperties
}

export class DLCTreeTableauWorkspaceView extends WorkspaceView<DLCTreeTableauWorkspaceViewState> {
    static readonly className = DL_CTREE_TABLEAU_WORKSPACE_TYPE;
    static readonly workspaceLabel = "Description logic completion tree tableau reasoning";
    static readonly factory = createNewEmptyDLCTreeTableauWorkspace;
    static readonly reducer = DLCTreeTableauWorkspaceReducer;

    refs: {
        workspace: HTMLDivElement
    };

    constructor (props: WorkspaceProps) {
        super(props);

        this.state = {
            panelSeparatorDragged: false,
            knowledgeBaseStyle: {}
        };
    }

    get applicationState () {
        return this.props.applicationState as DLCTreeTableauWorkspaceState;
    }

    get workspace () {
        return this.applicationState.Workspaces.byId[this.props.id] as DLCTreeTableauWorkspace;
    }

    get cTree () {
        return this.applicationState.DLCTrees.byId[this.workspace.cTreeId];
    }

    @autobind
    handleKnowledgeBaseReady (knowledgeBase: KnowledgeBase) {
        this.props.fillAndShowModal(DLProblemChoiceModal, {
            knowledgeBase,
            expressions: this.applicationState.DLExpressions,
            vocabularySymbols: this.applicationState.DLVocabularySymbols,
            vocabulary: this.applicationState.DLVocabularies.byId[knowledgeBase.vocabularyId],
            workspaceId: this.workspace.id
        });
    }

    @autobind
    handlePanelSeparatorDrag (e: MouseEvent<HTMLDivElement>) {
        e.preventDefault();
        const knowledgeBaseBottom = e.pageY - 7.5 - getHTMLElementPageCoords(e.currentTarget as HTMLDivElement).top;
        const totalHeight = this.refs.workspace.offsetHeight - 15;
        const realRatio = knowledgeBaseBottom / totalHeight;

        this.setState({
            knowledgeBaseStyle: {
                height: 0,
                flexGrow: realRatio / (1 - realRatio)
            }
        });
    }

    @autobind
    handlePanelSeparatorDragStart (e: MouseEvent<HTMLDivElement>) {
        this.setState({
            panelSeparatorDragged: true
        });
    }

    @autobind
    handleMouseUp (e: MouseEvent<HTMLDivElement>) {
        this.setState({
            panelSeparatorDragged: false
        });
    }


    getClassName () {
        const result = ["workspace", DL_CTREE_TABLEAU_WORKSPACE_TYPE];

        if (this.cTree) {
            result.push("workspace--problem-initialized");
        }

        return result.join(" ");
    }

    @autobind
    handleCancelReasoning () {
        this.props.fillAndShowModal(ConfirmationModal, {
            title: "Confirmation of reasoning abortion",
            message: "Are you sure you want to end the reasoning simulation? You will lose all information regarding the completion tree and the tableau.",
            onConfirm: () => {
                this.props.dispatch(DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.CREATE_CANCEL_REASONING(this.workspace.id));
            }
        });
    }

    render () {
        const knowledgeBase = this.applicationState.DLKnowledgeBases.byId[this.workspace.knowledgeBaseId];
        const vocabulary = this.applicationState.DLVocabularies.byId[knowledgeBase.vocabularyId];
        const vocabularySymbols = filterNormalizedStateIdCategory(this.applicationState.DLVocabularySymbols, [...vocabulary.atomicConcepts, ...vocabulary.atomicRoles, ...vocabulary.individuals]);


        const cTree = this.cTree;
        let cTreeNodes: NormalizedStateIdCategory<CTreeNode>,
            cTreeEdges: NormalizedStateIdCategory<CTreeEdge>,
            tableau: Tableau,
            tableauNodes: NormalizedStateIdCategory<TableauNode>,
            problem: Problem;

        if (cTree) {
            cTreeNodes = filterNormalizedStateIdCategory(this.applicationState.DLCTreeNodes, cTree.nodes);
            cTreeEdges = filterNormalizedStateIdCategory(this.applicationState.DLCTreeEdges, cTree.edges);
            tableau = this.applicationState.Tableaus.byId[cTree.tableauId];
            tableauNodes = TABLEAU_HELPER_FUNCTIONS.getTableauNodes(tableau.rootNodeId, this.applicationState.TableauNodes);
            problem = this.applicationState.Problems.byId[cTree.problemId];
        }

        return (
            <div
                ref="workspace"
                className={this.getClassName()}
                onMouseMove={this.state.panelSeparatorDragged ? this.handlePanelSeparatorDrag : undefined}
                onMouseUp={this.state.panelSeparatorDragged ? this.handleMouseUp : undefined}
            >
                <VKBPanelView
                    knowledgeBase={knowledgeBase}
                    editable={!problem!}
                    expressions={this.applicationState.DLExpressions}
                    vocabulary={vocabulary}
                    vocabularySymbols={vocabularySymbols}
                    fillAndShowModal={this.props.fillAndShowModal}
                    onReady={cTree ? undefined : this.handleKnowledgeBaseReady}
                    dispatch={this.props.dispatch}
                    style={cTree ? this.state.knowledgeBaseStyle : {}}
                />
                {

                    !cTree ||
                    <PanelSeparator
                        height={15}
                        onDragStart={this.handlePanelSeparatorDragStart}
                    />
                }
                {
                    !cTree ||
                    <CTreeView
                        cTree={cTree}
                        problem={problem!}
                        filteredCTreeNodes={cTreeNodes!}
                        filteredCTreeEdges={cTreeEdges!}
                        knowledgeBase={knowledgeBase}
                        expressions={this.applicationState.DLExpressions}
                        vocabulary={vocabulary}
                        filteredVocabularySymbols={vocabularySymbols}
                        tableau={tableau!}
                        onCancel={this.handleCancelReasoning}
                        filteredTableauNodes={tableauNodes!}
                        fillAndShowModal={this.props.fillAndShowModal}
                        updateModal={this.props.updateModal}
                        fillAndShowContextMenu={this.props.fillAndShowContextMenu}
                        dispatch={this.props.dispatch}
                    />
                }
            </div>
        );
    }
}