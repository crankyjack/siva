import {Reducer} from "redux";
import CTREE_ACTION from "./c-tree-actions";
import {CTreeState} from "../c-tree";
import CTREE_HELPER_FUNCTIONS from "../utils/c-tree-helper-functions";
import {AnyAction, default as ACTIONS} from "../../../../App/flux/actions";
import {initNormalizedCategoryInState, setInState} from "../../../../Common/utils/util";
import ConceptRouter from "../../../Denotation/KnowledgeBase/Expression/utils/concept-router";

const CTreeReducer: Reducer<CTreeState> = (state: CTreeState, payload: AnyAction) => {
    payloadSwitch:
        switch (payload.type) {
            case ACTIONS.CREATE_WORKSPACE: {
                const action = payload as ACTIONS.PAYLOAD_CREATE_WORKSPACE;

                state = initNormalizedCategoryInState(state, "DLCTrees");
                state = initNormalizedCategoryInState(state, "DLCTreeNodes");
                state = initNormalizedCategoryInState(state, "DLCTreeEdges");

                break payloadSwitch;
            }
            case CTREE_ACTION.CREATE_NODE: {
                const action = payload as CTREE_ACTION.PAYLOAD_CREATE_NODE;

                let nnfConceptIds;
                if (action.nodeLabelConceptIds) {
                    nnfConceptIds = action.nodeLabelConceptIds.reduce((accumulator, id) => {
                        const update = ConceptRouter.getNNFExpressionUpdate(id, state.DLExpressions);
                        state = setInState(state, ["DLExpressions"], update.expressions, true);
                        accumulator.push(update.id);
                        return accumulator;
                    }, [] as number[]);
                }
                [state] = CTREE_HELPER_FUNCTIONS.addNode(state, action.cTreeId, action.nodeOrigin, action.x, action.y, action.nameOrIndividualSymbolId, nnfConceptIds);
                break payloadSwitch;
            }
            case CTREE_ACTION.RENAME_NODE: {
                const action = payload as CTREE_ACTION.PAYLOAD_RENAME_NODE;

                state = CTREE_HELPER_FUNCTIONS.nodeSet(state, action.nodeId, "nameOrIndividualSymbolId", action.name);
                break payloadSwitch;
            }
            case CTREE_ACTION.MOVE_NODE: {
                const action = payload as CTREE_ACTION.PAYLOAD_MOVE_NODE;

                state = CTREE_HELPER_FUNCTIONS.nodeMerge(state, action.nodeId, {
                    x: action.x,
                    y: action.y
                });

                break payloadSwitch;
            }
            case CTREE_ACTION.CREATE_EDGE: {
                const action = payload as CTREE_ACTION.PAYLOAD_CREATE_EDGE;

                [state] = CTREE_HELPER_FUNCTIONS.connectNodes(state, action.cTreeId, action.startNodeId, action.endNodeId);
                break payloadSwitch;
            }
            case CTREE_ACTION.SET_STATUS: {
                const action = payload as CTREE_ACTION.PAYLOAD_SET_STATUS;

                state = CTREE_HELPER_FUNCTIONS.setCTreeStatus(state, action.cTreeId, action.statusMessage, action.statusType);
                break payloadSwitch;
            }
        }
    return state;
};

export default CTreeReducer;