import {DLProblemHelper} from "./dl-problem-router";
import {Class} from "../../../Common/utils/util";
import * as React from "react";
import {CTreeState} from "../../CTreeTableauReasoning/CompletionTree/c-tree";
import CTreeView from "../../CTreeTableauReasoning/CompletionTree/components/c-tree-view";
import CTreeViewMode from "../../CTreeTableauReasoning/CompletionTree/Mode/components/c-tree-view-mode";

export default class DLCTreeSolvableProblemRouter {
    private static helpers: Map<string, DLCTreeProblemHelper> = new Map();

    static registerHelper (helper: DLCTreeProblemHelper) {
        this.helpers.set(helper.PROBLEM_TYPE, helper);
    }

    static getHelper (dlProblemType: string) {
        return this.helpers.get(dlProblemType);
    }

    static isCTreeProblemInitialized (cTreeId: number, state: CTreeState) {
        const cTree = state.DLCTrees.byId[cTreeId];
        const problem = state.Problems.byId[cTree.problemId];
        const helper = this.getHelper(problem.type)!;

        return helper.isCTreeProblemInitialized(cTreeId, state);
    }

    static getCTreeProblemInitializationMode (cTreeView: CTreeView): CTreeViewMode | undefined {
        const helper = this.getHelper(cTreeView.props.problem.type)!;

        return helper.getCTreeProblemInitializationMode(cTreeView);
    }

    static getCTreeProblemDescriptionClass (cTreeView: CTreeView) {
        const helper = this.getHelper(cTreeView.props.problem.type)!;

        return helper.CTREE_PROBLEM_DESCRIPTION;
    }
}

export interface DLCTreeProblemHelper extends DLProblemHelper {
    CTREE_PROBLEM_DESCRIPTION: Class<DLCTreeSolvableProblemLatexDescription>;

    isCTreeProblemInitialized (cTreeId: number, state: CTreeState): boolean;

    getCTreeProblemInitializationMode (cTreeView: CTreeView): CTreeViewMode | undefined;
}

export abstract class DLCTreeSolvableProblemLatexDescription extends React.PureComponent<{
    cTreeView: CTreeView
}> {}