import {DLProblemChoiceModalProps} from "../components/dl-problem-choice-modal";
import Problem from "../../../Problem/problem";
import {setInState} from "../../../Common/utils/util";
import {default as ProblemRouter, ProblemHelper} from "../../../Problem/utils/problem-router";
import {DLDenotationState} from "../../Denotation/denotation";
import DLCTreeTableauWorkspace, {DL_CTREE_TABLEAU_WORKSPACE_TYPE, DLCTreeTableauWorkspaceState} from "../../CTreeTableauReasoning/CompletionTree/DLCTreeTableauWorkspace/dl-ctree-tableau-workspace";
import {createNewEmptyCTree} from "../../CTreeTableauReasoning/CompletionTree/c-tree";

export default class DLProblemRouter {
    private static helpers: Map<string, DLProblemHelper> = new Map();

    static registerHelper (helper: DLProblemHelper) {
        this.helpers.set(helper.PROBLEM_TYPE, helper);
    }

    static getHelper (dlProblemType: string) {
        return this.helpers.get(dlProblemType);
    }

    static getAllHelpers () {
        return Array.from(this.helpers.values());
    }

    static handleDLProblemDefinitionRequest (dlProblemType: string, dlProblemChoiceModalProps: DLProblemChoiceModalProps) {
        const helper = this.getHelper(dlProblemType)!;

        return helper.handleDLProblemDefinitionRequest(dlProblemChoiceModalProps);
    }

    static handleDLCTreeTableauWorkspaceAssign<S extends DLDenotationState> (problem: Problem, workspaceId: number, state: S): S {
        const workspace = state.Workspaces.byId[workspaceId] as DLCTreeTableauWorkspace;
        let dlCTreeTableauWorkspaceState = state as any as DLCTreeTableauWorkspaceState;
        let cTree;
        [dlCTreeTableauWorkspaceState, cTree] = createNewEmptyCTree(dlCTreeTableauWorkspaceState, workspace.knowledgeBaseId, problem.id);
        dlCTreeTableauWorkspaceState = setInState(dlCTreeTableauWorkspaceState, ["Workspaces", "byId", workspaceId, "cTreeId"], cTree.id, true);

        return dlCTreeTableauWorkspaceState as any as S;
    }
}

export interface DLProblemHelper extends ProblemHelper {
    handleDLProblemDefinitionRequest (dlProblemChoiceModalProps: DLProblemChoiceModalProps): void;
}

export interface DLKnowledgeBaseProblem extends Problem {
    knowledgeBaseId: number
}

ProblemRouter.registerWorkspaceProblemAssignHandler(DL_CTREE_TABLEAU_WORKSPACE_TYPE, DLProblemRouter.handleDLCTreeTableauWorkspaceAssign);