import * as React from "react";
import {SyntheticEvent} from "react";
import autobind from "autobind-decorator";
import {Button, FormGroup, Modal} from "react-bootstrap";
import Latex from "react-latex";
import {VocabularySymbol} from "../../Denotation/Vocabulary/vocabulary-symbol";
import Vocabulary from "../../Denotation/Vocabulary/vocabulary";
import DL_DENOTATION_HELPER_FUNCTIONS from "../../Denotation/utils/denotation-helper-functions";
import {ModalComponentProps, ModalComponentState} from "../../../Common/modal-component";
import {NormalizedStateIdCategory} from "../../../Common/utils/util";
import Expression, {ExpressionCategory} from "../../Denotation/KnowledgeBase/Expression/expression";
import ExpressionEditControl from "../../Denotation/KnowledgeBase/Expression/components/expression-edit-control";
import ExpressionRouter from "../../Denotation/KnowledgeBase/Expression/utils/expression-router";
import ErrorExpression from "../../Denotation/KnowledgeBase/Expression/error-expression";
import SymbolKeyboard from "../../Denotation/KnowledgeBase/Expression/components/symbol-keyboard";

export interface ConceptAssertionInputModalProps extends ModalComponentProps {
    title: string,
    onSubmit: (individualString: string, conceptString: string) => void,
    expressions: NormalizedStateIdCategory<Expression>,
    vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>
    vocabulary: Vocabulary
}

export interface ConceptAssertionInputModalState extends ModalComponentState {
    individualLatexValue: string
    conceptLatexValue: string
    individualErrorMessages: string[]
    conceptErrorMessages: string[]
}

export default class ConceptAssertionInputModal extends React.PureComponent<ConceptAssertionInputModalProps, ConceptAssertionInputModalState> {
    refs: {
        individualEditControl: ExpressionEditControl,
        conceptEditControl: ExpressionEditControl,
        latex: Latex
    };

    activeEditControl: ExpressionEditControl;

    constructor (props: ConceptAssertionInputModalProps) {
        super(props);

        this.state = {
            open: true,
            individualLatexValue: "",
            conceptLatexValue: "",
            individualErrorMessages: [],
            conceptErrorMessages: []
        };
    }

    @autobind
    updateIndividualLatexValue (expressionStringValue: string) {
        const update = ExpressionRouter.parseString(expressionStringValue, this.props.expressions, this.props.vocabularySymbols, this.props.vocabulary, ExpressionCategory.INDIVIDUAL);

        const individualLatexValue = ExpressionRouter.getLatexString(update.id, update.expressions, this.props.vocabularySymbols);

        const individualErrorMessages = DL_DENOTATION_HELPER_FUNCTIONS.checkExpressionForErrors(update.id, update.expressions).map(errorId => (update.expressions.byId[errorId] as ErrorExpression).errorMessage);

        this.setState({
            individualLatexValue,
            individualErrorMessages
        });
    }

    @autobind
    updateConceptLatexValue (expressionStringValue: string) {
        const update = ExpressionRouter.parseString(expressionStringValue, this.props.expressions, this.props.vocabularySymbols, this.props.vocabulary, ExpressionCategory.CONCEPT);

        const conceptLatexValue = ExpressionRouter.getLatexString(update.id, update.expressions, this.props.vocabularySymbols);

        const conceptErrorMessages = DL_DENOTATION_HELPER_FUNCTIONS.checkExpressionForErrors(update.id, update.expressions).map(errorId => (update.expressions.byId[errorId] as ErrorExpression).errorMessage);

        this.setState({
            conceptLatexValue,
            conceptErrorMessages
        });
    }

    @autobind
    handleOk (event: SyntheticEvent<any>) {
        event.preventDefault();

        this.props.onSubmit(this.refs.individualEditControl.input.value, this.refs.conceptEditControl.input.value);
        this.hide();
    }

    @autobind
    hide () {
        this.setState({
            open: false
        });
    }

    @autobind
    focusSelectInput () {
        this.refs.individualEditControl.input.focus();
        this.refs.individualEditControl.input.select();
    }

    @autobind
    removeModal () {
        this.props.removeModal(this.props.id);
    }

    @autobind
    handleIndividualUpdate () {
        this.updateIndividualLatexValue(this.refs.individualEditControl.input.value);
    }

    @autobind
    handleConceptUpdate () {
        this.updateConceptLatexValue(this.refs.conceptEditControl.input.value);
    }

    @autobind
    handleKeyboardSymbolClick (symbol: string) {
        if (this.activeEditControl) {
            this.activeEditControl.insert(symbol);

            if (this.activeEditControl === this.refs.conceptEditControl) {
                this.handleConceptUpdate();
            }
            else {
                this.handleIndividualUpdate();
            }
        }
    }

    @autobind
    handleExpressionEditControlFocus (expressionEditControl: ExpressionEditControl) {
        this.activeEditControl = expressionEditControl;
    }

    componentDidMount () {
        this.updateIndividualLatexValue("");
        this.updateConceptLatexValue("");
    }

    render () {
        const hasErrors = this.state.individualErrorMessages.length > 0 || this.state.conceptErrorMessages.length > 0;

        return (
            <Modal
                show={this.state.open}
                onEntered={this.focusSelectInput}
                onHide={this.hide}
                onExited={this.removeModal}
                backdrop
            >
                <form onSubmit={this.handleOk}>
                    <Modal.Header closeButton>
                        <Modal.Title>{this.props.title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <SymbolKeyboard
                            onSymbolClick={this.handleKeyboardSymbolClick}
                        />
                        <FormGroup
                            validationState={this.state.individualErrorMessages.length > 0 ? "error" : "success"}
                        >
                            <ExpressionEditControl
                                ref="individualEditControl"
                                label="Input individual:"
                                expectedExpressionCategory={ExpressionCategory.INDIVIDUAL}
                                onUpdate={this.handleIndividualUpdate}
                                onFocus={this.handleExpressionEditControlFocus}
                            />
                        </FormGroup>
                        <FormGroup
                            validationState={this.state.conceptErrorMessages.length > 0 ? "error" : "success"}
                        >
                            <ExpressionEditControl
                                ref="conceptEditControl"
                                label="Input concept:"
                                expectedExpressionCategory={ExpressionCategory.CONCEPT}
                                onUpdate={this.handleConceptUpdate}
                                onFocus={this.handleExpressionEditControlFocus}
                            />
                        </FormGroup>
                        {
                            <div
                                className="katex-wrap"
                                title={[...this.state.individualErrorMessages, ...this.state.conceptErrorMessages].join("\n")}
                            >
                                <Latex
                                    ref={"latex"}
                                    children={`$${this.state.individualLatexValue} : ${this.state.conceptLatexValue}$`}
                                />
                            </div>
                        }
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            onClick={this.handleOk}
                            type="submit"
                            bsStyle={hasErrors ? "danger" : "success"}
                            title={hasErrors ? `The concept expression contains the following errors:\n${[...this.state.individualErrorMessages, ...this.state.conceptErrorMessages].join("\n")}` : ""}
                            disabled={hasErrors}
                        >
                            Confirm
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>
        );
    }
}