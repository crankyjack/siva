import * as React from "react";
import {SyntheticEvent} from "react";
import autobind from "autobind-decorator";
import {Button, FormGroup, Modal} from "react-bootstrap";
import Latex from "react-latex";
import {VocabularySymbol} from "../../Denotation/Vocabulary/vocabulary-symbol";
import Vocabulary from "../../Denotation/Vocabulary/vocabulary";
import DL_DENOTATION_HELPER_FUNCTIONS from "../../Denotation/utils/denotation-helper-functions";
import {ModalComponentProps, ModalComponentState} from "../../../Common/modal-component";
import {NormalizedStateIdCategory} from "../../../Common/utils/util";
import Expression, {ExpressionCategory} from "../../Denotation/KnowledgeBase/Expression/expression";
import ExpressionEditControl from "../../Denotation/KnowledgeBase/Expression/components/expression-edit-control";
import ExpressionRouter from "../../Denotation/KnowledgeBase/Expression/utils/expression-router";
import ErrorExpression from "../../Denotation/KnowledgeBase/Expression/error-expression";
import SymbolKeyboard from "../../Denotation/KnowledgeBase/Expression/components/symbol-keyboard";

export interface FormValidationInformation {
    hint: string;
    validationState: "success" | "warning" | "error" | undefined;
    buttonBsStyle: string | null;
    buttonDisabled: boolean;
}

export interface ConceptInputModalProps extends ModalComponentProps {
    title: string,
    initialValue?: string,
    onSubmit: (expressionString: string) => void,
    expressions: NormalizedStateIdCategory<Expression>,
    vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>
    vocabulary: Vocabulary
}

export interface ConceptInputModalState extends ModalComponentState {
    expressionLatexValue: string
    errorMessages: string[]
}

export default class ConceptInputModal extends React.PureComponent<ConceptInputModalProps, ConceptInputModalState> {
    refs: {
        expressionEditControl: ExpressionEditControl,
        latex: Latex
    };

    constructor (props: ConceptInputModalProps) {
        super(props);

        this.state = {
            open: true,
            expressionLatexValue: "",
            errorMessages: []
        };
    }

    @autobind
    updateExpressionLatexValue (expressionStringValue: string) {
        const update = ExpressionRouter.parseString(expressionStringValue, this.props.expressions, this.props.vocabularySymbols, this.props.vocabulary, ExpressionCategory.CONCEPT);

        const expressionLatexValue = ExpressionRouter.getLatexString(update.id, update.expressions, this.props.vocabularySymbols);

        const expressionErrors = DL_DENOTATION_HELPER_FUNCTIONS.checkExpressionForErrors(update.id, update.expressions).map(errorId => (update.expressions.byId[errorId] as ErrorExpression).errorMessage);

        this.setState({
            expressionLatexValue,
            errorMessages: expressionErrors
        });
    }

    @autobind
    handleOk (event: SyntheticEvent<any>) {
        event.preventDefault();

        this.props.onSubmit(this.refs.expressionEditControl.input.value);
        this.hide();
    }

    @autobind
    hide () {
        this.setState({
            open: false
        });
    }

    @autobind
    focusSelectInput () {
        this.refs.expressionEditControl.input.focus();
        this.refs.expressionEditControl.input.select();
    }

    @autobind
    removeModal () {
        this.props.removeModal(this.props.id);
    }

    @autobind
    handleExpressionUpdate (newValue: string) {
        this.updateExpressionLatexValue(newValue);
    }

    @autobind
    handleKeyboardSymbolClick (symbol: string) {
        this.refs.expressionEditControl.insert(symbol);
        this.handleExpressionUpdate(this.refs.expressionEditControl.input.value);
    }

    componentDidMount (): void {
        this.updateExpressionLatexValue(this.props.initialValue || "");
    }

    render () {
        const hasErrors = this.state.errorMessages.length > 0;

        return (
            <Modal
                show={this.state.open}
                onEntered={this.focusSelectInput}
                onHide={this.hide}
                onExited={this.removeModal}
                backdrop
            >
                <form onSubmit={this.handleOk}>
                    <Modal.Header closeButton>
                        <Modal.Title>{this.props.title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <SymbolKeyboard
                            onSymbolClick={this.handleKeyboardSymbolClick}
                        />
                        <FormGroup
                            validationState={hasErrors ? "error" : "success"}
                        >
                            <ExpressionEditControl
                                ref="expressionEditControl"
                                label="Input concept:"
                                expectedExpressionCategory={ExpressionCategory.CONCEPT}
                                onUpdate={this.handleExpressionUpdate}
                            />
                        </FormGroup>
                        {
                            <div
                                className="katex-wrap"
                                title={hasErrors ? this.state.errorMessages.join("\n") : ""}
                            >
                                <Latex
                                    ref={"latex"}
                                    children={`$${this.state.expressionLatexValue}$`}
                                />
                            </div>
                        }
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            onClick={this.handleOk}
                            type="submit"
                            bsStyle={hasErrors ? "danger" : "success"}
                            title={hasErrors ? `The concept expression contains the following errors:\n${this.state.errorMessages.join("\n")}` : ""}
                            disabled={hasErrors}
                        >
                            Confirm
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>
        );
    }
}