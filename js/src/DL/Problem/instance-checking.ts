import {DLProblemChoiceModalProps} from "./components/dl-problem-choice-modal";
import ProblemRouter from "../../Problem/utils/problem-router";
import DLProblemRouter from "./utils/dl-problem-router";
import DLCTreeSolvableProblemRouter from "./utils/dl-ctree-solvable-problem-router";
import ConceptAssertionInputModal from "./components/concept-assertion-input-modal";
import {filterNormalizedStateIdCategory, findInNormalizedStateIdCategory} from "../../Common/utils/util";
import Problem from "../../Problem/problem";
import InstanceCheckingLatexDescription from "./components/instance-checking-latex-description";
import ConceptRouter from "../Denotation/KnowledgeBase/Expression/utils/concept-router";
import {CTreeState} from "../CTreeTableauReasoning/CompletionTree/c-tree";
import {CTREE_NODE_ORIGIN} from "../CTreeTableauReasoning/CompletionTree/Node/c-tree-node";
import CTreeView from "../CTreeTableauReasoning/CompletionTree/components/c-tree-view";
import CTreeViewMode from "../CTreeTableauReasoning/CompletionTree/Mode/components/c-tree-view-mode";
import ModeSingleNodeInitialization from "../CTreeTableauReasoning/CompletionTree/Mode/components/mode-single-node-initialization";
import DL_CTREE_TABLEAU_WORKSPACE_ACTIONS from "../CTreeTableauReasoning/CompletionTree/DLCTreeTableauWorkspace/flux/dl-ctree-tableau-workspace-actions";
import CTREE_ACTIONS from "../CTreeTableauReasoning/CompletionTree/flux/c-tree-actions";
import KNOWLEDGE_BASE_ACTIONS from "../Denotation/KnowledgeBase/flux/knowledge-base-actions";

export default interface InstanceChecking extends Problem {
    knowledgeBaseId: number,
    individualSymbolId: number,
    conceptId: number
}

export class InstanceCheckingHelper {
    static readonly PROBLEM_TYPE = "INSTANCE_CHECKING";
    static readonly PROBLEM_NAME = "Instance checking";
    static readonly PROBLEM_DESCRIPTION = "Description";
    static readonly CTREE_PROBLEM_DESCRIPTION = InstanceCheckingLatexDescription;

    static create (individualSymbolId: number, conceptId: number, knowledgeBaseId: number): InstanceChecking {
        return {
            id: NaN,
            type: this.PROBLEM_TYPE,
            individualSymbolId,
            knowledgeBaseId,
            conceptId
        };
    }

    static handleDLProblemDefinitionRequest (dlProblemChoiceModalProps: DLProblemChoiceModalProps): void {
        dlProblemChoiceModalProps.fillAndShowModal(ConceptAssertionInputModal, {
            title: this.PROBLEM_NAME,
            onSubmit: (individualString: string, conceptString: string) => {
                dlProblemChoiceModalProps.dispatch(KNOWLEDGE_BASE_ACTIONS.CREATE_DEFINE_INSTANCE_CHECKING(individualString, conceptString, dlProblemChoiceModalProps.knowledgeBase.id, dlProblemChoiceModalProps.workspaceId));
            },
            expressions: dlProblemChoiceModalProps.expressions,
            vocabularySymbols: dlProblemChoiceModalProps.vocabularySymbols,
            vocabulary: dlProblemChoiceModalProps.vocabulary
        });
    }

    static isCTreeProblemInitialized (cTreeId: number, state: CTreeState) {
        const cTree = state.DLCTrees.byId[cTreeId];
        const filteredCTreeNodes = filterNormalizedStateIdCategory(state.DLCTreeNodes, cTree.nodes);
        const problem = state.Problems.byId[cTree.problemId];
        const instanceChecking = (problem as InstanceChecking);
        const nnfUpdate = ConceptRouter.getNegatedNNFExpressionUpdate(instanceChecking.conceptId, state.DLExpressions);

        const initialNode = findInNormalizedStateIdCategory(filteredCTreeNodes, node => {
            return node.origin === CTREE_NODE_ORIGIN.PROBLEM_INPUT && node.labelConcepts.indexOf(nnfUpdate.id) === 0 && node.nameOrIndividualSymbolId === instanceChecking.individualSymbolId;
        });

        return initialNode !== null;
    }

    static getCTreeProblemInitializationMode (cTreeView: CTreeView): CTreeViewMode | undefined {
        const instanceChecking = (cTreeView.props.problem as InstanceChecking);
        const nodeName = cTreeView.vocabularySymbols.byId[instanceChecking.individualSymbolId].symbol;

        return new ModeSingleNodeInitialization(cTreeView, {
            nodeName: nodeName,
            onNodeCreate: (x, y) => {
                cTreeView.props.dispatch(DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.CREATE_INITIALIZE_INSTANCE_CHECKING(x, y, cTreeView.props.problem.id, cTreeView.id));
            },
            onStartCallback: () => {
                cTreeView.props.dispatch(CTREE_ACTIONS.CREATE_SET_STATUS(cTreeView.id, `[Problem initialization] Click to place a new completion tree node, that will represent individual ${nodeName}.`, "neutral"));
            }
        });
    }
}

ProblemRouter.registerHelper(InstanceCheckingHelper);
DLProblemRouter.registerHelper(InstanceCheckingHelper);
DLCTreeSolvableProblemRouter.registerHelper(InstanceCheckingHelper);