import * as React from "react";
import TableauNode from "../tableau-node";
import {Tableau} from "../tableau";
import {TableauConstants} from "../utils/tableau-constants";
import TABLEAU_ACTIONS from "../flux/tableau-actions";
import autobind from "autobind-decorator";
import {Dispatch} from "redux";
import MainState from "../../App/flux/main-state";

type TableauNodeViewProps = {
    x: number
    y: number
    tableauNode: TableauNode
    tableau: Tableau
    dispatch: Dispatch<MainState>
    leadsToTheCurrentState: boolean
}

type TableauNodeViewState = {}

export default class TableauNodeView extends React.Component<TableauNodeViewProps, TableauNodeViewState> {
    constructor (props: TableauNodeViewProps) {
        super(props);
    }

    getClass () {
        const result = [TableauConstants.NODE.CLASS.BASE];
        if (this.props.leadsToTheCurrentState) {
            result.push(TableauConstants.NODE.CLASS.LEADS_TO_ACTIVE);
        }
        if (typeof this.props.tableauNode.state !== "undefined") {
            result.push(TableauConstants.NODE.CLASS[this.props.tableauNode.state]);
        }
        return result.join(" ");
    }

    isActiveNode () {
        return this.props.tableau.activeNodeId === this.props.tableauNode.id;
    }

    isRootNode () {
        return this.props.tableau.rootNodeId === this.props.tableauNode.id;
    }

    @autobind
    clickHandler () {
        this.props.dispatch(TABLEAU_ACTIONS.CREATE_TRACE_TABLEAU_TO_NODE(this.props.tableauNode.id, this.props.tableau.id));
    }

    render () {
        return (
            <g
                className={this.getClass()}
                transform={`translate(${this.props.x},${this.props.y})`}
            >
                <circle
                    className={this.getClass()}
                    r={TableauConstants.NODE.RADIUS.DEFAULT}
                    filter={this.props.leadsToTheCurrentState ? "url(#active-shadow)" : ""}
                    onClick={this.clickHandler}
                />
                {
                    this.isRootNode() ?
                        <circle
                            fill="white"
                            r={TableauConstants.NODE.RADIUS.ROOT_INNER}
                            pointerEvents="none"
                        /> :
                        null
                }
            </g>
        );
    }
}