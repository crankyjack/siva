import {AnyAction, Reducer} from "redux";
import {TableauState} from "../tableau";
import TABLEAU_ACTIONS from "./tableau-actions";
import TABLEAU_HELPER_FUNCTIONS from "../utils/tableau-helper-functions";
import TableauRuleRouter from "../utils/tableau-rule-router";
import {initNormalizedCategoryInState, setInState} from "../../Common/utils/util";
import ACTIONS from "../../App/flux/actions";

const TableauReducer: Reducer<TableauState> = (state: TableauState, payload: AnyAction) => {
    payloadSwitch:
        switch (payload.type) {
            case ACTIONS.CREATE_WORKSPACE: {
                const action = payload as ACTIONS.PAYLOAD_CREATE_WORKSPACE;

                state = initNormalizedCategoryInState(state, "Tableaus");
                state = initNormalizedCategoryInState(state, "TableauNodes");

                break payloadSwitch;
            }
            case TABLEAU_ACTIONS.PRUNE_ACTIVE_CHILDREN: {
                const action = payload as TABLEAU_ACTIONS.PAYLOAD_TRACE_TABLEAU_TO_NODE;
                const tableau = state.Tableaus.byId[action.tableauId];

                state = setInState(state, ["TableauNodes", "byId", tableau.activeNodeId, "childrenIds"], [], true);

                break;
            }
            case TABLEAU_ACTIONS.TRACE_TABLEAU_TO_NODE: {
                const action = payload as TABLEAU_ACTIONS.PAYLOAD_TRACE_TABLEAU_TO_NODE;
                const tableau = state.Tableaus.byId[action.tableauId];

                const nodesInfo = TABLEAU_HELPER_FUNCTIONS.getNodesInfoMap(tableau.rootNodeId, state.TableauNodes);

                let backtrackBranchNodeIds = [tableau.activeNodeId];
                let applyTableauNodeIds = [action.nodeId];

                while (nodesInfo[backtrackBranchNodeIds[backtrackBranchNodeIds.length - 1]].level > nodesInfo[applyTableauNodeIds[0]].level) {
                    backtrackBranchNodeIds.push(nodesInfo[backtrackBranchNodeIds[backtrackBranchNodeIds.length - 1]].parentId);
                }
                while (nodesInfo[applyTableauNodeIds[0]].level > nodesInfo[backtrackBranchNodeIds[backtrackBranchNodeIds.length - 1]].level) {
                    applyTableauNodeIds.unshift(nodesInfo[applyTableauNodeIds[0]].parentId);
                }

                while (applyTableauNodeIds[0] !== backtrackBranchNodeIds[backtrackBranchNodeIds.length - 1]) {
                    applyTableauNodeIds.unshift(nodesInfo[applyTableauNodeIds[0]].parentId);
                    backtrackBranchNodeIds.push(nodesInfo[backtrackBranchNodeIds[backtrackBranchNodeIds.length - 1]].parentId);
                }

                for (const nodeId of backtrackBranchNodeIds) {
                    state = TableauRuleRouter.revertTableauRule(action.tableauId, state);
                }
                for (const nodeId of applyTableauNodeIds) {
                    state = TableauRuleRouter.applyTableauRule(state.TableauNodes.byId[nodeId], action.tableauId, state);
                }
                break payloadSwitch;
            }
            case TABLEAU_ACTIONS.UPDATE_ACTIVE_NODE_STATE: {
                const action = payload as TABLEAU_ACTIONS.PAYLOAD_UPDATE_ACTIVE_NODE_STATE;

                state = TableauRuleRouter.updateActiveTableauNodeState(action.tableauId, state);
                break payloadSwitch;
            }
        }
    return state;

};

export default TableauReducer;