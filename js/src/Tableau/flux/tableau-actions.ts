import {AnyAction} from "../../App/flux/actions";

export namespace TABLEAU_ACTIONS {
    export const PRUNE_ACTIVE_CHILDREN = "PRUNE_ACTIVE_CHILDREN";
    export const UPDATE_ACTIVE_NODE_STATE = "UPDATE_ACTIVE_NODE_STATE";
    export const TRACE_TABLEAU_TO_NODE = "TRACE_TABLEAU_TO_NODE";

    export function CREATE_PRUNE_ACTIVE_CHILDREN (tableauId: number): PAYLOAD_PRUNE_ACTIVE_CHILDREN {
        return {
            type: PRUNE_ACTIVE_CHILDREN,
            tableauId: tableauId
        };
    }

    export function CREATE_UPDATE_ACTIVE_NODE_STATE (tableauId: number): PAYLOAD_UPDATE_ACTIVE_NODE_STATE {
        return {
            type: UPDATE_ACTIVE_NODE_STATE,
            tableauId: tableauId
        };
    }

    export function CREATE_TRACE_TABLEAU_TO_NODE (nodeId: number, tableauId: number): PAYLOAD_TRACE_TABLEAU_TO_NODE {
        return {
            type: TRACE_TABLEAU_TO_NODE,
            nodeId: nodeId,
            tableauId: tableauId
        };
    }

    export interface PAYLOAD_TRACE_TABLEAU_TO_NODE extends AnyAction {
        nodeId: number,
        tableauId: number
    }

    export interface PAYLOAD_UPDATE_ACTIVE_NODE_STATE extends TableauAction {
    }

    export interface PAYLOAD_PRUNE_ACTIVE_CHILDREN extends TableauAction {
    }
}

export interface TableauAction extends AnyAction {
    tableauId: number
}

export default TABLEAU_ACTIONS;