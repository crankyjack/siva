import {TableauState} from "../tableau";
import TableauNode from "../tableau-node";
import {appendNormalizedToState, appendToStateArray, findInNormalizedStateIdCategory, setInState} from "../../Common/utils/util";

export default class TableauRuleRouter {
    private static tableauRuleHelpers: Map<string, TableauRuleHelper> = new Map();
    private static tableauCurrentNodeStateUpdateHandlerMap: Map<string, <S extends TableauState> (tableauId: number, state: S) => S> = new Map();

    static registerHelper (tableauRuleHelper: TableauRuleHelper) {
        this.tableauRuleHelpers.set(tableauRuleHelper.TABLEAU_ACTION_TYPE, tableauRuleHelper);
    }

    static getHelper (tableauRuleType: string) {
        return this.tableauRuleHelpers.get(tableauRuleType);
    }

    static registerTableauCurrentNodeStateUpdateHandler (tableauType: string, updateHandler: <S extends TableauState> (tableauId: number, state: S) => S) {
        this.tableauCurrentNodeStateUpdateHandlerMap.set(tableauType, updateHandler);
    }

    static getTableauCurrentNodeStateUpdateHandler (tableauType: string) {
        return this.tableauCurrentNodeStateUpdateHandlerMap.get(tableauType);
    }

    static applyTableauRule<S extends TableauState> (tableauNode: TableauNode, tableauId: number, state: S): S {
        const tableauRuleHelper = this.getHelper(tableauNode.actionType)!;
        const tableau = state.Tableaus.byId[tableauId];

        if (tableau.rootNodeId !== tableauNode.id &&
            state.TableauNodes.byId[tableau.activeNodeId].childrenIds.indexOf(tableauNode.id) !== -1) {
            state = tableauRuleHelper.applyTableauRule(tableauNode, state);
            state = setInState(state, ["Tableaus", "byId", tableauId, "activeNodeId"], tableauNode.id, true);
        }
        if (typeof tableauNode.state === "undefined") {
            state = this.updateActiveTableauNodeState(tableauId, state);
        }
        return state;
    }

    static updateActiveTableauNodeState<S extends TableauState> (tableauId: number, state: S): S {
        const tableau = state.Tableaus.byId[tableauId];
        const tableauNodeStateUpdateHandler = this.getTableauCurrentNodeStateUpdateHandler(tableau.type)!;
        state = tableauNodeStateUpdateHandler(tableauId, state);
        return state;
    }

    static revertTableauRule<S extends TableauState> (tableauId: number, state: S): S {
        const tableau = state.Tableaus.byId[tableauId];
        const activeNode = state.TableauNodes.byId[tableau.activeNodeId];

        const tableauRuleHelper = this.getHelper(activeNode.actionType)!;

        if (tableau.rootNodeId !== activeNode.id) {
            const parentNode = findInNormalizedStateIdCategory(state.TableauNodes, node => node.childrenIds.indexOf(activeNode.id) !== -1)!;

            state = tableauRuleHelper.revertTableauRule(activeNode, state);
            state = setInState(state, ["Tableaus", "byId", tableauId, "activeNodeId"], parentNode.id, true);
        }
        return state;
    }

    static expandTableau<S extends TableauState> (tableauExpandNode: TableauNode, tableauId: number, state: S) {
        const tableau = state.Tableaus.byId[tableauId];

        state = appendNormalizedToState(state, ["TableauNodes"], tableauExpandNode);
        state = this.appendTableauNodeChild(tableau.activeNodeId, tableauExpandNode.id, state);
        return state;
    }

    static appendTableauNodeChild<S extends TableauState> (tableauNodeId: number, tableauNodeIdToAppend: number, state: S) {
        return appendToStateArray(state, ["TableauNodes", "byId", tableauNodeId, "childrenIds"], tableauNodeIdToAppend, false);
    }

    static getTableauNodeLabelText (tableauNode: TableauNode, tableauNodeDescriptionState: any) {
        const tableauRuleHelper = this.getHelper(tableauNode.actionType)!;

        return tableauRuleHelper.getTableauNodeLabelText(tableauNode, tableauNodeDescriptionState);
    }
}

export interface TableauRuleHelper {
    TABLEAU_ACTION_TYPE: string;

    applyTableauRule<S extends TableauState> (tableauNode: TableauNode, state: S): S;

    revertTableauRule<S extends TableauState> (tableauNode: TableauNode, state: S): S;

    getTableauNodeLabelText (tableauNode: TableauNode, tableauNodeDescriptionState: any): string;
}