import {TableauNodeState} from "../tableau-node";

export namespace TableauConstants {
    export const NODE = {
        CLASS: {
            BASE: "tableau-node",
            LEADS_TO_ACTIVE: "tableau-node--leads-to-active",
            [TableauNodeState.NEUTRAL]: "tableau-node--neutral",
            [TableauNodeState.SUCCESS]: "tableau-node--success",
            [TableauNodeState.FAILURE]: "tableau-node--failure"
        },
        SPACING: {
            X: 100,
            Y: 120
        },
        GROUP: {
            CLASS: {
                ROOT: "tableau-node-group-root"
            }
        },
        RADIUS: {
            DEFAULT: 16,
            ROOT_INNER: 8
        }
    };
}